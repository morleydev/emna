Emna
====

C++ game framework for Entity/Component systems

Supports SFML for native compilation, with work-in-progress support for SDL for Javascript compilation via Emscripten.

Requires
* CMake
* SFML on Unix
* Zander C++ Dependency Management on Windows
* Emscripten for Javascript

* [![Build Status](https://travis-ci.org/MorleyDev/Emna.png?branch=master)](https://travis-ci.org/MorleyDev/Emna) **Coverage Build (Travis-CI)**
* [![Coverage Status](http://coveralls.io/repos/MorleyDev/Emna/badge.png?branch=master)](http://coveralls.io/r/MorleyDev/Emna?branch=master)
* [![Build Status](https://drone.io/github.com/MorleyDev/Emna/status.png)](https://drone.io/github.com/MorleyDev/Emna/latest) - **GCC 4.8 (Linux)**
* [![Build status](https://ci.appveyor.com/api/projects/status/6n8ln0sr6ysa76nw)](https://ci.appveyor.com/project/MorleyDev/Emna) - **Visual Studio 12 (Windows)**
