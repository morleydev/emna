#include <Emna/View.h>
#include <Emna/ViewStore.h>
#include <Emna/util/make_unique.h>

#include <bandit/bandit.h>

#include <Emna/test/mocks/MockGraphicsDriver.h>
#include <Emna/test/mocks/MockView.h>

#include <array>
#include <stdexcept>

go_bandit([]() {
	using namespace bandit;

	describe("Given a ViewStore with inserted views", []() {
		std::vector<std::size_t> callOrder;
		std::array<emna::test::Mock<void (emna::GraphicsDriver *)>, 7> mockDraws;
		mockDraws[0].setCallback([&](emna::GraphicsDriver* gd) { callOrder.push_back(0); });
		mockDraws[1].setCallback([&](emna::GraphicsDriver* gd) { callOrder.push_back(1); });
		mockDraws[2].setCallback([&](emna::GraphicsDriver* gd) { callOrder.push_back(2); });
		mockDraws[3].setCallback([&](emna::GraphicsDriver* gd) { callOrder.push_back(3); });
		mockDraws[4].setCallback([&](emna::GraphicsDriver* gd) { callOrder.push_back(4); });
		mockDraws[5].setCallback([&](emna::GraphicsDriver* gd) { callOrder.push_back(5); });
		mockDraws[6].setCallback([&](emna::GraphicsDriver* gd) { callOrder.push_back(6); });

		emna::ViewStore viewStore;
		viewStore.add< emna::test::mocks::MockView<0> >(2, &mockDraws[0]);
		viewStore.add< emna::test::mocks::MockView<1> >(4, &mockDraws[1]);
		viewStore.add< emna::test::mocks::MockView<2> >(8, &mockDraws[2]);
		viewStore.add< emna::test::mocks::MockView<3> >(0, &mockDraws[3]);
		viewStore.add< emna::test::mocks::MockView<3> >(23, &mockDraws[6]);
		viewStore.add< emna::test::mocks::MockView<4> >(-1, &mockDraws[4]);
		viewStore.add< emna::test::mocks::MockView<5> >(-2, &mockDraws[5]);

		viewStore.remove< emna::test::mocks::MockView<0> >();
		viewStore.remove< emna::test::mocks::MockView<3> >();

		describe("When getting an inserted view", [&]() {
			auto& returnedView = viewStore.get< emna::test::mocks::MockView<1> >();

			it("Then the expected view was returned", [&]() {
				AssertThat(returnedView.mockdraw.get(), Is().EqualTo(&mockDraws[1]));
			});
		});
		it("Then getting a view that was not inserted throws the expected exception", [&]() {
			AssertThrows(std::out_of_range, viewStore.get< emna::test::mocks::MockView<14> >());
		});
		it("Then getting a view that was not removed throws the expected exception", [&]() {
			AssertThrows(std::out_of_range, viewStore.get< emna::test::mocks::MockView<3> >());
		});
		it("Then removing a non-existant view does nothing", [&]() {
			viewStore.remove< emna::test::mocks::MockView<12> >();
		});
		describe("When ticking the view store", [&]() {
			emna::test::mocks::MockGraphicsDriver expectedGraphics;
			viewStore.draw(expectedGraphics);

			it("Then the expected views are drawn", [&]() {
				mockDraws[1].verifyTimes(1, &expectedGraphics);
				mockDraws[2].verifyTimes(1, &expectedGraphics);
				mockDraws[4].verifyTimes(1, &expectedGraphics);
				mockDraws[5].verifyTimes(1, &expectedGraphics);
			});
			it("Then the removed views are not drawn", [&]() {
				mockDraws[0].verifyTimes(0, &expectedGraphics);
				mockDraws[3].verifyTimes(0, &expectedGraphics);
				mockDraws[6].verifyTimes(0, &expectedGraphics);
			});
			it("Then the views are drawn in the expected order", [&]() {
				AssertThat(callOrder, Is().EqualToContainer(std::vector<std::size_t>({ 5, 4, 1, 2 })));
			});
		});
	});
});
