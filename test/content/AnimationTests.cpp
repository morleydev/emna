#include <Emna/content/Animation.h>
#include <Emna/test/mocks/content/MockTexture.h>
#include <Emna/util/make_unique.h>
#include <Emna/util/to_global_string.h>
#include <bandit/bandit.h>

go_bandit([]()
{
	using namespace bandit;
	describe("Given an animation with frames and a texture", [&]() { 
		
		emna::test::mocks::content::MockTexture* mockAnimationTexture;
		std::chrono::microseconds animationFrameLength;
		std::vector<emna::content::Animation::Frame> frames;
		std::unique_ptr<emna::content::Animation> animation;
		
		before_each([&]() { 
			auto animationTexture = emna::util::make_unique<emna::test::mocks::content::MockTexture>(123, 435);
			mockAnimationTexture = animationTexture.get();
			
			animationFrameLength = std::chrono::microseconds(50000000);
			
			frames = std::vector<emna::content::Animation::Frame>();
			frames.push_back(emna::content::Animation::Frame({ emna::maths::point2i(12, 10), emna::maths::point2i(15, 2) }));
			frames.push_back(emna::content::Animation::Frame({ emna::maths::point2i(1, 5), emna::maths::point2i(5, 12) }));
			
			animation = emna::util::make_unique<emna::content::Animation>(std::move(animationTexture), animationFrameLength, frames);
		});
		it("Then the animation length is as expected", [&]() {
			AssertThat(animation->getLength(), Is().EqualTo(std::chrono::microseconds(100000000)));
		});
		it("Then getting the texture returns the expected texture", [&]() { 
			AssertThat(&animation->getTexture(), Is().EqualTo(mockAnimationTexture));
		});
		it("Then getting the frame count returns the expected frame count", [&]() {
			AssertThat(animation->getFrameCount(), Is().EqualTo(frames.size()));
		});
		for(auto time : std::vector<std::chrono::microseconds>({ std::chrono::microseconds(0), std::chrono::microseconds(10), std::chrono::microseconds(49999999), std::chrono::microseconds(100000000), std::chrono::microseconds(104999999), std::chrono::microseconds(200000000) }))
			it(emna::util::to_global_string("Then getting time " + std::to_string(time.count()) + " within the first frame gives the expected frame"), [&]() {
				auto frame = animation->getTextureFrameAt(time);
				AssertThat(frame.position, Is().EqualTo(frames[0].position));
				AssertThat(frame.size, Is().EqualTo(frames[0].size));
			});
		for(auto time : std::vector<std::chrono::microseconds>({ std::chrono::microseconds(50000000), std::chrono::microseconds(50000010), std::chrono::microseconds(99999999), std::chrono::microseconds(159999999) }))
			it(emna::util::to_global_string("Then getting time " + std::to_string(time.count()) + " within the second frame gives the expected frame"), [&]() {
				auto frame = animation->getTextureFrameAt(time);
				AssertThat(frame.position, Is().EqualTo(frames[1].position));
				AssertThat(frame.size, Is().EqualTo(frames[1].size));
			});
	});
});
