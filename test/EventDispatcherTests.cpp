#include <Emna/EventDispatcher.h>
#include <Emna/test/Mock.h>
#include <bandit/bandit.h>

go_bandit([](){
	using namespace bandit;
	describe("Given an EventDispatcher with registered handlers", []() {
		emna::EventDispatcher eventDispatcher;

		emna::test::Mock<void (int)> mockHandlerI1;
		emna::test::Mock<void (int)> mockHandlerI2;
		emna::test::Mock<void (float)> mockHandlerF;
		emna::test::Mock<void (char)> mockHandlerC;
		
		auto handler1n = eventDispatcher.bind<int>([&](int v) { mockHandlerI1(v); });
		auto handler2n = eventDispatcher.bind<int>([&](int v) { mockHandlerI2(v); });
		auto handler1f = eventDispatcher.bind<float>([&](float v) { mockHandlerF(v); });
		auto handler1c = eventDispatcher.bind<char>([&](char v) { mockHandlerC(v); });
		
		describe("when triggering an event", [&]() {
			auto expectedPtr = std::make_shared<int>();
			eventDispatcher.trigger(typeid(int), expectedPtr);

			it("then the expected handlers were triggered", [&]() {
				mockHandlerI1.verifyTimes(1, *expectedPtr);
				mockHandlerI2.verifyTimes(1, *expectedPtr);
			});
		});

		describe("when an event dispatcher has been removed via assignment and the event is triggered", [&]() {
			handler1n = std::move(emna::EventDispatcherHandler());
			mockHandlerI1 = std::move(emna::test::Mock<void (int)>());

			auto expectedPtr = std::make_shared<int>();
			eventDispatcher.trigger(typeid(int), expectedPtr);

			it("then the removed handler is not triggered", [&]() {
				mockHandlerI1.verifyTimes(0, *expectedPtr);
			});
		});

		describe("when an event dispatcher is removed via deconstruction and the event is triggered", [&]() {

			emna::test::Mock<void (int)> mockHandlerNew;
			auto expectedPtr = std::make_shared<int>();
			eventDispatcher.bind<int>([&](int v) { mockHandlerNew(v); });

			eventDispatcher.trigger(typeid(int), expectedPtr);

			it("then the removed handler is not triggered", [&]() {
				mockHandlerNew.verifyTimes(0, *expectedPtr);
			});
		});
	});
});
