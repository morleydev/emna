#include <Emna/EventQueue.h>
#include <bandit/bandit.h>

go_bandit([]() {
	using namespace bandit;
	describe("Given an event queue", []() {
		emna::EventQueue eventQueue;

		it("Then popping the event queue returns false", [&]() {
			std::pair<const std::type_info*, std::shared_ptr<void>> out;

			AssertThat(eventQueue.pop(out), Is().False());
		});

		describe("When data has been pushed onto the event queue", [&]() {
			eventQueue.push<int>(5);
			eventQueue.push<char>('a');
			eventQueue.push<float>(8.0f);

			it("Then popping the events returns the expected data in the expected order", [&]() {
				std::pair<const std::type_info*, std::shared_ptr<void>> out;

				AssertThat(eventQueue.pop(out), Is().True());
				AssertThat(out.first, Is().EqualTo(&typeid(int)));
				AssertThat(*std::static_pointer_cast<int>(out.second), Is().EqualTo(5));

				AssertThat(eventQueue.pop(out), Is().True());
				AssertThat(out.first, Is().EqualTo(&typeid(char)));
				AssertThat(*std::static_pointer_cast<char>(out.second), Is().EqualTo('a'));

				AssertThat(eventQueue.pop(out), Is().True());
				AssertThat(out.first, Is().EqualTo(&typeid(float)));
				AssertThat(*std::static_pointer_cast<float>(out.second), Is().EqualTo(8.0f));
			});
			it("Then popping the events when all have been popped returns false", [&]() {
				std::pair<const std::type_info*, std::shared_ptr<void>> out;

				AssertThat(eventQueue.pop(out), Is().False());
			});
		});
	});

});
