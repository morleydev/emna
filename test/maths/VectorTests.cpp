#include <Emna/maths/vector.h>
#include <bandit/bandit.h>

go_bandit([]() {
	bandit::describe("emna::maths::dot_product(point2, point2): ", []() {
		bandit::it("of positive points gives expected value", []() { 
			emna::maths::point2i p1(10, 5);
			emna::maths::point2i p2(2, 3);
			AssertThat(emna::maths::dot_product(p1, p2), Equals(35));
		});
		bandit::it("of negative points gives expected value", []() { 
			emna::maths::point2i p1(-10, -5);
			emna::maths::point2i p2(-2, -3);
			AssertThat(emna::maths::dot_product(p1, p2), Equals(35));
		});
		bandit::it("of mixed sign points gives expected value", []() { 
			emna::maths::point2i p1(10, -5);
			emna::maths::point2i p2(-2, 3);
			AssertThat(emna::maths::dot_product(p1, p2), Equals(-35));
		});
	});
	
	bandit::describe("emna::maths::magnitude_squared(point2): ", []() {
		bandit::it("of positive point gives expected value", []() { 
			emna::maths::point2i point(4, 2);
			AssertThat(emna::maths::magnitude_squared(point), Equals(20));
		});
		bandit::it("of negative point gives expected value", []() { 
			emna::maths::point2i point(-5, -3);
			AssertThat(emna::maths::magnitude_squared(point), Equals(34));
		});
		bandit::it("of mixed sign point gives expected value", []() { 
			emna::maths::point2i point(9, -7);
			AssertThat(emna::maths::magnitude_squared(point), Equals(130));
		});
	});
	
	bandit::describe("emna::maths::magnitude(point2): ", []() {
		bandit::it("of positive point gives expected value", []() { 
			emna::maths::point2f point(3.0f, 4.0f);
			AssertThat(emna::maths::magnitude(point), EqualsWithDelta(5.0f, 0.001f));
		});
		bandit::it("of negative point gives expected value", []() { 
			emna::maths::point2f point(-3.0f, -4.0f);
			AssertThat(emna::maths::magnitude(point), EqualsWithDelta(5.0f, 0.001f));
		});
		bandit::it("of mixed sign point gives expected value", []() { 
			emna::maths::point2f point(3.0f, -4.0f);
			AssertThat(emna::maths::magnitude(point), EqualsWithDelta(5.0f, 0.001f));
		});
	});
	
	bandit::describe("emna::maths::normalise(point2): ", []() {
		bandit::it("of positive point gives expected value", []() { 
			emna::maths::point2f point(3.0f, 4.0f);
			emna::maths::point2f expected(0.6f, 0.8f);
			AssertThat(emna::maths::normalise(point), EqualsWithDelta(expected, emna::maths::point2f(0.001f, 0.001f)));
		});
		bandit::it("of negative point gives expected value", []() { 
			emna::maths::point2f point(-3.0f, -4.0f);
			emna::maths::point2f expected(-0.6f, -0.8f);
			AssertThat(emna::maths::normalise(point), EqualsWithDelta(expected, emna::maths::point2f(0.001f, 0.001f)));
		});
		bandit::it("of mixed sign point gives expected value", []() { 
			emna::maths::point2f point(-3.0f, 4.0f);
			emna::maths::point2f expected(-0.6f, 0.8f);
			AssertThat(emna::maths::normalise(point), EqualsWithDelta(expected, emna::maths::point2f(0.001f, 0.001f)));
		});
	});
	
	bandit::describe("emna::maths::dot_product(point3, point3): ", []() {
		bandit::it("of positive points gives expected value", []() { 
			emna::maths::point3i p1(10, 5, 2);
			emna::maths::point3i p2(2, 3, 3);
			AssertThat(emna::maths::dot_product(p1, p2), Equals(41));
		});
		bandit::it("of negative points gives expected value", []() { 
			emna::maths::point3i p1(-10, -5, -2);
			emna::maths::point3i p2(-2, -3, -3);
			AssertThat(emna::maths::dot_product(p1, p2), Equals(41));
		});
		bandit::it("of mixed sign points gives expected value", []() { 
			emna::maths::point3i p1(10, -5, 2);
			emna::maths::point3i p2(-2, 3, -3);
			AssertThat(emna::maths::dot_product(p1, p2), Equals(-41));
		});
	});
	
	bandit::describe("emna::maths::magnitude_squared(point3): ", []() {
		bandit::it("of positive point gives expected value", []() { 
			emna::maths::point3i point(4, 2, 8);
			AssertThat(emna::maths::magnitude_squared(point), Equals(84));
		});
		bandit::it("of negative point gives expected value", []() { 
			emna::maths::point3i point(-4, -2, -8);
			AssertThat(emna::maths::magnitude_squared(point), Equals(84));
		});
		bandit::it("of mixed sign point gives expected value", []() { 
			emna::maths::point3i point(4, -2, 8);
			AssertThat(emna::maths::magnitude_squared(point), Equals(84));
		});
	});
	
	bandit::describe("emna::maths::magnitude(point3): ", []() {
		bandit::it("of positive point gives expected value", []() { 
			emna::maths::point3f point(3.0f, 4.0f, 2.0f);
			AssertThat(emna::maths::magnitude(point), EqualsWithDelta(5.385164807f, 0.001f));
		});
		bandit::it("of negative point gives expected value", []() { 
			emna::maths::point3f point(-3.0f, -4.0f, -2.0f);
			AssertThat(emna::maths::magnitude(point), EqualsWithDelta(5.385164807f, 0.001f));
		});
		bandit::it("of mixed sign point gives expected value", []() { 
			emna::maths::point3f point(3.0f, -4.0f, 2.0f);
			AssertThat(emna::maths::magnitude(point), EqualsWithDelta(5.385164807f, 0.001f));
		});
	});
	
	bandit::describe("emna::maths::normalise(point3): ", []() {
		bandit::it("of positive point gives expected value", []() { 
			emna::maths::point3f point(3.0f, 4.0f, 2.0f);
			emna::maths::point3f expected(0.557086015f, 0.742781353f, 0.371390676f);
			AssertThat(emna::maths::normalise(point), EqualsWithDelta(expected, emna::maths::point3f(0.001f, 0.001f, 0.001f)));
		});
		bandit::it("of negative point gives expected value", []() { 
			emna::maths::point3f point(-3.0f, -4.0f, -2.0f);
			emna::maths::point3f expected(-0.557086015f, -0.742781353f, -0.371390676f);
			AssertThat(emna::maths::normalise(point), EqualsWithDelta(expected, emna::maths::point3f(0.001f, 0.001f, 0.001f)));
		});
		bandit::it("of mixed sign point gives expected value", []() { 
			emna::maths::point3f point(-3.0f, 4.0f, 2.0f);
			emna::maths::point3f expected(-0.557086015f, 0.742781353f, 0.371390676f);
			AssertThat(emna::maths::normalise(point), EqualsWithDelta(expected, emna::maths::point3f(0.001f, 0.001f, 0.001f)));
		});
	});
});
