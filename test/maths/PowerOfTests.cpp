#include <Emna/maths/powerOf.h>
#include <bandit/bandit.h>

go_bandit([]() { 
	using namespace bandit;
	describe("is powerOf2", []() {
		it("-1 is not power of 2", []() { 
			AssertThat(emna::maths::isPowerOf2(-1), Equals(false));
		});
		it("0 is not power of 2", []() { 
			AssertThat(emna::maths::isPowerOf2(0), Equals(false));
		});
		it("1 is power of 2", []() { 
			AssertThat(emna::maths::isPowerOf2(1), Equals(true));
		});
		it("2 is power of 2", []() { 
			AssertThat(emna::maths::isPowerOf2(2), Equals(true));
		});
		it("256 is power of 2", []() { 
			AssertThat(emna::maths::isPowerOf2(256), Equals(true));
		});
	});
	
	describe("nextPowerOf2: ", []() {
		it("0 gives 1", []() { 
			AssertThat(emna::maths::nextPowerOf2(0), Equals(1));
		});
		it("1 gives 2", []() { 
			AssertThat(emna::maths::nextPowerOf2(1), Equals(2));
		});
		it("2 gives 4", []() { 
			AssertThat(emna::maths::nextPowerOf2(2), Equals(4));
		});
		it("3 gives 4", []() { 
			AssertThat(emna::maths::nextPowerOf2(3), Equals(4));
		});
		it("200 gives 256", []() { 
			AssertThat(emna::maths::nextPowerOf2(200), Equals(256));
		});
		it("-1 gives 1", []() { 
			AssertThat(emna::maths::nextPowerOf2(-1), Equals(1));
		});
	});
});
