#include <Emna/Controller.h>
#include <Emna/ControllerStore.h>
#include <Emna/util/make_unique.h>

#include <bandit/bandit.h>
#include <Emna/test/mocks/MockController.h>

#include <array>

go_bandit([]() {
	using namespace bandit;

	describe("Given a ControllerStore with inserted controllers", []() {

		std::array<emna::test::Mock<void (std::chrono::microseconds)>, 6> mockUpdates;

		emna::ControllerStore controllerStore;
		controllerStore.add< emna::test::mocks::MockController<0> >(&mockUpdates[0]);
		controllerStore.add< emna::test::mocks::MockController<1> >(&mockUpdates[1]);
		controllerStore.add< emna::test::mocks::MockController<2> >(&mockUpdates[2]);
		controllerStore.add< emna::test::mocks::MockController<3> >(&mockUpdates[3]);
		controllerStore.add< emna::test::mocks::MockController<4> >(&mockUpdates[4]);
		controllerStore.add< emna::test::mocks::MockController<5> >(&mockUpdates[5]);
		controllerStore.remove< emna::test::mocks::MockController<0> >();
		controllerStore.remove< emna::test::mocks::MockController<3> >();

		describe("When getting an inserted controller", [&]() {
			auto& returnedController = controllerStore.get< emna::test::mocks::MockController<1> >();

			it("Then the expected controller was returned", [&]() {
				AssertThat(returnedController.mockupdate.get(), Is().EqualTo(&mockUpdates[1]));
			});
		});
		it("Then getting a controller that was not inserted throws the expected exception", [&]() {
			AssertThrows(std::out_of_range, controllerStore.get< emna::test::mocks::MockController<14> >());
		});
		it("Then getting a controller that was not removed throws the expected exception", [&]() {
			AssertThrows(std::out_of_range, controllerStore.get< emna::test::mocks::MockController<3> >());
		});
		it("Then removing a non-existant controller does nothing", [&]() {
			controllerStore.remove< emna::test::mocks::MockController<12> >();
		});
		describe("When ticking the controller store", [&]() {
			std::chrono::microseconds expectedDeltaTime(32215);
			controllerStore.tick(expectedDeltaTime);

			it("Then the expected controllers are updated", [&]() {
				mockUpdates[1].verifyTimes(1, expectedDeltaTime);
				mockUpdates[2].verifyTimes(1, expectedDeltaTime);
				mockUpdates[4].verifyTimes(1, expectedDeltaTime);
				mockUpdates[5].verifyTimes(1, expectedDeltaTime);
			});
			it("Then the expected controllers are not updated", [&]() {
				mockUpdates[0].verifyTimes(0, expectedDeltaTime);
				mockUpdates[3].verifyTimes(0, expectedDeltaTime);
			});
		});
	});
});
