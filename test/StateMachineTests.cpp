#include <Emna/StateMachine.h>
#include <Emna/util/to_global_string.h>
#include <bandit/bandit.h>
#include <Emna/test/mocks/MockGraphicsDriver.h>
#include <Emna/test/mocks/MockState.h>
#include <map>

go_bandit([]() {
	using namespace bandit;

	describe("Given a state machine with added states", []() {
		std::map<std::size_t, emna::test::Mock< std::unique_ptr<emna::State> ()> > mockStateFactories;
		std::map<std::size_t, emna::test::mocks::MockState*> mockStates;

		auto createMockState = [&](std::size_t id) -> std::unique_ptr<emna::State> {
			auto state = emna::util::make_unique<emna::test::mocks::MockState>();
			mockStates[id] = state.get();
			state->mockupdate.setReturn(id);
			return std::move(state);
		};

		mockStateFactories[0].setReturn([&]() { return createMockState(0); });
		mockStateFactories[1].setReturn([&]() { return createMockState(1); });
		mockStateFactories[2].setReturn([&]() { return createMockState(2); });
		mockStateFactories[3].setReturn([&]() { return createMockState(3); });
		mockStateFactories[4].setReturn([&]() { return createMockState(4); });

		emna::StateMachine stateMachine;
		stateMachine.add(0, [&]() { return mockStateFactories[0](); });
		stateMachine.add(1, [&]() { return mockStateFactories[1](); });
		stateMachine.add(2, [&]() { return mockStateFactories[2](); });
		stateMachine.add(3, [&]() { return mockStateFactories[3](); });
		stateMachine.add(4, [&]() { return mockStateFactories[4](); });

		it("Then no state is created ", [&]() {
			for(auto& states : mockStateFactories)
				states.second.verifyTimes(0);
		});

		describe("When the state machine is drawn", [&]() {
			emna::test::mocks::MockGraphicsDriver mockGraphicsDriver;
			stateMachine.draw(mockGraphicsDriver);

			it("Then no state is drawn ", [&]() {
				for(auto& states : mockStates)
					states.second->mockdraw.verifyTimes(0, &mockGraphicsDriver);
			});
		});
		describe("When the state machine is updated", [&]() {
			std::chrono::microseconds expectedDT(2134566);
			stateMachine.update(expectedDT);

			it("Then no state is updated ", [&]() {
				for(auto& states : mockStates)
					states.second->mockupdate.verifyTimes(0, expectedDT);
			});
		});

		describe("Then moving to a state that does not exist does not crash", [&]() {
			stateMachine.move(132);
			it("And updating does not crash", [&]() {
				stateMachine.update(std::chrono::microseconds(21314));
			});
			it("And drawing does not crash", [&]() {
				emna::test::mocks::MockGraphicsDriver mockGraphicsDriver;
				stateMachine.draw(mockGraphicsDriver);
			});
		});

		for(auto state = 4; state > 0; --state) {
			describe(emna::util::to_global_string(std::string("When moving to state ") + std::to_string(state)), [&]() {
				stateMachine.move(state);

				it("Then the state was created", [&]() {
					mockStateFactories[state].verifyTimes(1);
				});

				describe("And the state machine is updated", [&]() {
					std::chrono::microseconds expectedDT(215124);
					stateMachine.update(expectedDT);

					it("Then the expected state is updated", [&]() {
						mockStates[state]->mockupdate.verifyTimes(1, expectedDT);
					});
				});
				describe("And the state machine is drawn", [&]() {
					emna::test::mocks::MockGraphicsDriver mockGraphicsDriver;
					stateMachine.draw(mockGraphicsDriver);

					it("Then the expected state is drawn", [&]() {
						mockStates[state]->mockdraw.verifyTimes(1, &mockGraphicsDriver);
					});
				});
			});
		}

		describe("When moving again to state 3", [&]() {
			stateMachine.move(3);

			it("Then the state was created again", [&]() {
				mockStateFactories[3].verifyTimes(2);
			});
			describe("And the state machine is drawn", [&]() {
				emna::test::mocks::MockGraphicsDriver mockGraphicsDriver;
				stateMachine.draw(mockGraphicsDriver);

				it("Then the expected state is drawn", [&]() {
					mockStates[3]->mockdraw.verifyTimes(1, &mockGraphicsDriver);
				});
			});
			describe("And updating the state machine where state 3's update returns state 2", [&]() {
				mockStates[3]->mockupdate.setReturn(std::size_t(2));
				std::chrono::microseconds expectedDT(1232);
				stateMachine.update(expectedDT);

				it("Then state 2 was created again", [&]() {
					mockStateFactories[2].verifyTimes(2);
				});
				it("Then the state machine has moved to state 2", [&]() {
					std::chrono::microseconds expectedDT;
					emna::test::mocks::MockGraphicsDriver mockGraphicsDriver;

					stateMachine.draw(mockGraphicsDriver);
					stateMachine.update(expectedDT);

					mockStates[2]->mockdraw.verifyTimes(1, &mockGraphicsDriver);
					mockStates[2]->mockupdate.verifyTimes(1, expectedDT);
				});
			});
		});
	});
});
