#include <Emna/Game.h>

#include <bandit/bandit.h>

#include <Emna/test/Mock.h>
#include <Emna/test/mocks/MockGraphicsDriver.h>
#include <Emna/test/mocks/MockAudioDriver.h>
#include <Emna/test/mocks/MockNetworkDriver.h>
#include <Emna/test/mocks/MockInputDriver.h>
#include <Emna/test/mocks/MockState.h>
#include <Emna/test/mocks/MockController.h>
#include <Emna/test/mocks/MockView.h>

#include <memory>

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			class GameHarness : public emna::Game
			{
			public:
				struct StubSystemFactory
				{
					Mock<emna::drivers::System (emna::maths::point2i)> mockSelf;
					
					MockGraphicsDriver* mockGraphicsDriver;
					MockAudioDriver* mockAudioDriver;
					MockNetworkDriver* mockNetworkDriver;
					MockInputDriver* mockInputDriver;
					Mock<void (std::chrono::microseconds)> mockSleepFor;
					
					drivers::System operator()(emna::maths::point2i point) 
					{
						drivers::System system;
						
						auto graphics = emna::util::make_unique<MockGraphicsDriver>();
						auto input = emna::util::make_unique<MockInputDriver>();
						auto network =  emna::util::make_unique<MockNetworkDriver>();
						auto audio = emna::util::make_unique<MockAudioDriver>();
						
						mockGraphicsDriver = graphics.get();
						mockAudioDriver = audio.get();
						mockNetworkDriver = network.get();
						mockInputDriver = input.get();
						mockSleepFor = Mock<void (std::chrono::microseconds)>();
			
						system.graphicsDevice = std::move(graphics);
						system.audioDevice = std::move(audio);
						system.networkDevice = std::move(network);
						system.inputDevice = std::move(input);
						system.sleepFor = [&](std::chrono::microseconds time) { mockSleepFor(time); };
						system.isFrameRateLocked = false;
						return system;
					}
				};

				MockState* activeState;
				MockController<0>* activeController;
				MockView<0>* activeView;
				
				std::function<void (std::chrono::microseconds)> updateBehaviour;
				std::function<void (void)> drawBehaviour;
	
				GameHarness(std::uint_least16_t x, 
							std::uint_least16_t y,
							std::function<drivers::System (emna::maths::point2i)> systemFactory)
							: emna::Game(x, y, systemFactory),
							activeState(nullptr),
							activeController(nullptr),
							activeView(nullptr)
				{
					states.add(0, [this]() 
					{ 
						auto state = util::make_unique<MockState>(); 
						activeState = state.get();
						return state;
					});
					states.move(0);
					
					controllers.add<MockController<0>>();
					activeController = &controllers.get<MockController<0>>();
					
					views.add<MockView<0>>(0);
					activeView = &views.get<MockView<0>>();
				}
				
				virtual ~GameHarness()
				{
				}
				
				Mock<void (std::chrono::microseconds)> mockupdate;
				virtual void update(std::chrono::microseconds deltatime) 
				{
					Game::update(deltatime);
					mockupdate(deltatime);
				}
				
				Mock<void (void)> mockdraw;
				virtual void draw() 
				{
					Game::draw();
					mockdraw();
				}
				
				template<typename T, typename... ARGS> void queueEvent(ARGS&&... args) 
				{
					eventQueue.push<T>(std::forward<ARGS>(args)...);
				}
				
				template<typename T> EventDispatcherHandler watchEvent(std::function<void (const T&)> handler)
				{
					return eventDispatcher.bind(handler);
				}
			};
		}
	}
}

go_bandit([]() { 
	using namespace bandit;
	
	describe("Given a game with state", []() { 
		std::uint_least16_t viewX = 640;
		std::uint_least16_t viewY = 480;
		
		emna::test::mocks::GameHarness::StubSystemFactory mockSystem;
		std::unique_ptr<emna::test::mocks::GameHarness> gameHarness;
		
		before_each([&]() { 
			mockSystem = emna::test::mocks::GameHarness::StubSystemFactory();
			gameHarness = emna::util::make_unique<emna::test::mocks::GameHarness>(viewX, viewY, std::ref(mockSystem));
				
		});
		
		it("Then the framerate is not locked", [&]() { 
			AssertThat(gameHarness->isFrameRateLocked(), Is().False());
		});
		it("Then the game is alive", [&]() { 
			AssertThat(gameHarness->isAlive(), Is().True());
		});
															
		describe("When beginning an update to the game", [&]() {
			std::chrono::microseconds deltatime(1324);
				
			before_each([&]() { 
				gameHarness->beginUpdate(deltatime);
			});
			
			it("Then the input device is polled", [&]() { 
				mockSystem.mockInputDriver->mockpoll.verifyTimes(1);
			});
			it("Then the audio driver is updated", [&]() { 
				mockSystem.mockAudioDriver->mockupdate.verifyTimes(1, deltatime);
			});
		});
		describe("When updating a game", [&]() {
			std::chrono::microseconds deltatime(1324);
				
			before_each([&]() { 
				gameHarness->update(deltatime);
			});
			it("Then the state is active", [&]() { 
				AssertThat(gameHarness->activeState == nullptr, Is().False());
			});
			it("Then the state is updated", [&]() { 
				gameHarness->activeState->mockupdate.verifyTimes(1, deltatime);
			});
			it("Then the controllers are updated", [&]() { 
				gameHarness->activeController->mockupdate->verifyTimes(1, deltatime);
			});
		});													
		describe("When ending an update to the game", [&]() {
			std::chrono::microseconds deltatime(1324);
			
			emna::test::Mock<void (int)> mockIntEventHandler;
				
			before_each([&]() { 
				mockIntEventHandler = emna::test::Mock<void (int)>();
				auto intEventHandler = gameHarness->watchEvent<int>([&](int event) { mockIntEventHandler(event); });
				gameHarness->queueEvent<int>(10);
				
				gameHarness->endUpdate(deltatime);
			});
			it("Then the events are processed", [&]() { 
				mockIntEventHandler.verifyTimes(1, 10);
			});
		});
		
		describe("When beginning a draw of the game", [&]() { 
			before_each([&]() { 
				gameHarness->beginDraw();
			});
			it("Then the graphics device is cleared", [&]() { 
				mockSystem.mockGraphicsDriver->mockclear.verifyTimes(1);
			});
		});
		describe("When drawing the game", [&]() { 
			before_each([&]() {
				gameHarness->draw();
			});
			it("Then the views are drawn", [&]() { 
				gameHarness->activeView->mockdraw->verifyTimes(1, mockSystem.mockGraphicsDriver);
			});
			it("Then the states are drawn", [&]() { 
				gameHarness->activeState->mockdraw.verifyTimes(1, mockSystem.mockGraphicsDriver);
			});
		});
		describe("When ending a draw of the game", [&]() { 
			before_each([&]() { 
				gameHarness->endDraw();
			});
			it("Then the graphics device is flushed", [&]() {
				mockSystem.mockGraphicsDriver->mockflush.verifyTimes(1);
			});
		});
		describe("When killing the game", [&]() { 
			before_each([&]() { 
				gameHarness->kill();
			});
			it("Then the game is not alive", [&]() {
				AssertThat(gameHarness->isAlive(), Is().False());
			});
		});
		describe("When running the game 5 times with an unlocked rendering framerate", [&]() { 
			
			std::uint_least8_t updateCounter;
			before_each([&]() { 
				
				updateCounter = 0;
				gameHarness->mockupdate.setCallback([&](std::chrono::microseconds deltatime) { 
					if (++updateCounter == 5) 
						gameHarness->kill();
				});
				
				gameHarness->setFrameRateLocked(false);
				gameHarness->run();
			});
			
			it("Then the game is killed", [&]() { 
				AssertThat(gameHarness->isAlive(), Is().False());
			});
			it("Then the game was updated 5 times", [&]() { 
				gameHarness->mockupdate.verifyTimes(5, std::chrono::microseconds(16667));
			});
			it("Then the game was drawn at least once", [&]() { 
				gameHarness->mockdraw.verify();
			});
		});
		describe("When running the game 5 times with a locked rendering framerate", [&]() { 
			
			std::uint_least8_t updateCounter;
			before_each([&]() { 
				
				updateCounter = 0;
				gameHarness->mockupdate.setCallback([&](std::chrono::microseconds deltatime) { 
					if (++updateCounter == 5) 
						gameHarness->kill();
				});
				
				gameHarness->setFrameRateLocked(true);
				gameHarness->run();
			});
			
			it("Then the game is killed", [&]() { 
				AssertThat(gameHarness->isAlive(), Is().False());
			});
			it("Then the game was updated 5 times", [&]() { 
				gameHarness->mockupdate.verifyTimes(5, std::chrono::microseconds(16667));
			});
			it("Then the game was drawn at least once", [&]() { 
				gameHarness->mockdraw.verify();
			});
		});
		describe("When running the game and the update throws a std::exception", [&]() { 
			
			before_each([&]() { 
				gameHarness->mockupdate.setCallback([&](std::chrono::microseconds deltatime) { 
					throw std::exception();
				});
				
				gameHarness->setFrameRateLocked(true);
				gameHarness->run();
			});
			
			it("Then the game is killed", [&]() { 
				AssertThat(gameHarness->isAlive(), Is().False());
			});
			it("Then the game was updated 1 time", [&]() { 
				gameHarness->mockupdate.verifyTimes(1, std::chrono::microseconds(16667));
			});
		});
		describe("When running the game and the update throws an unknown", [&]() { 
			
			before_each([&]() { 
				gameHarness->mockupdate.setCallback([&](std::chrono::microseconds deltatime) { 
					throw int(5);
				});
				
				gameHarness->setFrameRateLocked(true);
				gameHarness->run();
			});
			
			it("Then the game is killed", [&]() { 
				AssertThat(gameHarness->isAlive(), Is().False());
			});
			it("Then the game was updated 1 time", [&]() { 
				gameHarness->mockupdate.verifyTimes(1, std::chrono::microseconds(16667));
			});
		});
	});
});
