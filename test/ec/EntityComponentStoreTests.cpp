#include <bandit/bandit.h>
#include <Emna/ec/EntityComponentStore.h>

namespace 
{
	inline void AssertAreMatching(std::vector<emna::ec::EntityId> a, std::vector<emna::ec::EntityId> b)
	{
		auto actualEntityCount = std::count_if(a.begin(), a.end(), [&](emna::ec::EntityId e) { 
			return std::find(b.begin(), b.end(), e) != b.end();
		});
		
		AssertThat(a.size(), Equals(b.size()));
		AssertThat(actualEntityCount, Equals(b.size()));
	}
}

go_bandit([]() {
	using namespace bandit;
	
	describe("Given an EntityComponentStore with entities and linked components", []() { 
		
		std::vector<emna::ec::EntityId> expectedEntities;
		emna::ec::EntityComponentStore store;
		before_each([&]() {
			expectedEntities = std::vector<emna::ec::EntityId>();
			expectedEntities.push_back(emna::ec::EntityId::generate());
			expectedEntities.push_back(emna::ec::EntityId::generate());
			expectedEntities.push_back(emna::ec::EntityId::generate());
			
			store = emna::ec::EntityComponentStore();
			for(auto entity : expectedEntities)
				store.add(entity);
				
			store.link(expectedEntities[0], int(10));
			store.link(expectedEntities[1], int(15));
			store.link(expectedEntities[1], char('z'));
		});
		
		it("then getting all entities returns the added entities", [&]() { 
			AssertAreMatching(store.getAllEntities(), expectedEntities);
		});
		it("then getting all entities with [int] components returns the expected", [&]() { 
			AssertAreMatching(store.getAllEntitiesWith<int>(), { expectedEntities[0], expectedEntities[1] });
		});
		it("then getting all entities with [char] components returns the expected", [&]() { 
			AssertAreMatching(store.getAllEntitiesWith<char>(), { expectedEntities[1] });
		});
		it("then getting all entities with [int, char] components returns the expected", [&]() {
			AssertAreMatching(store.getAllEntitiesWith<int, char>(), { expectedEntities[1] });
		});
		it("then getting a component on an entity returns the expected component", [&]() { 
			AssertThat(store.getComponentFor<int>(expectedEntities[0]), Equals(10));
			AssertThat(store.getComponentFor<int>(expectedEntities[1]), Equals(15));
			AssertThat(store.getComponentFor<char>(expectedEntities[1]), Equals('z'));
		});
		
		it("then getting a component that does not exist returns a null component", [&]() { 
			AssertThrows(std::runtime_error, store.getComponentFor<long>(expectedEntities[0]));
		});
		it("then getting a component for an entity that that does not exist returns a null component", [&]() { 
			AssertThrows(std::runtime_error, store.getComponentFor<int>(emna::ec::EntityId::generate()));
		});
		
		describe("when adding a duplicate entity ", [&]() { 
			before_each([&]() {
				store.add(expectedEntities.back());
			});
			it("then getting all entities does not contain duplicate entities", [&]() { 
				AssertAreMatching(store.getAllEntities(), expectedEntities);
			});
		});
		it("then linking a new component to an existing entity-component overwrites the previous component", [&]() { 
			store.link(expectedEntities[0], int(5));
			AssertThat(store.getComponentFor<int>(expectedEntities[0]), Equals(5));
		});
		describe("when unlinking one component ", [&]() { 
			before_each([&]() {
				store.unlink<int>(expectedEntities[0]);
			});
			it("then getting all entities with [int] component does not return the unlinked entity", [&]() { 
				AssertAreMatching(store.getAllEntitiesWith<int>(), { expectedEntities[1] });
			});
			it("then getting the unlinked component throws a runtime error", [&]() { 
				AssertThrows(std::runtime_error, store.getComponentFor<int>(expectedEntities[0]));
			});
		});
		describe("when removing an entity that exists ", [&]() { 
			emna::ec::EntityId unlinkedEntity;
			before_each([&]() {
				unlinkedEntity = expectedEntities.back();
				expectedEntities.pop_back();

				store.remove(unlinkedEntity);
			});
			it("then getting all entities does not contain the removed entity", [&]() { 
				AssertAreMatching(store.getAllEntities(), expectedEntities);
			});
			it("then getting a component on that entity throws a runtime error", [&]() { 
				AssertThrows(std::runtime_error, store.getComponentFor<char>(unlinkedEntity));
			});
		});
		it("then removing an entity that does not exist does nothing ", [&]() { 
			store.remove(emna::ec::EntityId::generate());
		});
		it("then unlinking a component from an entity that does not exist does nothing ", [&]() { 
			store.unlink<int>(emna::ec::EntityId::generate());
		});
		it("then unlinking a component that does not exist on an entity does not exist does nothing ", [&]() { 
			store.unlink<float>(expectedEntities[0]);
		});
	});
});
