#include <bandit/bandit.h>
#include <Emna/ec/ComponentStore.h>

go_bandit([](){
	using namespace bandit;
	
	describe("Given a component store", [](){
		
		emna::ec::ComponentStore store;
		
		it("then the store is empty", [&]() {
			AssertThat(store.empty(), Is().True());
		});
		
		describe("when added components", [&]() { 
			store.add(emna::ec::Component<void>(emna::ec::Component<int>(10)));
			store.add(emna::ec::Component<void>(emna::ec::Component<float>(2.4f)));
			store.add(emna::ec::Component<void>(emna::ec::Component<char>('d')));
			store.add(emna::ec::Component<void>(emna::ec::Component<char>('z')));
			
			it("then the store is not empty", [&]() {
				AssertThat(store.empty(), Is().False());
			});
			it("then the store contains the expected components", [&]() {
				AssertThat(store.size(), Equals(3));

				std::size_t matchCount = 0;
				for(const auto& a : store) 
				{ 
					if (a.getType() == typeid(int))
					{
						AssertThat(a.getData<int>(), Equals(10));
						++matchCount;
					}
					else if (a.getType() == typeid(float))
					{
						AssertThat(a.getData<float>(), Equals(2.4f));
						++matchCount;
					}
					else if (a.getType() == typeid(char))
					{
						AssertThat(a.getData<char>(), Equals('z'));
						++matchCount;
					}
				}
				AssertThat(matchCount, Equals(3));
			});
			it("then the store contains the data it contains", [&]() { 
				AssertThat(store.get(typeid(int)).getData<int>(), Equals(10));
				AssertThat(store.get(typeid(float)).getData<float>(), Equals(2.4f));
				AssertThat(store.get(typeid(char)).getData<char>(), Equals('z'));
			});
			it("then a tryGet for an existing component returns the data", [&]() { 
				emna::ec::Component<void> component;
				AssertThat(store.tryGet(typeid(int), component), Is().True());
				AssertThat(component.getData<int>(), Equals(10));
			});
			it("then a tryGet for an component that does not exist returns false", [&]() { 
				emna::ec::Component<void> component;
				AssertThat(store.tryGet(typeid(long), component), Is().False());
				AssertThat(static_cast<bool>(component), Is().False());
			});
			it("then the store contains the types it contains", [&]() { 
				AssertThat(store.has(typeid(int)), Is().True());
				AssertThat(store.has(typeid(float)), Is().True());
				AssertThat(store.has(typeid(char)), Is().True());
			});
			it("then the store does not contain the types it does not contain", [&]() { 
				AssertThat(store.has(typeid(long*)), Is().False());
			});
			it("then removing a component means the store no longer has that component", [&]() { 
				store.remove(typeid(int));
				AssertThat(store.has(typeid(int)), Is().False());
			});
		});
		
	});
});

