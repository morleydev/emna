#include <bandit/bandit.h>
#include <Emna/ec/Component.h>

go_bandit([](){
	using namespace bandit;
	
	describe("Given an constructed component", []() { 
		int expectedData = 124;
		emna::ec::Component<int> component(expectedData);
		
		it("then getting the data from a copy constructed component gives the expected value", [&]() { 
			AssertThat(emna::ec::Component<int>(component).getData(), Equals(expectedData));
		});
		it("then getting the data from an assignment constructed component gives the expected value", [&]() {
			emna::ec::Component<int> newComponent; newComponent = emna::ec::Component<long>(112);
			newComponent = component;
			AssertThat(newComponent.getData(), Equals(expectedData));
		});
		
		it("then getting the data from a move constructed component gives the expected value", [&]() { 
			emna::ec::Component<int> tmp(component);
			
			AssertThat(emna::ec::Component<int>(std::move(tmp)).getData(), Equals(expectedData));
		});
		it("then getting the data from a move assignment constructed component gives the expected value", [&]() { 
			emna::ec::Component<int> tmp(component);
			emna::ec::Component<int> newComponent(emna::ec::Component<long>(112));
			newComponent = std::move(tmp);
			
			AssertThat(newComponent.getData(), Equals(expectedData));
		});
		
		it("then getting the data from an assignment constructed component gives the expected value", [&]() {
			emna::ec::Component<int> newComponent; newComponent = std::move(emna::ec::Component<long>(112));
			newComponent = component;
			AssertThat(newComponent.getData(), Equals(expectedData));
		});
		
		it("then getting the data gives the expected value", [&]() { 
			AssertThat(component.getData(), Equals(expectedData));
		});
		
		describe("when updating the component", [&]() { 
			expectedData = 521;
			component.setData(expectedData);
			
			it("then getting the data gives the expected value", [&]() { 
				AssertThat(component.getData(), Equals(expectedData));
			});
		});
		
		describe("when turning the data into a void Component", [&]() {
			emna::ec::Component<void> voidComponent(component);
			
			it("then the an unassigned void component is false", [&]() { 
				emna::ec::Component<void> unassignedComponent;
				AssertThat(static_cast<bool>(unassignedComponent), Is().False());
			});
			it("then the void component is true", [&]() { 
				AssertThat(static_cast<bool>(voidComponent), Is().True());
			});
			it("then get type returns the integer", [&]() {
				AssertThat(&voidComponent.getType(), Equals(&typeid(int)));
			});
			it("then get data with an int returns the expected value", [&]() { 
				AssertThat(voidComponent.getData<int>(), Equals(expectedData));
			});
			
			it("then getting the data from a copy constructed component gives the expected value", [&]() { 
				emna::ec::Component<void> newComponent(component);
				AssertThat(newComponent.getData<int>(), Equals(expectedData));
				AssertThat(emna::ec::Component<void>(component).getData<int>(), Equals(expectedData));
				AssertThat(&newComponent.getType(), Equals(&typeid(int)));
			});
			it("then getting the data from an assignment constructed component gives the expected value", [&]() {
				emna::ec::Component<void> newComponent;
				newComponent = component;
				
				AssertThat(newComponent.getData<int>(), Equals(expectedData));
				AssertThat(&newComponent.getType(), Equals(&typeid(int)));
			});
			
			it("then getting the data from a move constructed component gives the expected value", [&]() { 
				emna::ec::Component<void> tmp(component);
				emna::ec::Component<void> newComponent(std::move(tmp));
				
				AssertThat(newComponent.getData<int>(), Equals(expectedData));
                AssertThat(&newComponent.getType(), Equals(&typeid(int)));
			});
			it("then getting the data from a move assignment constructed component gives the expected value", [&]() { 
				emna::ec::Component<void> tmp(component);
				emna::ec::Component<void> newComponent; newComponent = std::move(emna::ec::Component<long>(14332));
				newComponent = std::move(tmp);
				
				AssertThat(newComponent.getData<int>(), Equals(expectedData));
                AssertThat(&newComponent.getType(), Equals(&typeid(int)));
			});
			
			it("then getting the data from an assignment constructed component gives the expected value", [&]() {
				emna::ec::Component<void> newComponent; newComponent = emna::ec::Component<long>(14332);
				newComponent = component;
				component = emna::ec::Component<long>(14332);
				
				AssertThat(newComponent.getData<int>(), Equals(expectedData));
                AssertThat(&newComponent.getType(), Equals(&typeid(int)));
				
				component = emna::ec::Component<int>(expectedData);
			});
		});
	});
});
