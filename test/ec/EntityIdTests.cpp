#include <bandit/bandit.h>
#include <Emna/ec/EntityId.h>
#include <sstream>

go_bandit([]() {
	using namespace bandit;
	describe("EntityId:", [](){
		describe("Given an empty constructed EntityId", []() { 
			emna::ec::EntityId id;
        
			it("is equal to another empty constructed EntityId", [&]() {
				AssertThat(id, Equals(emna::ec::EntityId()));
			});
			it("is equal to an assignment constructed empty EntityId", [&]() {
				auto otherId = emna::ec::EntityId::generate();
				otherId = emna::ec::EntityId();
				AssertThat(id, Equals(otherId));
			});
			it("is not equal to a generated EntityId", [&]() {
				AssertThat(id, !Equals(emna::ec::EntityId::generate()));
			});
		});
		
		describe("Given a generated EntityId", []() { 
			auto id = emna::ec::EntityId::generate();
			
			it("Then is not equal to another generated EntityId", [&]() {
				AssertThat(id, !Equals(emna::ec::EntityId::generate()));
			});
			
			it("Then is equal itself", [&]() {
				AssertThat(id, Equals(id));
			});
			it("Then is equal to a copy constructed version of itself", [&]() {
				emna::ec::EntityId otherId(id);
				AssertThat(id, Equals(otherId));
			});
			it("Then is equal to an assignment constructed version of itself", [&]() {
				emna::ec::EntityId otherId;
				otherId = id;
				AssertThat(id, Equals(otherId));
			});
			
			it("Then it can write to a stream", [&]() { 
				std::stringstream stream;
				stream << id;
				AssertThat(stream.str(), !IsEmpty());
			});
		});
	});
});
