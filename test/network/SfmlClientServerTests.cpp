#ifndef EMNA_DISABLE_SFML

#include <Emna/network/SfmlClient.hpp>
#include <Emna/network/SfmlServer.hpp>
#include <Emna/util/make_unique.h>

#include <bandit/bandit.h>
#include <iostream>

namespace
{
    inline std::uint16_t getFreePort()
    {
	    sf::TcpListener listener;
	    listener.listen(sf::TcpListener::AnyPort);
	    return listener.getLocalPort();
    }
}

go_bandit([]() {
	using namespace bandit;
	bandit::describe("Given an Sfml Client and Sfml Server", []() {
		auto clientPtr = emna::util::make_unique<emna::network::SfmlClient>();
		emna::network::SfmlServer server;

		std::string ip = "127.0.0.1";
		std::uint16_t port = getFreePort();

		it("Then the client is not connected", [&]() {
			AssertThat((*clientPtr).isConnected(), Is().False());
		});
		it("Then the server is not listening", [&]() {
			AssertThat(server.isListening(), Is().False());
		});

		describe("when the server is listening", [&]() {
			server.listen(port);
			it("Then the server is listening", [&]() {
				AssertThat(server.isListening(), Is().True());
			});
		});

		std::int16_t clientId = -1;
		describe("when the client connects to a listening server", [&]() {
			auto connectResult = (*clientPtr).connect(ip, port);
			sf::sleep(sf::milliseconds(50));

			clientId = server.tryAccept();

			it("Then the connection attempt returns true", [&]() {
				AssertThat(connectResult, Is().True());
			});
			it("Then the client is connected", [&]() {
				AssertThat((*clientPtr).isConnected(), Is().True());
			});
			it("Then the server accepted the request", [&]() {
				AssertThat(clientId, Is().GreaterThan(0));
			});
			it("Then the server has the client in the list of connections", [&]() {
				AssertThat(server.getConnections(), Is().Containing(clientId));
			});
		});

		describe("when the client has sent no data to the server and trying to receive a message", [&]() {
			emna::network::Message receivedMessage;
			auto tryReceiveResult = server.tryReceive(clientId, receivedMessage);
			it("then no message was received", [&]() {
				AssertThat(tryReceiveResult, Is().False());
			});
		});

		describe("when data from a not connected client is attempted to be received", [&]() {
			emna::network::Message receivedMessage;
			auto tryReceiveResult = server.tryReceive(4, receivedMessage);
			it("then no message was received", [&]() {
				AssertThat(tryReceiveResult, Is().False());
			});
		});

		describe("when the server has sent no data to the client and trying to receive a message", [&]() {
			emna::network::Message receivedMessage;
			auto tryReceiveResult = (*clientPtr).tryReceive(receivedMessage);

			it("then no message was received", [&]() {
				AssertThat(tryReceiveResult, Is().False());
			});
		});

		describe("when the client sends data to the server", [&]() {

			emna::network::Message sendMessage;
			sendMessage.data = "some data";
			sendMessage.type = "messagetype";

			(*clientPtr).send(sendMessage, emna::network::PacketType::Infailable);

			sf::sleep(sf::milliseconds(50));

			emna::network::Message receivedMessage;
			auto tryReceiveResult = server.tryReceive(clientId, receivedMessage);
			
			it("then the message was received", [&]() {
				AssertThat(tryReceiveResult, Is().True());
			});
			it("then the message received contains the expected data", [&]() {
				AssertThat(receivedMessage.data, Is().EqualTo(sendMessage.data));
				AssertThat(receivedMessage.type, Is().EqualTo(sendMessage.type));
			});
		});

		describe("when the server sends data to the client", [&]() {
			emna::network::Message sendMessage;
			sendMessage.data = "some data";
			sendMessage.type = "messagetype";
			server.broadcast(sendMessage, emna::network::PacketType::Infailable);

			sf::sleep(sf::milliseconds(50));

			emna::network::Message receivedMessage;

			auto tryReceiveResult = (*clientPtr).tryReceive(receivedMessage);
			it("then the message was received", [&]() {
				AssertThat(tryReceiveResult, Is().True());
			});
			it("then the message received contains the expected data", [&]() {
				AssertThat(receivedMessage.data, Is().EqualTo(sendMessage.data));
				AssertThat(receivedMessage.type, Is().EqualTo(sendMessage.type));
			});
		});

		describe("when the client has disconnected and the server tries to receive a message", [&]() {
			clientPtr = std::move(std::unique_ptr<emna::network::SfmlClient>(new emna::network::SfmlClient));

			sf::sleep(sf::milliseconds(50));

			emna::network::Message receiveMessage;
			auto tryReceiveResult = server.tryReceive(clientId, receiveMessage);
			it("then the receive fails", [&]() {
				AssertThat(tryReceiveResult, Is().False());
			});
			it("then the client is no longer in the list of connected clients", [&]() {
				AssertThat(server.getConnections(), Is().Not().Containing(clientId));
			});
		});
		clientId = clientPtr->connect(ip, port);
	});
});

#endif//EMNA_DISABLE_SFML
