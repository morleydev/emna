#include <Emna/test/Mock.h>
#include <bandit/bandit.h>

namespace
{
	class TestMock
	{
	public:
		MockAction(Action1, int);
		MockAction(Action0);
		MockFunction(char, Function1, int);
		MockFunction(char, Function0);
	};
}

go_bandit([](){
	using namespace bandit;
	
	describe("Given Mock<void (void)>", []() {
		emna::test::Mock<void (void)> mock;
		before_each([&]() { 
			mock = emna::test::Mock<void (void)>();
		});
		
		describe("when mock has not been called", [&]() { 
			it("Then verify throws the expected exception", [&]() { 
				AssertThrows(emna::test::VerifyFailException, mock.verify());
			});
			it("then verifyTimes with zero call count does not throw any exception", [&]() {
				mock.verifyTimes(0);
			});
			it("then verifyTimes with too high a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(2));
			});
		});
		
		describe("when mock has been called", [&]() { 
			before_each([&]() {
				mock(); 
				mock(); 
				mock();
			});
			
			it("then verify does not throw any exception", [&]() {
				mock.verify();
			});
			it("then verifyTimes with the call count does not throw any exception", [&]() {
				mock.verifyTimes(3);
			});
			it("then verifyTimes with too low a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(2));
			});
			it("then verifyTimes with too high a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(5));
			});
		});
		
		describe("when mock has been called with a callback", [&]() { 
			emna::test::Mock<void (void)> otherMock;
			before_each([&]() {
				mock.setCallback([&]() { otherMock(); });
				mock();
			});
			
			it("then the callback was called", [&]() {
				otherMock.verifyTimes(1);
			});
		});
		
		describe("when mock has been called with a callback that throws", [&]() { 
			
			bool threwException;
			before_each([&]() {
				threwException = false;
				mock.setCallback([]() { throw std::exception(); });
				
				try
				{
					mock();
				}
				catch(const std::exception& e)
				{
					threwException = true;
				}
			});
			it("then the exception was thrown", [&]() { 
				AssertThat(threwException, Is().True());
			});
			it("then the mock was invoked", [&]() {
				mock.verifyTimes(1);
			});
		});
	});
	
	describe("Given Mock<int (void)>", []() {
		emna::test::Mock<int (void)> mock;
		int expectedReturnValue = 5;
		
		before_each([&]() { 
			mock = emna::test::Mock<int (void)>();
			
			mock.setReturn(expectedReturnValue);
		});
		
		describe("when mock has not been called", [&]() { 
			it("Then verify throws the expected exception", [&]() { 
				AssertThrows(emna::test::VerifyFailException, mock.verify());
			});
			it("then verifyTimes with zero call count does not throw any exception", [&]() {
				mock.verifyTimes(0);
			});
			it("then verifyTimes with too high a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(2));
			});
		});
		
		describe("when mock has been called", [&]() { 
			
			int returnCodes[3];
			
			before_each([&]() {
				returnCodes[0] = mock();
				returnCodes[1] = mock();
				returnCodes[2] = mock();
			});
			
			it("then the expected return values are returned", [&]() { 
				AssertThat(returnCodes[0], Equals(expectedReturnValue)); 
				AssertThat(returnCodes[1], Equals(expectedReturnValue)); 
				AssertThat(returnCodes[2], Equals(expectedReturnValue));
			});
			
			it("then verify does not throw any exception", [&]() {
				mock.verify();
			});
			it("then verifyTimes with the call count does not throw any exception", [&]() {
				mock.verifyTimes(3);
			});
			it("then verifyTimes with too low a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(2));
			});
			it("then verifyTimes with too high a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(5));
			});
		});
		
		describe("when mock has been called with a callback and return function", [&]() { 
			emna::test::Mock<int (void)> otherMock, returnMock;
			int expectedReturn = 542;
			int actualReturn = 0;
			before_each([&]() { 
				otherMock = emna::test::Mock<int (void)>();
				returnMock = emna::test::Mock<int (void)>();
			
				returnMock.setReturn(expectedReturn);
				mock.setCallback([&]() { otherMock(); });
				mock.setReturn([&]() { return returnMock(); });
				actualReturn = mock();
			});
			it("then the callback was called", [&]() {
				otherMock.verifyTimes(1);
			});
			it("then the return function was called", [&]() { 
				returnMock.verifyTimes(1);
			});
			it("then the expected value was returned", [&]() { 
				AssertThat(actualReturn, Equals(expectedReturn));
			});
		});
	});
	
	describe("Given Mock<void (int, char)>", []() {
		emna::test::Mock<void (int, char)> mock;
		
		before_each([&]() { 
			mock = emna::test::Mock<void (int, char)>();
		});
		
		describe("when mock has not been called", [&]() { 
			it("then verify throws the expected exception", [&]() { 
				AssertThrows(emna::test::VerifyFailException, mock.verify(12, 'z'));
			});
			it("then verifyTimes with zero call count does not throw any exception", [&]() {
				mock.verifyTimes(0, 'q', 52);
			});
			it("then verifyTimes with too high a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(2, 'z', 12));
			});
		});
		
		describe("when mock has been called", [&]() { 
			before_each([&]() {
				mock(12, 'a'); 
				mock(12, 'a'); 
				mock(15, 'z');
			});
			
			it("then verify with called arguments does not throw any exception", [&]() {
				mock.verify(12, 'a');
				mock.verify(15, 'z');
			});
			it("then verifyTimes with the call count and correct arguments does not throw any exception", [&]() {
				mock.verifyTimes(2, 12, 'a');
				mock.verifyTimes(1, 15, 'z');
			});
			it("then verifyTimes with incorrect arguments throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(2, 25, 'd'));
			});
			it("then verifyTimes with too low a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(1, 12, 'a'));
			});
			it("then verifyTimes with too high a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(5, 15, 'z'));
			});
		});
		
		describe("when mock has been called with a callback", [&]() { 
			emna::test::Mock<void (int, char)> otherMock;
			
			before_each([&]() { 
				otherMock = emna::test::Mock<void (int, char)>();
				mock.setCallback([&](int n, char c) { otherMock(n, c); });
				mock(5, 2);
			});
			it("then the callback was called", [&]() {
				otherMock.verifyTimes(1, 5, 2);
			});
		});
		
		describe("when mock has been called and then is verified with functors", [&]() { 
			
			emna::test::Mock<void (int)> functorMock;
			before_each([&]() {
				mock(12, 'a');
				mock(12, 'z');
				mock(12, 'q');
				mock(12, 'p');
				mock(15, 'a');
				mock(15, 'e');
				
				mock.verify([&](int c) { functorMock(c); return true; }, 'a');
				mock.verifyTimes(2, [&](int c) { functorMock(c); return true; }, 'a');
			});
			it("then the callback was called", [&]() {
				functorMock.verifyTimes(8, 12);
				functorMock.verifyTimes(4, 15);
			});
		});
	});
	
	describe("Given Mock<int (int, char)>", []() {
		emna::test::Mock<int (int, char)> mock;
		int expectedReturnValue = 8;
		
		before_each([&]() { 
			mock = emna::test::Mock<int (int, char)>();
			mock.setReturn(expectedReturnValue);
		});
		
		describe("when mock has not been called", [&]() { 
			it("then verify throws the expected exception", [&]() { 
				AssertThrows(emna::test::VerifyFailException, mock.verify(12, 'z'));
			});
			it("then verifyTimes with zero call count does not throw any exception", [&]() {
				mock.verifyTimes(0, 'q', 52);
			});
			it("then verifyTimes with too high a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(2, 'z', 12));
			});
		});
		
		describe("when mock has been called", [&]() { 
			int returnCodes[3];
			before_each([&]() {
				returnCodes[0] = mock(12, 'a');
				returnCodes[1] = mock(12, 'a');
				returnCodes[2] = mock(15, 'z');
			});
			it("then the expected return values are returned", [&]() { 
				AssertThat(returnCodes[0], Equals(expectedReturnValue)); 
				AssertThat(returnCodes[1], Equals(expectedReturnValue)); 
				AssertThat(returnCodes[2], Equals(expectedReturnValue));
			});
			it("then verify with called arguments does not throw any exception", [&]() {
				mock.verify(12, 'a');
				mock.verify(15, 'z');
			});
			it("then verifyTimes with the call count and correct arguments does not throw any exception", [&]() {
				mock.verifyTimes(2, 12, 'a');
				mock.verifyTimes(1, 15, 'z');
			});
			it("then verifyTimes with incorrect arguments throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(2, 25, 'd'));
			});
			it("then verifyTimes with too low a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(1, 12, 'a'));
			});
			it("then verifyTimes with too high a call count throws the expected exception", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(5, 15, 'z'));
			});
		});
		
		describe("when mock has been called with functors", [&]() { 
			
			emna::test::Mock<void (char)> functorMock;
			before_each([&]() { 
				functorMock = emna::test::Mock<void (char)>();
				mock(12, 'a'); 
				mock(12, 'a'); 
				mock(15, 'z');
				
				mock.verify(12, [&](char c) { functorMock(c); return true; });
				mock.verifyTimes(2, 12, [&](char c) { functorMock(c); return true; });
			});
			it("then the callback was called", [&]() {
				functorMock.verifyTimes(4, 'a');
				functorMock.verifyTimes(2, 'z');
			});
		});
		
		describe("when mock has been called with a callback and return function", [&]() { 
			emna::test::Mock<int (int, char)> otherMock, returnMock;
			int expectedReturn = 52;
			int actualReturn;
			
			before_each([&]() {
				otherMock = emna::test::Mock<int (int, char)>();
				returnMock = emna::test::Mock<int (int, char)>();
				
				returnMock.setReturn(expectedReturn);
				
				mock.setCallback([&](int n, char c) { otherMock(n, c); });
				mock.setReturn([&](int n, char c) { return returnMock(n, c); });
				actualReturn = mock(5, 2);
			});
			it("then the callback was called", [&]() {
				otherMock.verifyTimes(1, 5, 2);
			});
			it("then the return function was called", [&]() { 
				returnMock.verifyTimes(1, 5, 2);
			});
			it("then the expected value was returned", [&]() { 
				AssertThat(actualReturn, Equals(expectedReturn));
			});
		});
	});

	describe("Given a mock that takes a non-native POD value", [&]() {
		struct POD
		{
			POD(int a, float b, char c) : x(a), y(b), z(c) { }

			int x;
			float y;
			char z;

			bool operator==(const POD& other) const
			{
				return x == other.x && y == other.y && z == other.z;
			}
		};

		emna::test::Mock<void (POD)> mock;
		describe("When the mock is invoked with the POD", [&]() {
			POD actualP(12, 1.0f, 'a');
			mock(actualP);

			it("Then the mock can be verified", [&]() {
				mock.verify(actualP);
				mock.verifyTimes(1, actualP);
				mock.verifyTimes(0, POD(10, 0.1f, 'b'));
			});

			it("Then the mock throws the expected exception when verification fails", [&]() {
				AssertThrows(emna::test::VerifyFailException, mock.verify(POD(12, 1.0f, 'b')));

				AssertThrows(emna::test::VerifyFailException, mock.verifyTimes(1, POD(12, 1.0f, 'b')));
			});
		});
	});

	describe("Given a mocked class", []() { 
		TestMock mock;
		describe("when functions are called on that class", [&]() { 
			mock.Action1(5);
			mock.Action0();
			mock.Function1(5);
			mock.Function0();
			
			it("then the mocks can be verified", [&]() {
				mock.mockAction1.verify(5);
				mock.mockAction0.verify();
				mock.mockFunction1.verify(5);
				mock.mockFunction0.verify();
			});
		});
	});
});
