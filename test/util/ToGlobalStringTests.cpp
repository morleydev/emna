#include <bandit/bandit.h>
#include <Emna/util/to_global_string.h>
#include <string>

go_bandit([]() {
	bandit::describe("to_global_string:", []() { 
		bandit::it("Storing a string returns a c_str which contains the same information", []() {
			std::string data = "some string data";
			const char* pData = emna::util::to_global_string(data);
			
			AssertThat(std::string(pData), Equals(data));
		});
		bandit::it("A stored string is constant even if the source changes", []() {
			std::string data = "some string data";
			const char* pData = emna::util::to_global_string(data);
			std::string dataCopy = data;
			data = "Some other string";
			
			AssertThat(std::string(pData), Equals(dataCopy));
		});
	});
});
