#include <Emna/util/log.h>
#include <bandit/bandit.h>
#include <sstream>
#include <iostream>

go_bandit([]() {
	using namespace bandit;

	describe("Given output and error streams for logging", []() {
		std::stringstream logStream;
		std::stringstream errStream;

		before_each([&]() {
			logStream.str("");
			errStream.str("");

			emna::util::setLogStream(logStream);
			emna::util::setErrStream(errStream);
		});
		after_each([&]() {
			emna::util::setLogStream(std::cout);
			emna::util::setErrStream(std::cerr);
		});

		describe("When writing a debug log", [&]() {
			before_each([&]() {
				emna::util::log(emna::util::log_level::debug, "expected", "message", 5);
			});

#if defined(EMNA_DEBUG) || defined(DEBUG) || defined(_DEBUG)
			it("then the expected message is written to the logStream", [&]() {
				AssertThat(logStream.str(), Is().EqualTo("[DEBUG] expectedmessage5\n"));
			});
#else
			it("then nothing is written to the logStream", [&]() {
				AssertThat(logStream.str(), Is().Empty());
			});
#endif
		});

		describe("When writing a message log", [&]() {
			before_each([&]() {
				emna::util::log(emna::util::log_level::message, "expected", "message", 5);
			});

			it("then the expected message is written to the logStream", [&]() {
				AssertThat(logStream.str(), Is().EqualTo("[MSG] expectedmessage5\n"));
			});
		});

		describe("When writing a warning log", [&]() {
			before_each([&]() {
				emna::util::log(emna::util::log_level::warning, "expected", "message", 5);
			});

			it("then the expected message is written to the logStream", [&]() {
				AssertThat(logStream.str(), Is().EqualTo("[WARN] expectedmessage5\n"));
			});
		});

		describe("When writing an error log", [&]() {
			before_each([&]() {
				emna::util::log(emna::util::log_level::error, "expected", "message", 5);
			});

			it("then the expected message is written to the logStream", [&]() {
				AssertThat(errStream.str(), Is().EqualTo("[ERR] expectedmessage5\n"));
			});
		});
	});
});
