#include <bandit/bandit.h>

int main(const int nArgs, char** ppcArgs)
{
	return bandit::run(nArgs, ppcArgs);
}
