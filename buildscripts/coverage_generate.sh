#!/bash

cmake $1 -DCMAKE_BUILD_TYPE=Debug -DDISABLE_SFML=ON -DDISABLE_SDL=ON -DENABLE_GCOV=ON
cmake --build .
lcov --directory . --zerocounters
ctest
lcov --directory . --capture --output-file full-app.info
lcov --remove full-app.info '*test*' '*ext*' '/usr/*' --output-file app.info
mkdir -p html
pushd html
	genhtml ../app.info
popd
