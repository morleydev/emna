mkdir ext
pushd ext
	mkdir include
	pushd include
		mkdir bandit
	popd
	mkdir lib
	mkdir bin
popd

git clone https://github.com/joakimkarlsson/bandit bandit_tmp
xcopy bandit_tmp\bandit\* ext\include\bandit\* /s /y
rd bandit_tmp /s /q

git clone https://github.com/USCiLab/cereal cereal_tmp
xcopy cereal_tmp\include\* ext\include\ /s /y
rd cereal_tmp /s /q

git clone https://git01.codeplex.com/cpplinq cpplinq_tmp
xcopy cpplinq_tmp\CppLinq\*.hpp ext\include\ /s /y
rd cpplinq_tmp /s /q
