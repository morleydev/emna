#!/bin/bash

mkdir -p ext
mkdir -p ext/include
mkdir -p ext/lib
mkdir -p ext/bin

git clone https://github.com/joakimkarlsson/bandit bandit_tmp
cp bandit_tmp/bandit ext/include/ -r
rm bandit_tmp -rf

git clone https://github.com/USCiLab/cereal cereal_tmp
cp cereal_tmp/include/* ext/include/ -r
rm cereal_tmp -rf
	
git clone https://git01.codeplex.com/cpplinq cpplinq_tmp
cp cpplinq_tmp/CppLinq/*.hpp ext/include/ -r
rm cpplinq_tmp -rf
