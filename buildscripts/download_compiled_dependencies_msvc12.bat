git clone https://github.com/LaurentGomila/SFML sfml_source --depth 1

mkdir sfml_build
pushd sfml_build
	cmake ../sfml_source -G"Visual Studio 12"
	cmake --build . -- /p:Configuration=%1 /m:10
popd

xcopy sfml_source\include\* ext\include\ /y /s
xcopy sfml_build\lib\%1\*.lib ext\lib\ /y /s
xcopy sfml_build\lib\%1\*.pdb ext\lib\ /y /s
xcopy sfml_build\lib\%1\*.dll ext\bin\ /y /s
xcopy sfml_source\extlibs\bin\x86\*.dll ext\bin\ /y /s

rd sfml_build /s /q
rd sfml_source /s /q

