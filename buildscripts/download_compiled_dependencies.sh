git clone https://github.com/LaurentGomila/SFML sfml_source --depth 1

mkdir -p sfml_build
pushd sfml_build
	cmake ../sfml_source -DCMAKE_BUILD_TYPE=$1
	cmake --build . 
popd

cp ./sfml_source/include/* ./ext/include/ -r
cp ./sfml_build/lib/*.so* ./ext/bin/ -r

rm sfml_build -rf
rm sfml_source -rf

