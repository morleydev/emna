#ifndef EMNA_STATE_H_INCLUDED
#define EMNA_STATE_H_INCLUDED

#include "configure.h"
#include "GraphicsDriver.h"
#include "ControllerStore.h"
#include "ViewStore.h"
#include <chrono>

namespace emna
{
	class State
	{
	protected:
		ControllerStore controllers;
		ViewStore views;

	public:
		EMNA_EXTERN virtual ~State();

		EMNA_EXTERN virtual std::size_t update(std::chrono::microseconds deltaTime) = 0;
		EMNA_EXTERN virtual void draw(GraphicsDriver& graphics);
	};
}

#endif // EMNA_STATE_H_INCLUDED
