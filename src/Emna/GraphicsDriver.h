#ifndef EMNA_GRAPHICSDRIVER_H_INCLUDED
#define EMNA_GRAPHICSDRIVER_H_INCLUDED

#include "configure.h"
#include "content/Texture.h"
#include "maths/point2.h"

#include <memory>

namespace emna
{
    class GraphicsDriver
    {
    private:
	    std::size_t m_width;
	    std::size_t m_height;
	    std::size_t m_viewWidth;
	    std::size_t m_viewHeight;

    public:
	    EMNA_EXTERN GraphicsDriver();
	    EMNA_EXTERN virtual ~GraphicsDriver();

	    GraphicsDriver(const GraphicsDriver &) = delete;
	    GraphicsDriver &operator=(const GraphicsDriver &) = delete;

	    virtual void clear() = 0;
	    virtual void draw(maths::point2f pos, maths::point2f size,
	                      maths::point2i texturePos, maths::point2i textureSize,
	                      const content::Texture &) = 0;
	    virtual void flush() = 0;

	    inline std::size_t getWidth() const { return m_width; }
	    inline std::size_t getHeight() const { return m_height; }

	    inline std::size_t getViewWidth() const { return m_viewWidth; }
	    inline std::size_t getViewHeight() const { return m_viewHeight; }

    protected:
	    inline void setWidth(std::size_t w) { m_width = w; }
	    inline void setHeight(std::size_t h) { m_height = h; }

	    inline void setViewWidth(std::size_t w) { m_viewWidth = w; }
	    inline void setViewHeight(std::size_t h) { m_viewHeight = h; }
    };
}

#endif // EMNA_GRAPHICSDRIVER_H_INCLUDED
