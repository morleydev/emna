#include "EventQueue.h"

void emna::EventQueue::_push(const std::type_info& type, std::shared_ptr<void> data)
{
	m_queue.push_back(std::make_pair(&type, std::move(data)));
}

bool emna::EventQueue::pop(std::pair<const std::type_info*, std::shared_ptr<void>>& out)
{
	if ( m_queue.empty() )
		return false;

	auto data = m_queue.front();
	m_queue.pop_front();	out.first = data.first;	out.second = data.second;	return true;}
