#ifndef EMNA_AUDIODRIVER_H_INCLUDED
#define EMNA_AUDIODRIVER_H_INCLUDED

#include "configure.h"
#include "content/MusicStream.h"
#include "content/SoundEffect.h"
#include "maths/point3.h"
#include <chrono>

namespace emna
{
    class AudioDriver
    {
    public:
	    EMNA_EXTERN virtual ~AudioDriver();

	    virtual void setListenerPosition(maths::point3f position) = 0;
	    virtual maths::point3f getListenerPosition() const = 0;

	    virtual void setGlobalVolume(float volume) = 0;
	    virtual float getGlobalVolume() const = 0;

	    virtual void play(content::SoundEffect& music, maths::point3f position) = 0;
	    virtual void play(content::MusicStream& music) = 0;

	    virtual void update(std::chrono::microseconds deltaTime) = 0;
    };
}

#endif // EMNA_AUDIODRIVER_H_INCLUDED
