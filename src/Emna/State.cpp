#include "State.h"

emna::State::~State()
{
}

std::size_t emna::State::update(std::chrono::microseconds deltaTime)
{
	controllers.tick(deltaTime);
	return 0;
}

void emna::State::draw(emna::GraphicsDriver &graphics)
{
	views.draw(graphics);
}
