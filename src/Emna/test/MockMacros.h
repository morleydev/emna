#ifndef EMNA_MOCK_MOCKMACROS_HPP
#define EMNA_MOCK_MOCKMACROS_HPP

#ifdef __COUNTER__
#define EMNA_MOCK_UNIQUE_NUMBER __COUNTER__
#else
#define EMNA_MOCK_UNIQUE_NUMBER __LINE__
#endif


// Work around for Visual Studio compiler bug where variadic macros are not correctly expanded
#ifdef _MSC_VER
#define EMNA_MOCK_VA_NUM_ARGS_HELPER(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, N, ...)	N
#define EMNA_MOCK_VA_NUM_ARGS_REVERSE_SEQUENCE 10, 9, 8, 7, 6, 5, 4, 3, 2, 1
#define EMNA_MOCK_LEFT_PARENTHESIS (
#define EMNA_MOCK_RIGHT_PARENTHESIS )
#define EMNA_MOCK_VA_NARGS(...)						EMNA_MOCK_VA_NUM_ARGS_HELPER EMNA_MOCK_LEFT_PARENTHESIS __VA_ARGS__, EMNA_MOCK_VA_NUM_ARGS_REVERSE_SEQUENCE EMNA_MOCK_RIGHT_PARENTHESIS

#else
#define EMNA_MOCK_VA_NARGS_IMPL(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, N, ...) N
#define EMNA_MOCK_VA_NARGS(...) EMNA_MOCK_VA_NARGS_IMPL(__VA_ARGS__, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
#endif

#define EMNA_MOCK_MACRO_CONCAT_IMPL(func, suffix) func##suffix
#define EMNA_MOCK_MACRO_CONCAT(func, suffix) EMNA_MOCK_MACRO_CONCAT_IMPL(func, suffix)

#define EMNA_MOCK_MOCK_ACTION_1(Name) mutable ::emna::test::Mock<void (void)> mock##Name; virtual void Name() { mock##Name(); }
#define EMNA_MOCK_MOCK_ACTION_2(Name, Arg1) mutable ::emna::test::Mock<void (Arg1)> mock##Name; virtual void Name(Arg1 a1) { mock##Name(a1); }
#define EMNA_MOCK_MOCK_ACTION_3(Name, Arg1, Arg2) mutable ::emna::test::Mock<void (Arg1, Arg2)> mock##Name; virtual void Name(Arg1 a1, Arg2 a2) { mock##Name(a1, a2); }
#define EMNA_MOCK_MOCK_ACTION_4(Name, Arg1, Arg2, Arg3) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3)> mock##Name; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3) { mock##Name(a1, a2, a3); }
#define EMNA_MOCK_MOCK_ACTION_5(Name, Arg1, Arg2, Arg3, Arg4) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4)> mock##Name; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4) { mock##Name(a1, a2, a3, a4); }
#define EMNA_MOCK_MOCK_ACTION_6(Name, Arg1, Arg2, Arg3, Arg4, Arg5) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5)> mock##Name; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5) { mock##Name(a1, a2, a3, a4, a5); }
#define EMNA_MOCK_MOCK_ACTION_7(Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)> mock##Name; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6) { mock##Name(a1, a2, a3, a4, a5, a6); }
#define EMNA_MOCK_MOCK_ACTION_8(Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)> mock##Name; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7) { mock##Name(a1, a2, a3, a4, a5, a6, a7); }
#define EMNA_MOCK_MOCK_ACTION_9(Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)> mock##Name; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7, Arg8 a8) { mock##Name(a1, a2, a3, a4, a5, a6, a7, a8, a9); }
#define EMNA_MOCK_MOCK_ACTION_10(Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9)> mock##Name; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7, Arg8 a8, Arg9 a9) { mock##Name(a1, a2, a3, a4, a5, a6, a7, a8, a9); }

#define EMNA_MOCK_MOCK_FUNCTION_2(Return, Name) mutable ::emna::test::Mock<Return (void)> mock##Name; virtual Return Name() { return mock##Name(); }
#define EMNA_MOCK_MOCK_FUNCTION_3(Return, Name, Arg1) mutable ::emna::test::Mock<Return (Arg1)> mock##Name; virtual Return Name(Arg1 a1) { return mock##Name(a1); }
#define EMNA_MOCK_MOCK_FUNCTION_4(Return, Name, Arg1, Arg2) mutable ::emna::test::Mock<Return (Arg1, Arg2)> mock##Name; virtual Return Name(Arg1 a1, Arg2 a2) { return mock##Name(a1, a2); }
#define EMNA_MOCK_MOCK_FUNCTION_5(Return, Name, Arg1, Arg2, Arg3) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3)> mock##Name; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3) { return mock##Name(a1, a2, a3); }
#define EMNA_MOCK_MOCK_FUNCTION_6(Return, Name, Arg1, Arg2, Arg3, Arg4) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4)> mock##Name; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4) { return mock##Name(a1, a2, a3, a4); }
#define EMNA_MOCK_MOCK_FUNCTION_7(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5)> mock##Name; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5) { return mock##Name(a1, a2, a3, a4, a5); }
#define EMNA_MOCK_MOCK_FUNCTION_8(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)> mock##Name; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6) { return mock##Name(a1, a2, a3, a4, a5, a6); }
#define EMNA_MOCK_MOCK_FUNCTION_9(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)> mock##Name; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7) { return mock##Name(a1, a2, a3, a4, a5, a6, a7); }
#define EMNA_MOCK_MOCK_FUNCTION_10(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)> mock##Name; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7, Arg8 a8) { return mock##Name(a1, a2, a3, a4, a5, a6, a7, a8, a9); }
#define EMNA_MOCK_MOCK_FUNCTION_11(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9)> mock##Name; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7, Arg8 a8, Arg9 a9) { return mock##Name(a1, a2, a3, a4, a5, a6, a7, a8, a9); }

#define EMNA_MOCK_MOCK_ACTION_CONST_1(Name) mutable ::emna::test::Mock<void (void)> mock##Name##Const; virtual void Name() const { mock##Name##Const(); }
#define EMNA_MOCK_MOCK_ACTION_CONST_2(Name, Arg1) mutable ::emna::test::Mock<void (Arg1)> mock##Name##Const; virtual void Name(Arg1 a1) const { mock##Name##Const(a1); }
#define EMNA_MOCK_MOCK_ACTION_CONST_3(Name, Arg1, Arg2) mutable ::emna::test::Mock<void (Arg1, Arg2)> mock##Name##Const; virtual void Name(Arg1 a1, Arg2 a2) const { mock##Name##Const(a1, a2); }
#define EMNA_MOCK_MOCK_ACTION_CONST_4(Name, Arg1, Arg2, Arg3) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3)> mock##Name##Const; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3) const { mock##Name##Const(a1, a2, a3); }
#define EMNA_MOCK_MOCK_ACTION_CONST_5(Name, Arg1, Arg2, Arg3, Arg4) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4)> mock##Name##Const; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4) const { mock##Name##Const(a1, a2, a3, a4); }
#define EMNA_MOCK_MOCK_ACTION_CONST_6(Name, Arg1, Arg2, Arg3, Arg4, Arg5) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5)> mock##Name##Const; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5) const { mock##Name##Const(a1, a2, a3, a4, a5); }
#define EMNA_MOCK_MOCK_ACTION_CONST_7(Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)> mock##Name##Const; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6) const { mock##Name##Const(a1, a2, a3, a4, a5, a6); }
#define EMNA_MOCK_MOCK_ACTION_CONST_8(Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)> mock##Name##Const; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7) const { mock##Name##Const(a1, a2, a3, a4, a5, a6, a7); }
#define EMNA_MOCK_MOCK_ACTION_CONST_9(Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)> mock##Name##Const; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7, Arg8 a8) const { mock##Name##Const(a1, a2, a3, a4, a5, a6, a7, a8, a9); }
#define EMNA_MOCK_MOCK_ACTION_CONST_10(Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9) mutable ::emna::test::Mock<void (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9)> mock##Name##Const; virtual void Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7, Arg8 a8, Arg9 a9) const { mock##Name##Const(a1, a2, a3, a4, a5, a6, a7, a8, a9); }

#define EMNA_MOCK_MOCK_FUNCTION_CONST_2(Return, Name) mutable ::emna::test::Mock<Return (void)> mock##Name##Const; virtual Return Name() const { return mock##Name##Const(); }
#define EMNA_MOCK_MOCK_FUNCTION_CONST_3(Return, Name, Arg1) mutable ::emna::test::Mock<Return (Arg1)> mock##Name##Const; virtual Return Name(Arg1 a1) const { return mock##Name##Const(a1); }
#define EMNA_MOCK_MOCK_FUNCTION_CONST_4(Return, Name, Arg1, Arg2) mutable ::emna::test::Mock<Return (Arg1, Arg2)> mock##Name##Const; virtual Return Name(Arg1 a1, Arg2 a2) const { return mock##Name##Const(a1, a2); }
#define EMNA_MOCK_MOCK_FUNCTION_CONST_5(Return, Name, Arg1, Arg2, Arg3) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3)> mock##Name##Const; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3) const { return mock##Name##Const(a1, a2, a3); }
#define EMNA_MOCK_MOCK_FUNCTION_CONST_6(Return, Name, Arg1, Arg2, Arg3, Arg4) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4)> mock##Name##Const; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4) const { return mock##Name##Const(a1, a2, a3, a4); }
#define EMNA_MOCK_MOCK_FUNCTION_CONST_7(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5)> mock##Name##Const; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5) const { return mock##Name##Const(a1, a2, a3, a4, a5); }
#define EMNA_MOCK_MOCK_FUNCTION_CONST_8(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)> mock##Name##Const; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6) const { return mock##Name##Const(a1, a2, a3, a4, a5, a6); }
#define EMNA_MOCK_MOCK_FUNCTION_CONST_9(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)> mock##Name##Const; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7) const { return mock##Name##Const(a1, a2, a3, a4, a5, a6, a7); }
#define EMNA_MOCK_MOCK_FUNCTION_CONST_10(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)> mock##Name##Const; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7, Arg8 a8) const { return mock##Name##Const(a1, a2, a3, a4, a5, a6, a7, a8, a9); }
#define EMNA_MOCK_MOCK_FUNCTION_CONST_11(Return, Name, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9) mutable ::emna::test::Mock<Return (Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9)> mock##Name##Const; virtual Return Name(Arg1 a1, Arg2 a2, Arg3 a3, Arg4 a4, Arg5 a5, Arg6 a6, Arg7 a7, Arg8 a8, Arg9 a9) const { return mock##Name##Const(a1, a2, a3, a4, a5, a6, a7, a8, a9); }

#ifdef _MSC_VER
#define EMNA_MOCK_MOCK_ACTION_N(N, ...) EMNA_MOCK_MACRO_CONCAT(EMNA_MOCK_MOCK_ACTION_, N) EMNA_MOCK_LEFT_PARENTHESIS __VA_ARGS__ EMNA_MOCK_RIGHT_PARENTHESIS
#define EMNA_MOCK_MOCK_FUNCTION_N(N, ...) EMNA_MOCK_MACRO_CONCAT(EMNA_MOCK_MOCK_FUNCTION_, N) EMNA_MOCK_LEFT_PARENTHESIS __VA_ARGS__ EMNA_MOCK_RIGHT_PARENTHESIS

#define EMNA_MOCK_MOCK_ACTION_CONST_N(N, ...) EMNA_MOCK_MACRO_CONCAT(EMNA_MOCK_MOCK_ACTION_CONST_, N) EMNA_MOCK_LEFT_PARENTHESIS __VA_ARGS__ EMNA_MOCK_RIGHT_PARENTHESIS
#define EMNA_MOCK_MOCK_FUNCTION_CONST_N(N, ...) EMNA_MOCK_MACRO_CONCAT(EMNA_MOCK_MOCK_FUNCTION_CONST_, N) EMNA_MOCK_LEFT_PARENTHESIS __VA_ARGS__ EMNA_MOCK_RIGHT_PARENTHESIS

#else
#define EMNA_MOCK_MOCK_ACTION_N(N, ...) EMNA_MOCK_MACRO_CONCAT(EMNA_MOCK_MOCK_ACTION_, N)(__VA_ARGS__)
#define EMNA_MOCK_MOCK_FUNCTION_N(N, ...) EMNA_MOCK_MACRO_CONCAT(EMNA_MOCK_MOCK_FUNCTION_, N)(__VA_ARGS__)

#define EMNA_MOCK_MOCK_ACTION_CONST_N(N, ...) EMNA_MOCK_MACRO_CONCAT(EMNA_MOCK_MOCK_ACTION_CONST_, N)(__VA_ARGS__)
#define EMNA_MOCK_MOCK_FUNCTION_CONST_N(N, ...) EMNA_MOCK_MACRO_CONCAT(EMNA_MOCK_MOCK_FUNCTION_CONST_, N)(__VA_ARGS__)
#endif

/*! \brief A mock action with no return values (Name, Arguments...) */
#define MockAction(...) EMNA_MOCK_MOCK_ACTION_N(EMNA_MOCK_VA_NARGS(__VA_ARGS__), __VA_ARGS__)

/*! \brief A mock function with a return value (Return, Name, Arguments...) */
#define MockFunction(...) EMNA_MOCK_MOCK_FUNCTION_N(EMNA_MOCK_VA_NARGS(__VA_ARGS__), __VA_ARGS__)

/*! \brief A mock const action with no return values (Name, Arguments...) */
#define MockActionConst(...) EMNA_MOCK_MOCK_ACTION_CONST_N(EMNA_MOCK_VA_NARGS(__VA_ARGS__), __VA_ARGS__)

/*! \brief A mock const function with a return value (Return, Name, Arguments...) */
#define MockFunctionConst(...) EMNA_MOCK_MOCK_FUNCTION_CONST_N(EMNA_MOCK_VA_NARGS(__VA_ARGS__), __VA_ARGS__)

#endif // EMNA_MOCK_MOCKMACROS_HPP