#ifndef EMNA_MOCKS_MOCKSTATE_HPP_INCLUDED
#define EMNA_MOCKS_MOCKSTATE_HPP_INCLUDED

#include "../../State.h"
#include "../Mock.h"

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			class MockState : public State
			{
			public:
				MockFunction(std::size_t, update, std::chrono::microseconds);
	
				test::Mock<void (GraphicsDriver*)> mockdraw;
				virtual void draw(GraphicsDriver& graphics) { mockdraw(&graphics); }
			};
		}
	}
}

#endif//EMNA_MOCKS_MOCKSTATE_HPP_INCLUDED
