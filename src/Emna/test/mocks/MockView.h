#ifndef EMNA_MOCKS_MOCKVIEW_HPP_INCLUDED
#define EMNA_MOCKS_MOCKVIEW_HPP_INCLUDED

#include "../../View.h"
#include "../Mock.h"

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			template<unsigned> class MockView : public emna::View
			{
			public:
				MockView(std::int_least32_t depth) 
				: mockdraw(new emna::test::Mock<void (emna::GraphicsDriver *)>()) 
				{
					setDepth(depth);
				}
				
				MockView(std::int_least32_t depth, emna::test::Mock<void (emna::GraphicsDriver *)> *mockDraw) 
				: mockdraw(mockDraw, [](emna::test::Mock<void (emna::GraphicsDriver *)>*) { }) 
				{
					setDepth(depth);
				}
				virtual ~MockView() { }
		
				std::shared_ptr< emna::test::Mock<void (emna::GraphicsDriver *)>> mockdraw;
				virtual void draw(emna::GraphicsDriver& graphics) { (*mockdraw)(&graphics); }
			};
		}
	}
}

#endif//EMNA_MOCKS_MOCKVIEW_HPP_INCLUDED
