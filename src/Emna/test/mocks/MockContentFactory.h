#ifndef EMNA_TEST_MOCKS_MOCKCONTENTFACTORY_H_INCLUDED
#define EMNA_TEST_MOCKS_MOCKCONTENTFACTORY_H_INCLUDED

#include "../../ContentFactory.h"
#include "../Mock.h"

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			class MockContentFactory : public ContentFactory
			{
			public:
				MockFunction(std::unique_ptr<content::Texture>, loadTexture, std::string);
				MockFunction(std::unique_ptr<content::SoundEffect>, loadSoundEffect, std::string);
				MockFunction(std::unique_ptr<content::MusicStream>, openMusicStream, std::string);
				MockFunction(std::unique_ptr<std::istream>, openFileStream, std::string);
			};
		}
	}
}

#endif//EMNA_TEST_MOCKS_MOCKCONTENTFACTORY_H_INCLUDED
