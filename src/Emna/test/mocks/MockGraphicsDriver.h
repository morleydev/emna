#ifndef EMNA_MOCKS_MOCKGRAPHICSDRIVER_H_INCLUDED
#define EMNA_MOCKS_MOCKGRAPHICSDRIVER_H_INCLUDED

#include "../../GraphicsDriver.h"
#include "../Mock.h"

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			class MockGraphicsDriver : public emna::GraphicsDriver
			{
			public:
				virtual ~MockGraphicsDriver() { }
	
				MockAction(clear);
				
				Mock<void (emna::maths::point2f pos, emna::maths::point2f size, emna::maths::point2i texturePos, emna::maths::point2i textureSize, emna::content::Texture const*)> mockdraw;
				virtual void draw(emna::maths::point2f pos, emna::maths::point2f size, emna::maths::point2i texturePos, emna::maths::point2i textureSize, const emna::content::Texture& texture) 
				{
					mockdraw(pos, size, texturePos, textureSize, &texture);
				}
				
				MockAction(flush);
			};
		}
	}
}

#endif // EMNA_MOCKS_MOCKGRAPHICSDRIVER_H_INCLUDED
