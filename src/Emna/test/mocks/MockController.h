#ifndef EMNA_MOCKS_MOCKCONTROLLER_HPP_INCLUDED
#define EMNA_MOCKS_MOCKCONTROLLER_HPP_INCLUDED

#include "../../Controller.h"
#include "../Mock.h"

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			template<unsigned> class MockController : public emna::Controller
			{
			public:
				MockController()
				: mockupdate(new emna::test::Mock<void (std::chrono::microseconds)>()) {
		
				}
				
				MockController(emna::test::Mock<void (std::chrono::microseconds)>* mockupdate)
				: mockupdate(mockupdate, [](emna::test::Mock<void (std::chrono::microseconds)>*) { }) {
		
				}
		
				std::shared_ptr<emna::test::Mock<void (std::chrono::microseconds)>> mockupdate;
				virtual void update (std::chrono::microseconds dt) {
					(*mockupdate)(dt);
				}
			};
		}
	}
}
#endif//EMNA_MOCKS_MOCKCONTROLLER_HPP_INCLUDED
