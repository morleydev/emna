#ifndef EMNA_TEST_MOCKS_CONTENT_MOCKTEXTURE_H_INCLUDED
#define EMNA_TEST_MOCKS_CONTENT_MOCKTEXTURE_H_INCLUDED

#include "../../../content/Texture.h"

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			namespace content
			{
				class MockTexture : public emna::content::Texture
				{
				public:
					MockTexture(std::size_t w, std::size_t h)
						: emna::content::Texture()
					{
						setWidth(w);
						setHeight(h);
					}
				};
			}
		}
	}
}

#endif//EMNA_TEST_MOCKS_CONTENT_MOCKTEXTURE_H_INCLUDED
