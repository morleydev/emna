#ifndef EMNA_MOCKS_MOCKNETWORKDRIVER_HPP_INCLUDED
#define EMNA_MOCKS_MOCKNETWORKDRIVER_HPP_INCLUDED

#include "../../NetworkDriver.h"
#include "../Mock.h"

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			class MockNetworkDriver : public NetworkDriver
			{
			public:
				virtual ~MockNetworkDriver() { }
				
				MockFunction(std::unique_ptr<network::Server>, createServer);
				MockFunction(std::unique_ptr<network::Client>, createClient);
			};
		}
	}
}

#endif//EMNA_MOCKS_MOCKNETWORKDRIVER_HPP_INCLUDED
