#ifndef EMNA_MOCKS_MOCKINPUTDRIVER_HPP_INCLUDED
#define EMNA_MOCKS_MOCKINPUTDRIVER_HPP_INCLUDED

#include "../../InputDriver.h"
#include "../Mock.h"

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			class MockInputDriver : public InputDriver
			{
			public:
				virtual ~MockInputDriver() { }
				
				MockAction(poll);
	
				MockAction(onKey, std::function<void (input::Key, input::ButtonState)>);
				MockAction(onMouseMove, std::function<void (maths::point2u)>);
				MockAction(onMouseClick, std::function<void (input::MouseButton, maths::point2u, input::ButtonState)>);
				MockAction(onClose, std::function<void (void)>);
			};
		}
	}
}

#endif//EMNA_MOCKS_MOCKINPUTDRIVER_HPP_INCLUDED
