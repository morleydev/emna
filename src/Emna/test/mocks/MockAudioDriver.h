#ifndef EMNA_MOCKS_MOCKAUDIODRIVER_H_INCLUDED
#define EMNA_MOCKS_MOCKAUDIODRIVER_H_INCLUDED

#include "../../AudioDriver.h"
#include "../Mock.h"

namespace emna
{
	namespace test
	{
		namespace mocks
		{
			class MockAudioDriver : public AudioDriver
			{
			public:
				virtual ~MockAudioDriver() { }
				
				MockAction(setListenerPosition, maths::point3f);
				MockFunctionConst(maths::point3f, getListenerPosition);
	
				MockAction(setGlobalVolume, float);
				MockFunctionConst(float, getGlobalVolume);
				
				test::Mock<void (content::SoundEffect*, maths::point3f)> mockplay2;
				virtual void play(content::SoundEffect& music, maths::point3f position) 
				{
					mockplay2(&music, position);
				}
				
				test::Mock<void (content::MusicStream*)> mockplay1;
				virtual void play(content::MusicStream& music)
				{
					mockplay1(&music);
				}
	
				MockAction(update, std::chrono::microseconds);
			};
		}
	}
}

#endif//EMNA_MOCKS_MOCKAUDIODRIVER_H_INCLUDED
