#ifndef EMNA_TEST_MOCK_HPP_INCLUDED
#define EMNA_TEST_MOCK_HPP_INCLUDED

#include <cstdint>
#include <vector>
#include <tuple>
#include <type_traits>
#include <utility>
#include <functional>
#include <stdexcept>
#include <sstream>

namespace emna
{
	namespace test
	{
		namespace detail
		{
			template <typename T, typename U> auto _areComparable(T, U) -> decltype(std::declval<T>() == std::declval<U>());
			template <typename T, typename U> std::false_type _areComparable(...);

			template <typename T, typename U, typename = decltype(_areComparable<T,U>(std::declval<T>(), std::declval<U>()))> 
			struct AreComparable
			{
				enum { value = true };
			};
			template <typename T, typename U> struct AreComparable<T,U, std::false_type>
			{
				enum { value = false };
			};

			template<typename A, typename B, bool isTrivialComparison = AreComparable<A,B>::value> struct CompareArguments 
			{
				inline bool operator()(const A& a, const B& b) const { return a == b; }
			};
			template<typename A, typename B> struct CompareArguments<A,B,false>
			{
				inline bool operator()(const A& a, const B& b) const { return b(a); }
			};
			
			template<unsigned I, typename...ARGS> 
			struct _CountMatchesInTuple
			{
				template<typename ... BRGS> std::size_t operator()(const std::tuple<ARGS...>& a, const std::tuple<BRGS...>& b) const
				{
					CompareArguments<decltype(std::get<I>(a)), decltype(std::get<I>(b))> comparer;
					return (comparer(std::get<I>(a), std::get<I>(b)) ? 1 : 0) 
					       + _CountMatchesInTuple<I-1, ARGS...>()(a,b);
				}
			};
			
			template<typename...ARGS> 
			struct _CountMatchesInTuple<0, ARGS...>
			{
				template<typename ... BRGS> std::size_t operator()(const std::tuple<ARGS...>& a, const std::tuple<BRGS...>& b) const
				{
					CompareArguments<decltype(std::get<0>(a)), decltype(std::get<0>(b))> comparer;
					return (comparer(std::get<0>(a), std::get<0>(b)) ? 1 : 0);
				}
			};
			
			template<unsigned I, typename...ARGS> 
			struct CountMatchesInTuple
			{
				template<typename ... BRGS> std::size_t operator()(const std::tuple<ARGS...>& a, const std::tuple<BRGS...>& b) const
				{
					return _CountMatchesInTuple<I-1, ARGS...>()(a, b);
				}
			};
			
			template<typename...ARGS> 
			struct CountMatchesInTuple<0, ARGS...>
			{
				template<typename ... BRGS> std::size_t operator()(const std::tuple<ARGS...>& a, const std::tuple<BRGS...>& b) const
				{
					return 0;
				}
			};


		    template <typename T, typename U> auto _isStreamable(T&, U&) -> decltype(*std::declval<T*>() << *std::declval<U*>());
		    template <typename T, typename U> std::false_type _isStreamable(...);

		    template <typename T, typename U, typename = decltype(_isStreamable<T,U>(*std::declval<T*>(), *std::declval<U*>()))>
		    struct IsStreamable
		    {
			    enum { value = true };
		    };
		    template <typename T, typename U> struct IsStreamable<T,U, std::false_type>
		    {
			    enum { value = false };
		    };

		    template<typename A, typename B, bool isStreamable = IsStreamable<A,B>::value> struct StreamArgument
			{
				inline void operator()(A& a, const B& b) const { a << b; }
			};
		    template<typename A, typename B> struct StreamArgument<A,B,false>
		    {
			    inline void operator()(A& a, const B& b) const { a << typeid(b).name(); }
		    };

		    inline std::string GetStringDescriptionOfArguments()
		    {
			    return "";
		    }

			template<typename T, typename... ARGS> std::string GetStringDescriptionOfArguments(const T& first, const ARGS&... args)
			{
				std::stringstream stream;
				StreamArgument<std::stringstream, T>()(stream , first);
				stream << ", " << GetStringDescriptionOfArguments(args...);
				return stream.str();
			}

		    template<typename T> std::string GetStringDescriptionOfArguments(const T& first)
		    {
			    std::stringstream stream;
			    StreamArgument<std::stringstream, T>()(stream , first);
			    return stream.str();
		    }
		}
		
		template<typename> class Mock;
		
		struct VerifyFailException : public std::logic_error
		{
			VerifyFailException()
					: std::logic_error("Verification failed")
			{
			}

			VerifyFailException(std::size_t times, std::string desc)
					: std::logic_error("Expected [" + desc + "] " + std::to_string(times) + " times")
			{
			}

			VerifyFailException(std::string desc)
					: std::logic_error("Expected [" + desc + "]")
			{
			}
		};
		
		template<typename ... ARGS> class Mock<void (ARGS...)>
		{
		private:
			std::vector<std::tuple<ARGS...>> m_invocations;
			std::function<void (ARGS...)> m_callback;
			
			template<typename ... EXPECTED> static bool compareTuples(const std::tuple<ARGS...> arguments, 
																	  const std::tuple<EXPECTED...>& expected)
			{
				return detail::CountMatchesInTuple<sizeof...(ARGS), ARGS...>()(arguments, expected) == sizeof...(ARGS);
			}
			
			template<typename ... EXPECTED> static std::size_t countMatches(const std::vector<std::tuple<ARGS...>>& invocations, 
																			const std::tuple<EXPECTED...>& expected)
			{
				static_assert(sizeof...(ARGS) == sizeof...(EXPECTED), "Verified argument count must match mocks argument count");
				
				std::size_t actualCount = 0;
				for(const auto& tuple : invocations)
					if (compareTuples(tuple, expected))
						++actualCount;
				return actualCount;
			}
			
		public:
			Mock()
            : m_invocations(),
              m_callback()
			{
			}
			
			template<typename C> inline void setCallback(C c)
			{
				m_callback = std::function<void (ARGS...)>(c);
			}
			
			void operator()(ARGS ... args) 
			{
				m_invocations.emplace_back(std::forward<ARGS>(args)...);
				
				if (m_callback) 
					m_callback(args...);
			}
		
			template<typename... TARGS> void verify(TARGS&& ... args) const
			{
				const auto expected = std::make_tuple(std::forward<TARGS>(args)...);
				auto actualCount = countMatches(m_invocations, expected);
				if (actualCount == 0)
					throw VerifyFailException(detail::GetStringDescriptionOfArguments(args...));
			}
			
			template<typename... TARGS> void verifyTimes(std::size_t count, TARGS&& ... args) const
			{
				const auto expected = std::make_tuple(std::forward<TARGS>(args)...);
				auto actualCount = countMatches(m_invocations, expected);
				if ( actualCount != count )
					throw VerifyFailException(count, detail::GetStringDescriptionOfArguments(args...));
			}
		};
		
		template<typename R, typename ... ARGS> class Mock<R (ARGS...)>
		{
		private:
			Mock<void (ARGS...)> m_innerMock;
			std::function<R (ARGS...)> m_returnValue;
			
		public:
			Mock() 
			: m_innerMock(), 
			  m_returnValue([](ARGS...) { return R(); }) 
			{
			}

			void setReturn(R value)
			{
				m_returnValue = ([value](ARGS...) { return value; });
			}

			template<typename C> void setReturn(C callback)
			{
				m_returnValue = std::move(std::function<R (ARGS...)>(callback));
			}
			
			template<typename C> void setCallback(C callback)
			{
				m_innerMock.template setCallback<C>(callback);
			}
			
			R operator()(ARGS ... args) 
			{
				m_innerMock(args...);
				return m_returnValue(args...);
			}
		
			template<typename... TARGS> void verify(TARGS&& ... args) const 
			{ 
				m_innerMock.verify(std::forward<TARGS>(args)...); 
			}
			
			template<typename... TARGS> void verifyTimes(std::size_t count, TARGS&& ... args) const 
			{ 
				m_innerMock.verifyTimes(count, std::forward<TARGS>(args)...);
			}
		};
	}
}

#include "MockMacros.h"

#endif // EMNA_TEST_MOCK_HPP_INCLUDED
