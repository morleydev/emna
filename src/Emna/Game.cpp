#include "Game.h"
#include "drivers/SystemFactory.hpp"
#include "util/log.h"

#ifdef EMSCRIPTEN
#include <emscripten/emscripten.h>
#endif //EMSCRIPTEN

#include <chrono>
#include <iostream>

emna::Game::Game(std::uint_least16_t viewX, std::uint_least16_t viewY)
		: m_isAlive(true),
		  m_systemDevices(drivers::SystemFactory::create(maths::point2i(viewX, viewY))),
		  graphicsDriver(*(m_systemDevices.graphicsDevice)),
		  inputDriver(*(m_systemDevices.inputDevice)),
		  audioDriver(*(m_systemDevices.audioDevice)),
		  networkDriver(*(m_systemDevices.networkDevice)),
		  content(*(m_systemDevices.contentFactory)),
		  controllers(),
          views(),
		  entities()
{
}

emna::Game::Game(std::uint_least16_t viewX, 
                 std::uint_least16_t viewY, 
                 std::function<emna::drivers::System (maths::point2i)> systemFactory)
		: m_isAlive(true),
		  m_systemDevices(systemFactory(maths::point2i(viewX, viewY))),
		  graphicsDriver(*(m_systemDevices.graphicsDevice)),
		  inputDriver(*(m_systemDevices.inputDevice)),
		  audioDriver(*(m_systemDevices.audioDevice)),
		  networkDriver(*(m_systemDevices.networkDevice)),
		  content(*(m_systemDevices.contentFactory)),
		  controllers(),
          views(),
		  entities()
{
}

emna::Game::~Game()
{
}

void emna::Game::setFrameRateLocked(bool isFrameRateLocked) 
{ 
	m_systemDevices.isFrameRateLocked = isFrameRateLocked; 
}
	
bool emna::Game::isFrameRateLocked() const 
{
	return m_systemDevices.isFrameRateLocked; 
}

void emna::Game::run()
{
	try
	{
#ifdef EMSCRIPTEN
		static Game *s_game = this;

		emscripten_set_main_loop([]()
		{
			const std::chrono::microseconds frameLength(16667);

			if (!s_game->isAlive())
			{
				emscripten_cancel_main_loop();
				return;
			}

			s_game->beginUpdate(frameLength);
			s_game->update(frameLength);
			s_game->endUpdate(frameLength);
			
			s_game->beginDraw();
			s_game->draw();
			s_game->endDraw();
		}, 60, 1);
		
#else // !EMSCRIPTEN

		const std::chrono::microseconds frameLength(16667);
		typedef std::chrono::system_clock clock;
		auto timeOfNextUpdate = clock::now() + frameLength;

		while (isAlive())
		{
			beginDraw();
			draw();
			endDraw();

			for (auto i = 0u; i < 10 && clock::now() >= timeOfNextUpdate; ++i, timeOfNextUpdate += frameLength)
			{
				beginUpdate(frameLength);
				update(frameLength);
				endUpdate(frameLength);
			}

			if (isFrameRateLocked())
			{
				auto st = timeOfNextUpdate - clock::now();
				if (st.count() > 0)
					m_systemDevices.sleepFor(std::chrono::duration_cast<std::chrono::microseconds>(st));
			}
			else
				m_systemDevices.sleepFor(std::chrono::microseconds(0));
		}
#endif //EMSCRIPTEN
	}
	catch (const std::exception &e)
	{
		kill();
		emna::util::log(emna::util::log_level::error, "Uncaught exception: ", e.what());
	}
	catch (...)
	{
		kill();
		emna::util::log(emna::util::log_level::error, "Uncaught unknown exception");
	}
}

void emna::Game::beginDraw()
{
	m_systemDevices.graphicsDevice->clear();
}

void emna::Game::draw()
{
	views.draw(*m_systemDevices.graphicsDevice);
	states.draw(*m_systemDevices.graphicsDevice);
}

void emna::Game::endDraw()
{
	m_systemDevices.graphicsDevice->flush();
}

void emna::Game::beginUpdate(std::chrono::microseconds deltaTime)
{
	m_systemDevices.inputDevice->poll();
	m_systemDevices.audioDevice->update(deltaTime);
}

void emna::Game::update(std::chrono::microseconds deltaTime)
{
	controllers.tick(deltaTime);
	states.update(deltaTime);
}

void emna::Game::endUpdate(std::chrono::microseconds deltaTime)
{
	std::pair<const std::type_info*, std::shared_ptr<void>> out;
	while(eventQueue.pop(out))
		eventDispatcher.trigger(*out.first, out.second);
}
