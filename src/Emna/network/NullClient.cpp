#include "NullClient.hpp"
#include <iostream>

bool emna::network::NullClient::connect(std::string ip, std::uint16_t port)
{
	std::cout << "Client| Connection attempt to " << ip << " on " << port << std::endl;
	return true;
}

void emna::network::NullClient::send(Message message, PacketType packetType)
{
	std::cout << "Client| Message sent: [" << message.type << "] with data " << message.data << std::endl;
}

bool emna::network::NullClient::tryReceive(Message &message)
{
	std::cout << "Client| Message receive attempted" << std::endl;
	return false;
}
