#include "SfmlServer.hpp"
#include "SfmlPacketMessageSerialisation.hpp"
#include <cpplinq.hpp>
#include <stdexcept>

emna::network::SfmlServer::SfmlServer()
: m_prevClientId(0),
  m_listener(),
  m_clientSockets(),
  m_clientPackets()
{
}

void emna::network::SfmlServer::listen(std::uint16_t port)
{
	if ( m_listener.listen(port) != sf::Socket::Status::Done )
		throw std::runtime_error("Could not listen on port " + std::to_string(port));

	m_listener.setBlocking(false);
	setIsListening(true);
}

std::int16_t emna::network::SfmlServer::tryAccept()
{
	auto socketPtr = std::make_shared<sf::TcpSocket>();
	if ( m_listener.accept(*socketPtr) != sf::Socket::Status::Done )
		return -1;

	socketPtr->setBlocking(false);

	const auto clientId = ++m_prevClientId;
	m_clientSockets[clientId] = std::move(socketPtr);
	m_clientPackets[clientId] = sf::Packet();
	return m_prevClientId;
}

std::vector<std::int16_t> emna::network::SfmlServer::getConnections() const
{
	return cpplinq::from(m_clientSockets)
			>> cpplinq::select([](const std::pair<std::int16_t, std::shared_ptr<sf::TcpSocket>>& c) { return c.first; })
	        >> cpplinq::to_vector();
}

void emna::network::SfmlServer::broadcast(Message message, PacketType packetType)
{
	sf::Packet packet;
	packet << message;

	for(auto clientSocket : m_clientSockets)
		clientSocket.second->send(packet);
}

bool emna::network::SfmlServer::tryReceive(std::int16_t clientId, Message &out)
{
	auto socket = m_clientSockets.find(clientId);
	if ( socket == m_clientSockets.end() )
		return false;

	sf::Packet& packet = m_clientPackets[clientId];
	const auto status = socket->second->receive(packet);

	switch(status)
	{
	case sf::Socket::Status::Done:
		packet >> out;
		packet.clear();
		return true;
	
	case sf::Socket::Status::Disconnected:
		m_clientSockets.erase(clientId);
        m_clientPackets.erase(clientId);
		return false;

	default:
		return false;
	}
}
