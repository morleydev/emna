#ifndef EMNA_NETWORK_SERVER_H_INCLUDED
#define EMNA_NETWORK_SERVER_H_INCLUDED

#include "../configure.h"
#include "Message.h"
#include "PacketType.h"
#include <vector>
#include <cstdint>

namespace emna
{
	namespace network
	{
		class Server
		{
		private:
			bool m_isListening;

		protected:
			void setIsListening(bool m_isListening)
			{
				Server::m_isListening = m_isListening;
			}

		public:
			EMNA_EXTERN Server();
			EMNA_EXTERN virtual ~Server();

			bool isListening() const
			{
				return m_isListening;
			}

			virtual void listen(std::uint16_t port) = 0;
			virtual std::int16_t tryAccept() = 0;
			virtual std::vector<std::int16_t> getConnections() const = 0;

			virtual void broadcast(Message message, PacketType packetType) = 0;
			virtual bool tryReceive(std::int16_t clientId, Message& out) = 0;
		};
	}
}


#endif // EMNA_NETWORK_SERVER_H_INCLUDED
