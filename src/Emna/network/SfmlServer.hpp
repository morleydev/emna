#ifndef EMNA_NETWORK_SFMLSERVER_H_INCLUDED
#define EMNA_NETWORK_SFMLSERVER_H_INCLUDED

#include "../configure.h"
#include "Message.h"
#include "Server.h"

#include <SFML/Network.hpp>

#include <cstdint>
#include <vector>
#include <map>
#include <memory>

namespace emna
{
	namespace network
	{
		class SfmlServer : public Server
		{
		private:
			std::int16_t m_prevClientId;
			sf::TcpListener m_listener;
			std::map<std::int16_t, std::shared_ptr<sf::TcpSocket>> m_clientSockets;
			std::map<std::int16_t, sf::Packet> m_clientPackets;

		public:
			EMNA_EXTERN SfmlServer();

			EMNA_EXTERN virtual void listen(std::uint16_t port);
			EMNA_EXTERN virtual std::int16_t tryAccept();
			EMNA_EXTERN virtual std::vector<std::int16_t> getConnections() const;
			EMNA_EXTERN virtual void broadcast(Message message, PacketType packetType);
			EMNA_EXTERN virtual bool tryReceive(std::int16_t clientId, Message &out);
		};
	}
}

#endif // EMNA_NETWORK_SFMLSERVER_H_INCLUDED
