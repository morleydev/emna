#ifndef EMNA_NETWORK_SFMLPACKETMESSAGESERIALISATION_H_INCLUDED
#define EMNA_NETWORK_SFMLPACKETMESSAGESERIALISATION_H_INCLUDED

#include "Message.h"
#include <SFML/Network.hpp>

namespace emna
{
	namespace network
	{
	    inline sf::Packet& operator <<(sf::Packet& packet, const Message& message)
	    {
		    return packet << sf::String(message.type) << sf::String(message.data);
	    }

	    inline sf::Packet& operator >>(sf::Packet& packet, Message& out)
	    {
		    sf::String tmp;
		    packet >> tmp; out.type = tmp.toAnsiString();
		    packet >> tmp; out.data = tmp.toAnsiString();
		    return packet;
	    }
	}
}

#endif//EMNA_NETWORK_SFMLPACKETMESSAGESERIALISATION_H_INCLUDED
