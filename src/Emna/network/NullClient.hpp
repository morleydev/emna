#ifndef EMNA_NETWORK_NULLCLIENT_H_INCLUDED
#define EMNA_NETWORK_NULLCLIENT_H_INCLUDED

#include "Client.h"

namespace emna
{
	namespace network
	{
		class NullClient : public Client
		{
		public:
			virtual bool connect(std::string ip, std::uint16_t port);
			virtual void send(Message message, PacketType packetType);
			virtual bool tryReceive(Message &out);
		};
	}
}

#endif // EMNA_NETWORK_NULLCLIENT_H_INCLUDED
