#include "NullServer.hpp"
#include <iostream>

void emna::network::NullServer::listen(std::uint16_t port)
{
	std::cout << "Server| listening on port " << port << std::endl;
}

std::int16_t emna::network::NullServer::tryAccept()
{
	std::cout << "Server| acceptance attempted " << std::endl;
	return -1;
}

std::vector<std::int16_t> emna::network::NullServer::getConnections() const
{
	return std::vector<std::int16_t>();
}

void emna::network::NullServer::broadcast(Message message, PacketType packetType)
{
	std::cout << "Server| message dispatched [" << message.type << "] with data " << message.data << std::endl;
}

bool emna::network::NullServer::tryReceive(int16_t clientId, Message &out)
{
	std::cout << "Server| message receive attempted for client " << clientId << std::endl;
	return false;
}