#ifndef EMNA_NETWORK_SFMLCLIENT_H_INCLUDED
#define EMNA_NETWORK_SFMLCLIENT_H_INCLUDED

#include "../configure.h"
#include "Client.h"
#include "Message.h"

#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <cstdint>

namespace emna
{
	namespace network
	{
		class SfmlClient : public Client
		{
		private:
			sf::TcpSocket m_socket;
			sf::Packet m_receivePacket;

		public:
			EMNA_EXTERN SfmlClient();
			EMNA_EXTERN ~SfmlClient();

			EMNA_EXTERN virtual bool connect(std::string ip, std::uint16_t port);
			EMNA_EXTERN virtual void send(Message message, PacketType packetType);
			EMNA_EXTERN virtual bool tryReceive(Message &message);
		};
	}
}

#endif // EMNA_NETWORK_SFMLCLIENT_H_INCLUDED
