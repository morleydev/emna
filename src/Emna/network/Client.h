#ifndef EMNA_NETWORK_CLIENT_H_INCLUDED
#define EMNA_NETWORK_CLIENT_H_INCLUDED

#include "../configure.h"
#include "Message.h"
#include "PacketType.h"
#include <cstdint>
#include <string>

namespace emna
{
	namespace network
	{
		class Client
		{
		private:
			bool m_isConnected;

		protected:
			void setIsConnected(bool status) { m_isConnected = status; }

		public:
			Client() : m_isConnected(false) { }

			EMNA_EXTERN virtual ~Client();

			bool isConnected() { return m_isConnected; }

			virtual bool connect(std::string ip, std::uint16_t port) = 0;
			virtual void send(Message message, PacketType packetType) = 0;
			virtual bool tryReceive(Message &message) = 0;
		};
	}

}

#endif // EMNA_NETWORK_CLIENT_H_INCLUDED
