#include "SfmlClient.hpp"
#include "SfmlPacketMessageSerialisation.hpp"
#include <cereal/archives/json.hpp>

emna::network::SfmlClient::SfmlClient()
: m_socket(),
  m_receivePacket()
{
}

emna::network::SfmlClient::~SfmlClient()
{
	if (isConnected())
		m_socket.disconnect();
}

bool emna::network::SfmlClient::connect(std::string ip, std::uint16_t port)
{
	if ( m_socket.connect(sf::IpAddress(ip), port) != sf::Socket::Status::Done )
		return false;

	m_socket.setBlocking(false);
	setIsConnected(true);
	return true;
}

void emna::network::SfmlClient::send(Message message, PacketType packetType)
{
	sf::Packet packet;
	packet << message;
	m_socket.send(packet);
}

bool emna::network::SfmlClient::tryReceive(Message &out)
{
	if (!isConnected())
		return false;

	const auto status = m_socket.receive(m_receivePacket);
	switch(status)
	{
	case sf::Socket::Status::Done:
	{
		m_receivePacket >> out;
		return true;
	}

	case sf::Socket::Status::Disconnected:
		setIsConnected(false);
		return false;

	case sf::Socket::Status::Error:
		throw std::runtime_error("Client socket error occured");

	default:
		return false;
	}
}
