#ifndef EMNA_NETWORK_NULLSERVER_H_INCLUDED
#define EMNA_NETWORK_NULLSERVER_H_INCLUDED

#include "Server.h"

#include <cstdint>
#include <vector>

namespace emna
{
	namespace network
	{
		class NullServer : public Server
		{

		public:
			virtual void listen(std::uint16_t port);
			virtual std::int16_t tryAccept();
			virtual std::vector<std::int16_t> getConnections() const;
			virtual void broadcast(Message message, PacketType packetType);
			virtual bool tryReceive(std::int16_t clientId, Message &out);
		};
	}
}

#endif // EMNA_NETWORK_NULLSERVER_H_INCLUDED
