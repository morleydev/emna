#ifndef EMNA_NETWORK_MESSAGE_H_INCLUDED
#define EMNA_NETWORK_MESSAGE_H_INCLUDED

#include <string>

namespace emna
{
	namespace network
	{
		typedef std::string MessageType;
		typedef std::string MessageData;

		struct Message
		{
			MessageType type;
			MessageData data;
		};
	}
}

#endif // EMNA_NETWORK_MESSAGE_H_INCLUDED
