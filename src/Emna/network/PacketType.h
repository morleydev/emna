#ifndef EMNA_NETWORK_PACKETTYPE_H_INCLUDED
#define EMNA_NETWORK_PACKETTYPE_H_INCLUDED

namespace emna
{
	namespace network
	{
		enum class PacketType
		{
			/*!
			 Failable messages are not guaranteed to arrive at their destination. This means the packets
			 may be sent via a protocol such as UDP if available.
			*/
			Failable,

			/*!
			 Infailable messages are guaranteed to either arrive at their destination or fail.
			 This means the packets will most likely be sent via a protocol such as TCP.
			*/
			Infailable
		};
	}
}


#endif // EMNA_NETWORK_PACKETTYPE_H_INCLUDED
