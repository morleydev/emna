#ifndef EMNA_CONFIGURE_H_INCLUDED
#define EMNA_CONFIGURE_H_INCLUDED

#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
	#ifdef EMNA_STATIC
		#define EMNA_EXTERN
	#elif defined(EMNA_COMPILE_DYNAMIC) // && !EMNA_STATIC
		#define EMNA_EXTERN __declspec(dllexport)
	#else // !EMNA_STATIC && !EMNA_COMPILE_DYNAMIC
		#define EMNA_EXTERN __declspec(dllimport)
	#endif // EMNA_STATIC
#else // !_MSC_VER && !__INTEL_COMPILER
	#define EMNA_EXTERN
#endif // MSVC

#endif // EMNA_CONFIGURE_H_INCLUDED
