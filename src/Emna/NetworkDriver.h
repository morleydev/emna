#ifndef EMNA_NETWORKDRIVER_H_INCLUDED
#define EMNA_NETWORKDRIVER_H_INCLUDED

#include "configure.h"
#include "network/Server.h"
#include "network/Client.h"
#include <memory>

namespace emna
{
	class NetworkDriver
	{
	public:
		EMNA_EXTERN virtual ~NetworkDriver();

		virtual std::unique_ptr<network::Server> createServer() = 0;
		virtual std::unique_ptr<network::Client> createClient() = 0;
	};
}

#endif // EMNA_NETWORKDRIVER_H_INCLUDED
