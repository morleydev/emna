#ifndef EMNA_VIEWSTORE_H_INCLUDED
#define EMNA_VIEWSTORE_H_INCLUDED

#include "configure.h"
#include "View.h"
#include "util/make_unique.h"

#include <map>
#include <memory>
#include <list>
#include <typeindex>

namespace emna
{
	class ViewStore
	{
	private:
		std::map<std::type_index, std::list<std::unique_ptr<View>>::iterator> m_viewSet;
		std::list<std::unique_ptr<View>> m_views;

	public:
		template<typename T, typename... Args> void add(Args... args)
		{
			add( typeid(T), std::unique_ptr<View>(util::make_unique<T>(args...)) );
		}

		template<typename T> T& get() const { return static_cast<T&>( get(typeid(T)) ); }
		template<typename T> void remove() { return remove(typeid(T)); }

		EMNA_EXTERN void draw(GraphicsDriver& graphics);

	private:
		EMNA_EXTERN void add(const std::type_info &type, std::unique_ptr<View> view);
		EMNA_EXTERN void remove(const std::type_info &type);
		EMNA_EXTERN View& get(const std::type_info &type) const;
	};
}

#endif // EMNA_VIEWSTORE_H_INCLUDED
