#include "SfmlInputDevice.hpp"
#include "../util/to_global_string.h"

emna::drivers::SfmlInputDevice::SfmlInputDevice(std::shared_ptr<sf::Window> window)
		: m_window(window),
		  m_keyHandler([](input::Key, input::ButtonState) { }),
		  m_mouseMoveHandler([](maths::point2u) { }),
		  m_mouseClickHandler([](input::MouseButton, maths::point2u, input::ButtonState) { }),
		  m_closeHandler([]() { })
{
	setHasKeyboardAccess(true);
	setHasMouseAccess(true);
}

emna::drivers::SfmlInputDevice::~SfmlInputDevice()
{
}

void emna::drivers::SfmlInputDevice::onKey(std::function<void(input::Key, input::ButtonState)> callback)
{
	m_keyHandler = callback;
}

void emna::drivers::SfmlInputDevice::onMouseMove(std::function<void(maths::point2u)> callback)
{
	m_mouseMoveHandler = callback;
}

void emna::drivers::SfmlInputDevice::onMouseClick(std::function<void(input::MouseButton, maths::point2u, input::ButtonState)> callback)
{
	m_mouseClickHandler = callback;
}

void emna::drivers::SfmlInputDevice::onClose(std::function<void(void)> callback)
{
	m_closeHandler = callback;
}

void emna::drivers::SfmlInputDevice::poll()
{
	sf::Event event;
	while (m_window->pollEvent(event))
	{
		switch (event.type)
		{
			case sf::Event::Closed:
				m_closeHandler();
		        break;

			case sf::Event::KeyPressed:
				m_keyHandler(getVirtualKey(event.key.code), input::ButtonState::Down);
		        break;

			case sf::Event::KeyReleased:
				m_keyHandler(getVirtualKey(event.key.code), input::ButtonState::Up);
		        break;

			case sf::Event::MouseMoved:
				if (!(event.mouseMove.x < 0 || event.mouseMove.y < 0))
					m_mouseMoveHandler(maths::point2u(event.mouseMove.x, event.mouseMove.y));
		        break;

			case sf::Event::MouseButtonPressed:
				if (!(event.mouseButton.x < 0 || event.mouseButton.y < 0))
					m_mouseClickHandler(getVirtualMouseButton(event.mouseButton.button),
					                    maths::point2u(event.mouseButton.x, event.mouseButton.y),
					                    input::ButtonState::Down);
		        break;

			case sf::Event::MouseButtonReleased:
				if (!(event.mouseButton.x < 0 || event.mouseButton.y < 0))
					m_mouseClickHandler(getVirtualMouseButton(event.mouseButton.button),
					                    maths::point2u(event.mouseButton.x, event.mouseButton.y),
					                    input::ButtonState::Up);
		        break;

			default:
				break;
		}
	}
}


emna::input::Key emna::drivers::SfmlInputDevice::getVirtualKey(sf::Keyboard::Key k)
{
#define SFML_TO_EMNA_MAP(s, e) case sf::Keyboard::s : return input::KeyCode::e

	switch (k)
	{
		SFML_TO_EMNA_MAP(A, A);
		SFML_TO_EMNA_MAP(B, B);
		SFML_TO_EMNA_MAP(C, C);
		SFML_TO_EMNA_MAP(D, D);
		SFML_TO_EMNA_MAP(E, E);
		SFML_TO_EMNA_MAP(F, F);
		SFML_TO_EMNA_MAP(G, G);
		SFML_TO_EMNA_MAP(H, H);
		SFML_TO_EMNA_MAP(I, I);
		SFML_TO_EMNA_MAP(J, J);
		SFML_TO_EMNA_MAP(K, K);
		SFML_TO_EMNA_MAP(L, L);
		SFML_TO_EMNA_MAP(M, M);
		SFML_TO_EMNA_MAP(N, N);
		SFML_TO_EMNA_MAP(O, O);
		SFML_TO_EMNA_MAP(P, P);
		SFML_TO_EMNA_MAP(Q, Q);
		SFML_TO_EMNA_MAP(R, R);
		SFML_TO_EMNA_MAP(S, S);
		SFML_TO_EMNA_MAP(T, T);
		SFML_TO_EMNA_MAP(U, U);
		SFML_TO_EMNA_MAP(V, V);
		SFML_TO_EMNA_MAP(W, W);
		SFML_TO_EMNA_MAP(X, X);
		SFML_TO_EMNA_MAP(Y, Y);
		SFML_TO_EMNA_MAP(Z, Z);

		SFML_TO_EMNA_MAP(F1, F1);
		SFML_TO_EMNA_MAP(F2, F2);
		SFML_TO_EMNA_MAP(F3, F3);
		SFML_TO_EMNA_MAP(F4, F4);
		SFML_TO_EMNA_MAP(F5, F5);
		SFML_TO_EMNA_MAP(F6, F6);
		SFML_TO_EMNA_MAP(F7, F7);
		SFML_TO_EMNA_MAP(F8, F8);
		SFML_TO_EMNA_MAP(F9, F9);
		SFML_TO_EMNA_MAP(F10, F10);
		SFML_TO_EMNA_MAP(F11, F11);
		SFML_TO_EMNA_MAP(F12, F12);

		SFML_TO_EMNA_MAP(Num0, Num0);
		SFML_TO_EMNA_MAP(Num1, Num1);
		SFML_TO_EMNA_MAP(Num2, Num2);
		SFML_TO_EMNA_MAP(Num3, Num3);
		SFML_TO_EMNA_MAP(Num4, Num4);
		SFML_TO_EMNA_MAP(Num5, Num5);
		SFML_TO_EMNA_MAP(Num6, Num6);
		SFML_TO_EMNA_MAP(Num7, Num7);
		SFML_TO_EMNA_MAP(Num8, Num8);
		SFML_TO_EMNA_MAP(Num9, Num9);

		SFML_TO_EMNA_MAP(Escape, Escape);
		SFML_TO_EMNA_MAP(Space, Space);

		SFML_TO_EMNA_MAP(Up, Up);
		SFML_TO_EMNA_MAP(Down, Down);
		SFML_TO_EMNA_MAP(Left, Left);
		SFML_TO_EMNA_MAP(Right, Right);

		default:
			return input::Key::instance(std::to_string(k), static_cast<std::uint32_t>(k));
	}

#undef SFML_TO_EMNA_MAP
}


emna::input::MouseButton emna::drivers::SfmlInputDevice::getVirtualMouseButton(sf::Mouse::Button b)
{
	switch (b)
	{
		case sf::Mouse::Left:
			return input::MouseButton::Left;

		case sf::Mouse::Right:
			return input::MouseButton::Right;

		case sf::Mouse::Middle:
		default:
			return input::MouseButton::Middle;
	}
}
