#include "SfmlAudioDevice.hpp"
#include "../content/SfmlMusicStream.hpp"
#include "../content/SfmlSoundEffect.hpp"
#include "../util/make_unique.h"
#include "../util/log.h"

emna::drivers::SfmlAudioDevice::SfmlAudioDevice()
{
}

emna::drivers::SfmlAudioDevice::~SfmlAudioDevice()
{
}

void emna::drivers::SfmlAudioDevice::play(content::MusicStream &music)
{
	sf::Music& sfMusic = static_cast<content::SfmlMusicStream&>(music).getData();
	sfMusic.setLoop(music.getLooping());
	sfMusic.play();
}

void emna::drivers::SfmlAudioDevice::play(emna::content::SoundEffect &music, emna::maths::point3<float> position)
{
	auto buffer = static_cast<content::SfmlSoundEffect&>(music).getBuffer();
	std::unique_ptr<sf::Sound> sound = util::make_unique<sf::Sound>();
	sound->setBuffer(*buffer);
	sound->setPosition(position.x, position.y, position.z);
	sound->play();

	m_activeSounds.emplace_back(buffer, std::move(sound));
}

void emna::drivers::SfmlAudioDevice::setListenerPosition(emna::maths::point3<float> position)
{
	sf::Listener::setPosition(position.x, position.y, position.z);
}

emna::maths::point3f emna::drivers::SfmlAudioDevice::getListenerPosition() const
{
	auto pos = sf::Listener::getPosition();

	return emna::maths::point3f(pos.x, pos.y, pos.z);
}

void emna::drivers::SfmlAudioDevice::setGlobalVolume(float volume)
{
	sf::Listener::setGlobalVolume(volume * 100.0f);
}

float emna::drivers::SfmlAudioDevice::getGlobalVolume() const
{
	return sf::Listener::getGlobalVolume() / 100.0f;
}

void emna::drivers::SfmlAudioDevice::update(std::chrono::microseconds deltaTime)
{
	auto pend = std::remove_if(m_activeSounds.begin(), m_activeSounds.end(), [](const BufferSoundPair& pair) {
		return pair.second->getStatus() == sf::Sound::Status::Stopped;
	});
	m_activeSounds.erase(pend, m_activeSounds.end());
}
