#ifndef EMNA_DRIVERS_SYSTEM_H_INCLUDED
#define EMNA_DRIVERS_SYSTEM_H_INCLUDED

#include "../configure.h"
#include "../AudioDriver.h"
#include "../GraphicsDriver.h"
#include "../InputDriver.h"
#include "../NetworkDriver.h"
#include "../ContentFactory.h"

#include <memory>
#include <functional>
#include <chrono>

namespace emna
{
    namespace drivers
    {
        struct System
        {
	        System()
	        {
	        }

	        System(System &&other)
			        : audioDevice(std::move(other.audioDevice)),
			          graphicsDevice(std::move(other.graphicsDevice)),
			          inputDevice(std::move(other.inputDevice)),
			          contentFactory(std::move(other.contentFactory)),
			          sleepFor(std::move(other.sleepFor)),
	                  isFrameRateLocked(other.isFrameRateLocked)
	        {
	        }

	        std::unique_ptr<AudioDriver> audioDevice;
	        std::unique_ptr<GraphicsDriver> graphicsDevice;
	        std::unique_ptr<InputDriver> inputDevice;
			std::unique_ptr<NetworkDriver> networkDevice;
	        std::unique_ptr<ContentFactory> contentFactory;

	        std::function<void(std::chrono::microseconds)> sleepFor;
	        bool isFrameRateLocked;
        };
    }
}

#endif // EMNA_DRIVERS_SYSTEM_H_INCLUDED
