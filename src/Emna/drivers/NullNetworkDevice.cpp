#include "NullNetworkDevice.hpp"
#include "../network/NullClient.hpp"
#include "../network/NullServer.hpp"
#include "../util/make_unique.h"

std::unique_ptr<emna::network::Server> emna::drivers::NullNetworkDevice::createServer()
{
	return util::make_unique<network::NullServer>();
}

std::unique_ptr<emna::network::Client> emna::drivers::NullNetworkDevice::createClient()
{
	return util::make_unique<network::NullClient>();
}
