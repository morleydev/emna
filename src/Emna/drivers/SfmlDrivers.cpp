#include "SfmlDrivers.hpp"
#include "SfmlContentFactory.hpp"
#include "SfmlGraphicsDevice.hpp"
#include "SfmlInputDevice.hpp"
#include "SfmlAudioDevice.hpp"
#include "SfmlNetworkDevice.hpp"

#include "../util/make_unique.h"
#include <cpplinq.hpp>

emna::drivers::System emna::drivers::createSfmlDrivers(maths::point2i view, SystemConfiguration config)
{
	auto fullscreen = config.fullscreen;
	auto windowWidth = config.windowWidth <= 0 ? (fullscreen ? sf::VideoMode::getDesktopMode().width : 1280) : config.windowWidth;
	auto windowHeight = config.windowHeight <= 0 ? (fullscreen ? sf::VideoMode::getDesktopMode().height : 720) : config.windowHeight;

	fullscreen = fullscreen && cpplinq::from(sf::VideoMode::getFullscreenModes())
	                    >> cpplinq::any([&](sf::VideoMode fs) { return fs.width == windowWidth && fs.height == windowHeight; });

	windowWidth = !fullscreen && sf::VideoMode::getDesktopMode().width >= windowWidth ? 1280 : windowWidth;
	windowHeight = !fullscreen && sf::VideoMode::getDesktopMode().height >= windowHeight ? 720 : windowWidth;

	auto window = std::make_shared<sf::RenderWindow>(sf::VideoMode(windowWidth, windowHeight),
	                                                 "Emna",
	                                                 sf::Style::Close | (fullscreen ? sf::Style::Fullscreen : sf::Style::Titlebar));
	window->setVerticalSyncEnabled(config.vsync);

	System system;
	system.contentFactory = std::move(util::make_unique<drivers::SfmlContentFactory>());
	system.graphicsDevice = std::move(util::make_unique<drivers::SfmlGraphicsDevice>(window, view));
	system.inputDevice = std::move(util::make_unique<drivers::SfmlInputDevice>(window));
	system.audioDevice = std::move(util::make_unique<drivers::SfmlAudioDevice>());
	system.networkDevice = std::move(util::make_unique<drivers::SfmlNetworkDevice>());
	system.sleepFor = [](std::chrono::microseconds time) { sf::sleep(sf::microseconds(time.count())); };
	return std::move(system);
}
