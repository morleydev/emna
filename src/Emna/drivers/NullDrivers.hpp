#ifndef EMNA_DRIVERS_NULLDRIVERS_H_INCLUDED
#define EMNA_DRIVERS_NULLDRIVERS_H_INCLUDED

#include "System.h"
#include "SystemConfiguration.hpp"

namespace emna
{
    namespace drivers
    {
        extern System createNullDrivers(maths::point2i view, SystemConfiguration config);
    }
}

#endif // EMNA_DRIVERS_NULLDRIVERS_H_INCLUDED
