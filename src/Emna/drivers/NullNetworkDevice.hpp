#ifndef EMNA_DRIVERS_NULLNETWORKDEVICE_H_INCLUDED
#define EMNA_DRIVERS_NULLNETWORKDEVICE_H_INCLUDED

#include "../NetworkDriver.h"

namespace emna
{
	namespace drivers
	{
		class NullNetworkDevice : public NetworkDriver
		{
		public:
			virtual std::unique_ptr<network::Server> createServer();
			virtual std::unique_ptr<network::Client> createClient();
		};
	}
}

#endif // EMNA_DRIVERS_NULLNETWORKDEVICE_H_INCLUDED
