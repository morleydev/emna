#ifndef EMNA_DRIVERS_NULLINPUTDEVICE_H_INCLUDED
#define EMNA_DRIVERS_NULLINPUTDEVICE_H_INCLUDED

#include "../InputDriver.h"

namespace emna
{
    namespace drivers
    {
        class NullInputDevice : public InputDriver
        {
        public:
	        NullInputDevice();

	        virtual ~NullInputDevice();

        public:
	        virtual void onKey(std::function<void(input::Key, input::ButtonState)>)
	        {
	        }

	        virtual void onMouseMove(std::function<void(maths::point2u)>)
	        {
	        }

	        virtual void onMouseClick(std::function<void(input::MouseButton, maths::point2u, input::ButtonState)>)
	        {
	        }

	        virtual void onClose(std::function<void(void)>)
	        {
	        }

	        virtual void poll()
	        {
	        }
        };
    }
}

#endif // EMNA_DRIVERS_NULLINPUTDEVICE_H_INCLUDED
