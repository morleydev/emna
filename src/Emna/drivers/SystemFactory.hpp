#ifndef EMNA_SYSTEMFACTORY
#define EMNA_SYSTEMFACTORY

#include "System.h"
#include "../maths/point2.h"
#include <memory>

namespace emna
{
    namespace drivers
    {
        class SystemFactory
        {
        public:
	        static emna::drivers::System create(maths::point2i view);
        };
    }
}

#endif // EMNA_SYSTEMFACTORY
