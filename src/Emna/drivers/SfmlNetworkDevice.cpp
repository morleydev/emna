#include "SfmlNetworkDevice.hpp"
#include "../network/SfmlClient.hpp"
#include "../network/SfmlServer.hpp"

#include "../util/make_unique.h"

std::unique_ptr<emna::network::Server> emna::drivers::SfmlNetworkDevice::createServer()
{
	return util::make_unique<network::SfmlServer>();
}

std::unique_ptr<emna::network::Client> emna::drivers::SfmlNetworkDevice::createClient()
{
	return util::make_unique<network::SfmlClient>();
}
