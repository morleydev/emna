#include "SdlContentFactory.hpp"
#include "../content/SdlTexture.hpp"
#include "../content/SdlMusicStream.hpp"
#include "../content/SdlSoundEffect.hpp"
#include "../util/make_unique.h"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#include <stdexcept>
#include <fstream>

std::unique_ptr<emna::content::SoundEffect> emna::drivers::SdlContentFactory::loadSoundEffect(std::string)
{
	return util::make_unique<content::SdlSoundEffect>();
}

std::unique_ptr<emna::content::Texture> emna::drivers::SdlContentFactory::loadTexture(std::string path)
{
	auto image = IMG_Load(path.c_str());
	if (!image)
		throw std::runtime_error("Could not find file: " + path);

	std::unique_ptr<SDL_Surface, std::function<void(SDL_Surface *)>> surface(image, [](SDL_Surface *sdlSurface) { SDL_FreeSurface(sdlSurface); });

	return util::make_unique<content::SdlTexture>(std::move(surface));
}

std::unique_ptr<emna::content::MusicStream> emna::drivers::SdlContentFactory::openMusicStream(std::string)
{
	return util::make_unique<content::SdlMusicStream>();
}

std::unique_ptr<std::istream> emna::drivers::SdlContentFactory::openFileStream(std::string path)
{
	auto ptr = util::make_unique<std::ifstream>(path);
	if (!ptr->is_open())
		throw std::runtime_error("Could not find file: " + path);

	return std::move(ptr);
}

emna::drivers::SdlContentFactory::~SdlContentFactory()
{
}
