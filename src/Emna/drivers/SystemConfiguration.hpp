#ifndef EMNA_DRIVERS_SYSTEMCONFIGURATION_H_INCLUDED
#define EMNA_DRIVERS_SYSTEMCONFIGURATION_H_INCLUDED

#include <cereal/cereal.hpp>

namespace emna
{
    namespace drivers
    {
        struct SystemConfiguration
        {
	        SystemConfiguration()
			: driver(0),
			  windowWidth(-1),
			  windowHeight(-1),
			  fullscreen(true),
	          vsync(false),
	        lockFrameRate(true)
	        {
	        }

	        std::size_t driver;
	        std::int_least16_t windowWidth;
	        std::int_least16_t windowHeight;
	        bool fullscreen;
	        bool vsync;
	        bool lockFrameRate;

	        template<typename TArchive>
	        void serialize(TArchive &archive)
	        {
		        archive(cereal::make_nvp("driver", driver),
		                cereal::make_nvp("display-width", windowWidth),
		                cereal::make_nvp("display-height", windowHeight),
		                cereal::make_nvp("display-fullscreen", fullscreen),
		                cereal::make_nvp("display-vsync", vsync),
		                cereal::make_nvp("lock-framerate", lockFrameRate));
	        }
        };
    }
}

#endif // EMNA_DRIVERS_SYSTEMCONFIGURATION_H_INCLUDED
