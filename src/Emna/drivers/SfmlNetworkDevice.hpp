#ifndef EMNA_DRIVERS_SFMLNETWORKDEVICE_H_INCLUDED
#define EMNA_DRIVERS_SFMLNETWORKDEVICE_H_INCLUDED

#include "../NetworkDriver.h"

namespace emna
{
	namespace drivers
	{
		class SfmlNetworkDevice : public NetworkDriver
		{
		public:
			EMNA_EXTERN virtual std::unique_ptr<network::Server> createServer();
			EMNA_EXTERN virtual std::unique_ptr<network::Client> createClient();
		};
	}
}

#endif// EMNA_DRIVERS_SFMLNETWORKDEVICE_H_INCLUDED
