#include "SfmlContentFactory.hpp"
#include "../content/SfmlTexture.hpp"
#include "../content/SfmlMusicStream.hpp"
#include "../content/SfmlSoundEffect.hpp"
#include "../util/make_unique.h"

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include <stdexcept>
#include <fstream>

emna::drivers::SfmlContentFactory::~SfmlContentFactory()
{
}

std::unique_ptr<emna::content::SoundEffect> emna::drivers::SfmlContentFactory::loadSoundEffect(std::string path)
{
	auto soundBuffer = std::make_shared<sf::SoundBuffer>();
	if (!soundBuffer->loadFromFile(path))
		throw std::runtime_error("Could not find sound effect file: " + path);

	return util::make_unique<content::SfmlSoundEffect>(soundBuffer);
}

std::unique_ptr<emna::content::Texture> emna::drivers::SfmlContentFactory::loadTexture(std::string path)
{
	sf::Texture texture;
	if (!texture.loadFromFile(path))
		throw std::runtime_error("Could not find texture file: " + path);

	return util::make_unique<content::SfmlTexture>(texture);
}

std::unique_ptr<emna::content::MusicStream> emna::drivers::SfmlContentFactory::openMusicStream(std::string path)
{
	auto music = util::make_unique<sf::Music>();
	if (!music->openFromFile(path))
		throw std::runtime_error("Could not find music file: " + path);

	return util::make_unique<content::SfmlMusicStream>(std::move(music));
}

std::unique_ptr<std::istream> emna::drivers::SfmlContentFactory::openFileStream(std::string path)
{
	auto ptr = util::make_unique<std::ifstream>(path);
	if (!ptr->is_open())
		throw std::runtime_error("Could not find file: " + path);

	return std::move(ptr);
}
