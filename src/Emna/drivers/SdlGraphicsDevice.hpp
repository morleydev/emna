#ifndef EMNA_DRIVERS_SDLGRAPHICSDRIVER_H_INCLUDED
#define EMNA_DRIVERS_SDLGRAPHICSDRIVER_H_INCLUDED

#include "../GraphicsDriver.h"
#include <SDL/SDL.h>
#include <memory>

namespace emna
{
    namespace drivers
    {
        class SdlGraphicsDevice : public GraphicsDriver
        {
        private:
	        std::shared_ptr<SDL_Surface> m_surface;

        public:
	        SdlGraphicsDevice(std::shared_ptr<SDL_Surface>, maths::point2i view);

	        virtual ~SdlGraphicsDevice();

	        virtual void clear();

	        virtual void draw(maths::point2f pos, maths::point2f size,
	                          maths::point2i texturePos, maths::point2i textureSize,
	                          const content::Texture &);

	        virtual void flush();
        };
    }
}

#endif // EMNA_DRIVERS_SDLGRAPHICSDRIVER_H_INCLUDED
