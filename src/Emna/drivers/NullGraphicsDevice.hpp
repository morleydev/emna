#ifndef EMNA_DRIVERS_NULLGRAPHICSDEVICE_H_INCLUDED
#define EMNA_DRIVERS_NULLGRAPHICSDEVICE_H_INCLUDED

#include "../GraphicsDriver.h"
#include <ostream>

namespace emna
{
    namespace drivers
    {
        class NullGraphicsDevice : public GraphicsDriver
        {
        private:
	        std::ostream &m_out;

        public:
	        virtual ~NullGraphicsDevice();
	        NullGraphicsDevice(maths::point2i view);
	        NullGraphicsDevice(maths::point2i view, std::ostream &out);

	        virtual void clear();
	        virtual void draw(maths::point2f pos, maths::point2f size, maths::point2i texturePos,
	                          maths::point2i textureSize,
	                          const content::Texture &);
	        virtual void flush();
        };
    }
}

#endif // EMNA_DRIVERS_NULLGRAPHICSDEVICE_H_INCLUDED
