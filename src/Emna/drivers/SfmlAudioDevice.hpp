#ifndef EMNA_DRIVERS_SFMLAUDIODEVICE_HPP_INCLUDED
#define EMNA_DRIVERS_SFMLAUDIODEVICE_HPP_INCLUDED

#include "../AudioDriver.h"
#include <SFML/Audio.hpp>
#include <vector>
#include <memory>

namespace emna
{
    namespace drivers
    {
        class SfmlAudioDevice : public AudioDriver
        {
        private:
	        typedef std::pair<std::shared_ptr<sf::SoundBuffer>, std::unique_ptr<sf::Sound>> BufferSoundPair;
	        std::vector<BufferSoundPair> m_activeSounds;

        public:
	        SfmlAudioDevice();

	        virtual ~SfmlAudioDevice();

	        virtual void play(content::MusicStream &music);
	        virtual void play(content::SoundEffect& music, maths::point3f position);

	        virtual void setListenerPosition(maths::point3f position);
	        virtual maths::point3f getListenerPosition() const;

	        virtual void setGlobalVolume(float volume);
	        virtual float getGlobalVolume() const;

	        virtual void update(std::chrono::microseconds deltaTime);
        };
    }
}

#endif // EMNA_DRIVERS_SFMLAUDIODEVICE_HPP_INCLUDED
