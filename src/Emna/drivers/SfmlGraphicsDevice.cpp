#include "SfmlGraphicsDevice.hpp"
#include "../content/SfmlTexture.hpp"

emna::drivers::SfmlGraphicsDevice::~SfmlGraphicsDevice()
{
}

emna::drivers::SfmlGraphicsDevice::SfmlGraphicsDevice(std::shared_ptr<sf::RenderWindow> renderWindow, maths::point2i virtualSize)
		: m_renderWindow(renderWindow)
{
	setWidth(static_cast<std::size_t>(m_renderWindow->getView().getSize().x));
	setHeight(static_cast<std::size_t>(m_renderWindow->getView().getSize().y));
	setView(virtualSize);
}

void emna::drivers::SfmlGraphicsDevice::clear()
{
	m_renderWindow->clear(sf::Color::Black);
}

void emna::drivers::SfmlGraphicsDevice::draw(maths::point2f pos, maths::point2f size,
                                             maths::point2i texturePos, maths::point2i textureSize,
                                             const content::Texture &texture)
{
	const auto &sfmlTexture = static_cast<const content::SfmlTexture &>(texture).getTexture();
	sf::Sprite sprite(sfmlTexture, sf::IntRect(texturePos.x, texturePos.y, textureSize.x, textureSize.y));
	sprite.setPosition(sf::Vector2f(pos.x, pos.y));
	sprite.setOrigin(0.0f, 0.0f);
	sprite.setScale(size.x / textureSize.x, size.y / textureSize.y);
	m_renderWindow->draw(sprite);
}

void emna::drivers::SfmlGraphicsDevice::flush()
{
	m_renderWindow->display();
}

void emna::drivers::SfmlGraphicsDevice::setView(maths::point2i viewSize)
{
	sf::View view(sf::FloatRect(0.0f, 0.0f, static_cast<float>(viewSize.x), static_cast<float>(viewSize.y)));
	view.setViewport(calculateViewport(viewSize));
	m_renderWindow->setView(view);

	setViewWidth(viewSize.x);
	setViewHeight(viewSize.y);
}

sf::FloatRect emna::drivers::SfmlGraphicsDevice::calculateViewport(maths::point2i viewSize)
{
	const auto viewWidth = static_cast<float>(viewSize.x);
	const auto viewHeight = static_cast<float>(viewSize.y);
	const float windowWidth = static_cast<float>(m_renderWindow->getSize().x);
	const float windowHeight = static_cast<float>(m_renderWindow->getSize().y);

	const auto targetAspectRatio = viewWidth / viewHeight;
	const auto windowAspectRatio = windowWidth / windowHeight;

	const auto scale = windowAspectRatio > targetAspectRatio
	                   ? windowHeight / viewHeight
	                   : windowWidth / viewWidth;

	const auto cropX = (windowAspectRatio > targetAspectRatio
	                    ? (m_renderWindow->getSize().x - viewSize.x * scale) / (2.0f * windowWidth)
	                    : 0.0f);
	const auto cropY = (windowAspectRatio < targetAspectRatio
	                    ? (m_renderWindow->getSize().y - viewSize.y * scale) / (2.0f * windowHeight)
	                    : 0.0f);
	const auto cropW = (viewWidth * scale) / windowWidth;
	const auto cropH = (viewHeight * scale) / windowHeight;

	return sf::FloatRect(cropX, cropY, cropW, cropH);
}
