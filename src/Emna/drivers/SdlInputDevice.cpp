#include "SdlInputDevice.hpp"
#include <SDL/SDL.h>
#include "../util/to_global_string.h"

emna::drivers::SdlInputDevice::SdlInputDevice()
		: m_keyHandler([](input::Key, input::ButtonState) { }),
		  m_mouseMoveHandler([](maths::point2u) { }),
		  m_mouseClickHandler([](input::MouseButton, maths::point2u, input::ButtonState) { }),
		  m_closeHandler([]() { })
{
	setHasKeyboardAccess(true);
	setHasMouseAccess(true);
}

emna::drivers::SdlInputDevice::~SdlInputDevice()
{
}

void emna::drivers::SdlInputDevice::onClose(std::function<void(void)> callback)
{
	m_closeHandler = callback;
}

void emna::drivers::SdlInputDevice::onKey(std::function<void(input::Key, input::ButtonState)> callback)
{
	m_keyHandler = callback;
}

void emna::drivers::SdlInputDevice::onMouseClick(std::function<void(input::MouseButton, maths::point2u, input::ButtonState)> callback)
{
	m_mouseClickHandler = callback;
}

void emna::drivers::SdlInputDevice::onMouseMove(std::function<void(maths::point2u)> callback)
{
	m_mouseMoveHandler = callback;
}

emna::input::MouseButton emna::drivers::SdlInputDevice::getVirtualMouseButton(std::uint8_t button)
{
	switch (button)
	{
		case SDL_BUTTON_LEFT:
			return emna::input::MouseButton::Left;
		case SDL_BUTTON_RIGHT:
			return emna::input::MouseButton::Right;
		case SDL_BUTTON_MIDDLE:
			return emna::input::MouseButton::Middle;

		default:
			return emna::input::MouseButton::Middle;
	}
}

emna::input::Key emna::drivers::SdlInputDevice::getVirtualKeyCode(SDL_keysym key)
{
	return input::Key::instance(std::to_string(key.sym), key.sym);
}

void emna::drivers::SdlInputDevice::poll()
{
	SDL_Event event;
	while (SDL_PollEvent(&event) != 0)
	{
		switch (event.type)
		{
			case SDL_QUIT:
				m_closeHandler();
		        break;

			case SDL_MOUSEMOTION:
				m_mouseMoveHandler(maths::point2u(event.motion.x, event.motion.y));
		        break;
			case SDL_MOUSEBUTTONDOWN:
				m_mouseClickHandler(getVirtualMouseButton(event.button.button), maths::point2u(event.button.x, event.button.y), input::ButtonState::Down);
		        break;
			case SDL_MOUSEBUTTONUP:
				m_mouseClickHandler(getVirtualMouseButton(event.button.button), maths::point2u(event.button.x, event.button.y), input::ButtonState::Up);
		        break;

			case SDL_KEYDOWN:
				m_keyHandler(getVirtualKeyCode(event.key.keysym), input::ButtonState::Down);
		        break;

			case SDL_KEYUP:
				m_keyHandler(getVirtualKeyCode(event.key.keysym), input::ButtonState::Up);
		        break;

			default:
				break;
		}
	}
}
