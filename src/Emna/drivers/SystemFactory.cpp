#include "SystemFactory.hpp"
#include "SystemConfiguration.hpp"

#ifdef EMSCRIPTEN
#include <emscripten/emscripten.h>
#endif // EMSCRIPTEN

#include "SdlDrivers.hpp"
#include "SfmlDrivers.hpp"
#include "NullDrivers.hpp"

#include <cereal/archives/json.hpp>

#include <fstream>
#include <stdexcept>

namespace emna
{
    namespace drivers
    {
        namespace
        {
            enum class Drivers
            {
	            SDL,
	            SFML,
	            Null
            };

            Drivers getDriverForConfigValue(std::size_t value)
            {
	            switch (value)
	            {
		            case 3:
			            return Drivers::Null;

#ifndef EMNA_DISABLE_SDL
				case 2:
					return Drivers::SDL;
#endif // EMNA_DISABLE_SDL

#ifndef EMNA_DISABLE_SFML
		            case 1:
			            return Drivers::SFML;
#endif // EMNA_DISABLE_SFML

		            default:
#ifndef EMNA_DISABLE_SFML
			            return Drivers::SFML;
#elif !defined(EMNA_DISABLE_SDL)
					return Drivers::SDL;
#else
					return Drivers::Null;
#endif // EMNA_DISABLE_x
	            }
            }

            System getSystem(emna::maths::point2i view, emna::drivers::SystemConfiguration config)
            {
	            const auto driver = getDriverForConfigValue(config.driver);
	            switch (driver)
	            {
	            case Drivers::SFML:
		            return createSfmlDrivers(view, config);
	            case Drivers::SDL:
		            return createSdlDrivers(view, config);
	            case Drivers::Null:
		            return createNullDrivers(view, config);
					
				default:
					throw std::runtime_error("Driver is not supported");
	            }
            }

        }
    }
}

emna::drivers::System emna::drivers::SystemFactory::create(maths::point2i view)
{
	SystemConfiguration config;
	std::ifstream configFile("system.json");
	if (configFile.is_open())
	{
		cereal::JSONInputArchive archive(configFile);
		archive(cereal::make_nvp("system-hints", config));
	}
#ifndef EMSCRIPTEN
	else
	{
		std::ofstream configOutFile("system.json");
		cereal::JSONOutputArchive archive(configOutFile);
		archive(cereal::make_nvp("system-hints", config));
	}
#endif // !EMSCRIPTEN

	auto system = getSystem(view, config);
	system.isFrameRateLocked = config.lockFrameRate;
	return system;
}
