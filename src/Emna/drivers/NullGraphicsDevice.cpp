#include "NullGraphicsDevice.hpp"
#include "../content/NullTexture.hpp"
#include <iostream>

emna::drivers::NullGraphicsDevice::NullGraphicsDevice(maths::point2i view)
		: m_out(std::cout)
{
	m_out << "NullGraphicsDevice::NullGraphicsDevice()\n";
	setViewWidth(view.x);
	setViewHeight(view.y);
}

emna::drivers::NullGraphicsDevice::NullGraphicsDevice(maths::point2i view, std::ostream &out)
		: m_out(out)
{
	m_out << "NullGraphicsDevice::NullGraphicsDevice()\n";
}

emna::drivers::NullGraphicsDevice::~NullGraphicsDevice()
{
	m_out << "NullGraphicsDevice::~NullGraphicsDevice()\n";
}

void emna::drivers::NullGraphicsDevice::clear()
{
	m_out << "NullGraphicsDevice::clear()\n";
}

void emna::drivers::NullGraphicsDevice::draw(maths::point2f pos, maths::point2f size,
                                             maths::point2i texturePos, maths::point2i textureSize,
                                             const content::Texture &texture)
{
	m_out << "NullGraphicsDevice::draw("
	<< "(" << pos << ", " << size << "), "
	<< "(" << texturePos << " - " << textureSize << "), "
	<< "[texture:" << static_cast<const content::NullTexture &>(texture).getName() << "]"
	<< ")\n";
}

void emna::drivers::NullGraphicsDevice::flush()
{
	m_out << "NullGraphicsDevice::flush()" << std::endl;
}
