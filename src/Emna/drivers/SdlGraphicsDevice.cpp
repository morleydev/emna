#include "SdlGraphicsDevice.hpp"
#include "../content/SdlTexture.hpp"

emna::drivers::SdlGraphicsDevice::SdlGraphicsDevice(std::shared_ptr<SDL_Surface> window, maths::point2i view)
		: m_surface(window)
{
	setWidth(m_surface->w);
	setHeight(m_surface->h);

	// TODO: Fix SDL Graphics Device to actually use a view
	setViewWidth(view.x);
	setViewHeight(view.y);
}

void emna::drivers::SdlGraphicsDevice::clear()
{
	SDL_FillRect(m_surface.get(), NULL, SDL_MapRGB(m_surface->format, 0, 0, 0));
}

void emna::drivers::SdlGraphicsDevice::draw(maths::point2f pos, maths::point2f size, maths::point2i texturePos, maths::point2i textureSize, const content::Texture &texture)
{
	auto surface = static_cast<const content::SdlTexture *>(&texture)->getTexture();

	SDL_Rect srcRect;
	srcRect.x = texturePos.x;
	srcRect.y = texturePos.y;
	srcRect.w = textureSize.x;
	srcRect.h = textureSize.y;

	SDL_Rect dstRect;
	dstRect.x = static_cast<int>(pos.x + 0.5f);
	dstRect.y = static_cast<int>(pos.y + 0.5f);
	dstRect.w = static_cast<int>(size.x + 0.5f);
	dstRect.h = static_cast<int>(size.y + 0.5f);

	SDL_BlitSurface(surface, &srcRect, m_surface.get(), &dstRect);
}

void emna::drivers::SdlGraphicsDevice::flush()
{
	SDL_Flip(m_surface.get());
}

emna::drivers::SdlGraphicsDevice::~SdlGraphicsDevice()
{
}
