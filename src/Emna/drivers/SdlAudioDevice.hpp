#ifndef EMNA_DRIVERS_SDLAUDIODEVICE_H_INCLUDED
#define EMNA_DRIVERS_SDLAUDIODEVICE_H_INCLUDED

#include "../AudioDriver.h"

namespace emna
{
    namespace drivers
    {
        class SdlAudioDevice : public AudioDriver
        {
        private:
	        maths::point3f m_position;
	        float m_soundEffectVolume;

        public:
	        virtual void play(content::MusicStream &music);
	        virtual void play(content::SoundEffect& music, maths::point3f position);

	        virtual void setListenerPosition(maths::point3f position);
	        virtual maths::point3f getListenerPosition() const;

	        virtual void setGlobalVolume(float volume);
	        virtual float getGlobalVolume() const;

	        virtual void update(std::chrono::microseconds deltaTime);

	        SdlAudioDevice();

	        virtual ~SdlAudioDevice();
        };
    }
}

#endif // EMNA_DRIVERS_SDLAUDIODEVICE_H_INCLUDED
