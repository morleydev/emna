#include <iostream>
#include "NullAudioDevice.hpp"
#include "../content/NullMusicStream.hpp"
#include "../content/NullSoundEffect.hpp"
#include "../util/log.h"

emna::drivers::NullAudioDevice::NullAudioDevice()
: m_out(std::cout),
  m_listenerPosition(0.0f, 0.0f, 0.0f),
  m_volume(1.0f)
{
}

emna::drivers::NullAudioDevice::NullAudioDevice(std::ostream &out)
		: m_out(out),
		  m_listenerPosition(0.0f, 0.0f, 0.0f),
		  m_volume(1.0f)
{
}

emna::drivers::NullAudioDevice::~NullAudioDevice()
{
}

void emna::drivers::NullAudioDevice::play(content::MusicStream &music)
{
	util::log(util::log_level::message,
	          "Playing: ",
	          static_cast<content::NullMusicStream&>(music).getFilename(),
	          (music.getLooping() ? "[loop]" : "[once]"));
}

void emna::drivers::NullAudioDevice::setListenerPosition(emna::maths::point3f position)
{
	util::log(util::log_level::message, "Moved listener to ", position);
	m_listenerPosition = position;
}

emna::maths::point3f emna::drivers::NullAudioDevice::getListenerPosition() const
{
	return m_listenerPosition;
}

void emna::drivers::NullAudioDevice::setGlobalVolume(float volume)
{
	util::log(util::log_level::message, "Set volume to ", volume);
	m_volume = volume;
}

float emna::drivers::NullAudioDevice::getGlobalVolume() const
{
	return m_volume;
}

void emna::drivers::NullAudioDevice::play(emna::content::SoundEffect &music, emna::maths::point3f position)
{
	util::log(util::log_level::message,
	          "Playing ", static_cast<content::NullSoundEffect&>(music).getFilename(), " at pos ", position);
}

void emna::drivers::NullAudioDevice::update(std::chrono::microseconds deltaTime)
{

}
