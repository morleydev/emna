#include "SdlDrivers.hpp"
#include "SdlContentFactory.hpp"
#include "SdlGraphicsDevice.hpp"
#include "SdlInputDevice.hpp"
#include "SdlAudioDevice.hpp"
#include "NullNetworkDevice.hpp"

#include "../util/make_unique.h"

#include <SDL/SDL.h>
#include <cstdint>

emna::drivers::System emna::drivers::createSdlDrivers(maths::point2i view, SystemConfiguration config)
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
		throw std::runtime_error(std::string("Could not initialise SDL: [") + SDL_GetError() + "]");

	auto videoMode = SDL_SetVideoMode(view.x > 0 ? view.x : (config.windowWidth > 0 ? config.windowWidth : 640),
	                                  view.y > 0 ? view.y : (config.windowHeight > 0 ? config.windowHeight : 480),
	                                  32,
	                                  SDL_SWSURFACE);
	if (!videoMode)
		throw std::runtime_error(std::string("Could not initialise SDL Video Mode: [") + SDL_GetError() + "]");

	std::shared_ptr<SDL_Surface> window(videoMode, [](SDL_Surface *) { SDL_Quit(); });

	System system;
	system.contentFactory = std::move(util::make_unique<drivers::SdlContentFactory>());
	system.graphicsDevice = std::move(util::make_unique<drivers::SdlGraphicsDevice>(window, view));
	system.inputDevice = std::move(util::make_unique<drivers::SdlInputDevice>());
	system.audioDevice = std::move(util::make_unique<drivers::SdlAudioDevice>());
	system.networkDevice = std::move(util::make_unique<drivers::NullNetworkDevice>());
	system.sleepFor = [](std::chrono::microseconds time) { SDL_Delay(static_cast<std::uint32_t>(time.count() / 1000)); };
	return std::move(system);
}
