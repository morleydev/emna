#ifndef EMNA_DRIVERS_NULLAUDIODEVICE_H_INCLUDED
#define EMNA_DRIVERS_NULLAUDIODEVICE_H_INCLUDED

#include "../AudioDriver.h"
#include <ostream>

namespace emna
{
    namespace drivers
    {
        class NullAudioDevice : public AudioDriver
        {
        private:
	        std::ostream &m_out;
	        maths::point3f m_listenerPosition;
	        float m_volume;

        public:
	        virtual void play(content::MusicStream &music);
	        virtual void play(content::SoundEffect& music, maths::point3f position);

	        virtual void setListenerPosition(maths::point3f position);
	        virtual maths::point3f getListenerPosition() const;

	        virtual void setGlobalVolume(float volume);
	        virtual float getGlobalVolume() const;

	        virtual void update(std::chrono::microseconds deltaTime);

	        NullAudioDevice();
	        NullAudioDevice(std::ostream&);

	        virtual ~NullAudioDevice();
        };
    }
}

#endif // EMNA_DRIVERS_NULLAUDIODEVICE_H_INCLUDED
