#ifndef EMNA_DRIVERS_SFMLINPUTDEVICE_H_INCLUDED
#define EMNA_DRIVERS_SFMLINPUTDEVICE_H_INCLUDED

#include "../InputDriver.h"
#include <SFML/Window.hpp>
#include <memory>

namespace emna
{
    namespace drivers
    {
        class SfmlInputDevice : public InputDriver
        {
        private:
	        std::shared_ptr<sf::Window> m_window;

	        std::function<void(input::Key, input::ButtonState)> m_keyHandler;
	        std::function<void(maths::point2u)> m_mouseMoveHandler;
	        std::function<void(input::MouseButton, maths::point2u, input::ButtonState)> m_mouseClickHandler;
	        std::function<void(void)> m_closeHandler;

	        static input::Key getVirtualKey(sf::Keyboard::Key k);

	        static input::MouseButton getVirtualMouseButton(sf::Mouse::Button b);


        public:
	        SfmlInputDevice(std::shared_ptr<sf::Window> window);

	        virtual ~SfmlInputDevice();

        public:
	        virtual void onKey(std::function<void(input::Key, input::ButtonState)>);

	        virtual void onMouseMove(std::function<void(maths::point2u)>);

	        virtual void onMouseClick(std::function<void(input::MouseButton, maths::point2u, input::ButtonState)>);

	        virtual void onClose(std::function<void(void)>);

	        virtual void poll();
        };
    }
}

#endif // EMNA_DRIVERS_SFMLINPUTDEVICE_H_INCLUDED
