#ifndef EMNA_DRIVERS_SDLCONTENTFACTORY_H_INCLUDED
#define EMNA_DRIVERS_SDLCONTENTFACTORY_H_INCLUDED

#include "../ContentFactory.h"

namespace emna
{
    namespace drivers
    {
        class SdlContentFactory : public ContentFactory
        {
        public:
	        virtual ~SdlContentFactory();

        public:
	        virtual std::unique_ptr<content::SoundEffect> loadSoundEffect(std::string);

	        virtual std::unique_ptr<content::Texture> loadTexture(std::string);

	        virtual std::unique_ptr<content::MusicStream> openMusicStream(std::string);

	        virtual std::unique_ptr<std::istream> openFileStream(std::string);
        };
    }
}

#endif // EMNA_DRIVERS_SDLCONTENTFACTORY_H_INCLUDED
