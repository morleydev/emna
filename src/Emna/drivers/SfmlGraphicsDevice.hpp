#ifndef EMNA_DRIVERS_SFMLGRAPHICSDRIVER_H_INCLUDED
#define EMNA_DRIVERS_SFMLGRAPHICSDRIVER_H_INCLUDED

#include "../GraphicsDriver.h"

#include <SFML/Graphics.hpp>

namespace emna
{
    namespace drivers
    {
        class SfmlGraphicsDevice : public GraphicsDriver
        {
        private:
	        std::shared_ptr<sf::RenderWindow> m_renderWindow;

        public:
	        SfmlGraphicsDevice(std::shared_ptr<sf::RenderWindow> renderWindow, maths::point2i virtualSize);

	        virtual ~SfmlGraphicsDevice();

	        virtual void clear();
	        virtual void draw(maths::point2f pos, maths::point2f size, maths::point2i texturePos, maths::point2i textureSize, const content::Texture &texture);
	        virtual void flush();

        private:
	        void setView(maths::point2i viewSize);
	        sf::FloatRect calculateViewport(maths::point2i viewSize);
        };
    }
}

#endif // EMNA_DRIVERS_SFMLGRAPHICSDRIVER_H_INCLUDED
