#ifndef EMNA_DRIVERS_SFMLDRIVERS_H_INCLUDED
#define EMNA_DRIVERS_SFMLDRIVERS_H_INCLUDED

#include "System.h"
#include "SystemConfiguration.hpp"
#include <stdexcept>

namespace emna
{
    namespace drivers
    {
#ifndef EMNA_DISABLE_SFML

        extern System createSfmlDrivers(maths::point2i view, SystemConfiguration config);

#else // EMNA_DISABLE_SFML
		inline System createSfmlDrivers(maths::point2i, SystemConfiguration)
		{
			throw std::runtime_error("SFML is disable");
		}
		
#endif // !EMNA_DISABLE_SFML
    }
}

#endif // EMNA_DRIVERS_SFMLDRIVERS_H_INCLUDED
