#ifndef EMNA_DRIVERS_SDLDRIVERS_H_INCLUDED
#define EMNA_DRIVERS_SDLDRIVERS_H_INCLUDED

#include "System.h"
#include "SystemConfiguration.hpp"
#include <stdexcept>

namespace emna
{
    namespace drivers
    {
#ifndef EMNA_DISABLE_SDL
		extern System createSdlDrivers(maths::point2i view, SystemConfiguration config);

#else // !EMNA_DISABLE_SDL

        inline System createSdlDrivers(maths::point2i view, SystemConfiguration config)
        {
	        throw std::runtime_error("SDL not supported");
        }

#endif // EMNA_DISABLE_SDL
    }
}

#endif // EMNA_DRIVERS_SDLDRIVERS_H_INCLUDED
