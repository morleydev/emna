#ifndef EMNA_DRIVERS_SDLINPUTDEVICE_H_INCLUDED
#define EMNA_DRIVERS_SDLINPUTDEVICE_H_INCLUDED

#include "../InputDriver.h"
#include <SDL/SDL.h>
#include <cstdint>

namespace emna
{
    namespace drivers
    {
        class SdlInputDevice : public InputDriver
        {
        private:
	        std::function<void(input::Key, input::ButtonState)> m_keyHandler;
	        std::function<void(maths::point2u)> m_mouseMoveHandler;
	        std::function<void(input::MouseButton, maths::point2u, input::ButtonState)> m_mouseClickHandler;
	        std::function<void(void)> m_closeHandler;

        public:
	        SdlInputDevice();

	        virtual ~SdlInputDevice();

	        static input::MouseButton getVirtualMouseButton(std::uint8_t button);

	        static input::Key getVirtualKeyCode(SDL_keysym key);

        public:
	        virtual void onKey(std::function<void(input::Key, input::ButtonState)> callback);

	        virtual void onMouseMove(std::function<void(maths::point2u)> callback);

	        virtual void onMouseClick(std::function<void(input::MouseButton, maths::point2u, input::ButtonState)> callback);

	        virtual void onClose(std::function<void(void)> callback);

	        virtual void poll();
        };
    }
}

#endif // EMNA_DRIVERS_SDLINPUTDEVICE_H_INCLUDED
