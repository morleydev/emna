#include "SdlAudioDevice.hpp"

emna::drivers::SdlAudioDevice::SdlAudioDevice()
: m_position(0.0f, 0.0f, 0.0f),
  m_soundEffectVolume(1.0f)
{
}

emna::drivers::SdlAudioDevice::~SdlAudioDevice()
{
}

void emna::drivers::SdlAudioDevice::play(emna::content::MusicStream &music)
{
}

void emna::drivers::SdlAudioDevice::play(emna::content::SoundEffect &music, emna::maths::point3<float> position)
{

}

void emna::drivers::SdlAudioDevice::setListenerPosition(emna::maths::point3<float> position)
{
	m_position = position;
}

emna::maths::point3<float> emna::drivers::SdlAudioDevice::getListenerPosition() const
{
	return m_position;
}

void emna::drivers::SdlAudioDevice::setGlobalVolume(float volume)
{
	m_soundEffectVolume = volume;
}

float emna::drivers::SdlAudioDevice::getGlobalVolume() const
{
	return m_soundEffectVolume;
}

void emna::drivers::SdlAudioDevice::update(std::chrono::microseconds deltaTime)
{

}
