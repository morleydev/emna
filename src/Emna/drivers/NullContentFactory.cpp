#include "NullContentFactory.hpp"
#include "../content/NullTexture.hpp"
#include "../content/NullMusicStream.hpp"
#include "../content/NullSoundEffect.hpp"
#include "../util/make_unique.h"
#include <fstream>
#include <stdexcept>

emna::drivers::NullContentFactory::NullContentFactory()
{
}

emna::drivers::NullContentFactory::~NullContentFactory()
{
}

std::unique_ptr<emna::content::SoundEffect> emna::drivers::NullContentFactory::loadSoundEffect(std::string path)
{
	return util::make_unique<content::NullSoundEffect>(path);
}

std::unique_ptr<emna::content::Texture> emna::drivers::NullContentFactory::loadTexture(std::string path)
{
	return util::make_unique<content::NullTexture>(path);
}

std::unique_ptr<emna::content::MusicStream> emna::drivers::NullContentFactory::openMusicStream(std::string path)
{
	return util::make_unique<content::NullMusicStream>(path);
}

std::unique_ptr<std::istream> emna::drivers::NullContentFactory::openFileStream(std::string path)
{
	auto ptr = util::make_unique<std::ifstream>(path);
	if (!ptr->is_open())
		throw std::runtime_error("Could not find file: " + path);

	return std::move(ptr);
}
