#include "NullDrivers.hpp"

#include "NullContentFactory.hpp"
#include "NullGraphicsDevice.hpp"
#include "NullInputDevice.hpp"
#include "NullAudioDevice.hpp"
#include "NullNetworkDevice.hpp"

#include "../util/make_unique.h"

#include <iostream>

emna::drivers::System emna::drivers::createNullDrivers(maths::point2i view, SystemConfiguration)
{
	System system;
	system.contentFactory = std::move(util::make_unique<drivers::NullContentFactory>());
	system.graphicsDevice = std::move(util::make_unique<drivers::NullGraphicsDevice>(view));
	system.inputDevice = std::move(util::make_unique<drivers::NullInputDevice>());
	system.audioDevice = std::move(util::make_unique<drivers::NullAudioDevice>());
	system.networkDevice = std::move(util::make_unique<drivers::NullNetworkDevice>());
	system.sleepFor = [](std::chrono::microseconds time)
			{
				std::cout << "Sleep for: " << time.count() << "microseconds" << std::endl;
			};
	return std::move(system);
}
