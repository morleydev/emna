#ifndef EMNA_GAME_H_INCLUDED
#define EMNA_GAME_H_INCLUDED

#include "configure.h"
#include "drivers/System.h"
#include "EventQueue.h"
#include "EventDispatcher.h"
#include "ControllerStore.h"
#include "ViewStore.h"
#include "StateMachine.h"
#include "ec/EntityComponentStore.h"
#include <chrono>
#include <cstdint>
#include <functional>

namespace emna
{
    class Game
    {
    private:
	    bool m_isAlive;
	    drivers::System m_systemDevices;

    protected:
	    GraphicsDriver &graphicsDriver;
	    InputDriver &inputDriver;
	    AudioDriver &audioDriver;
	    NetworkDriver &networkDriver;
	    ContentFactory &content;
	    EventQueue eventQueue;
	    EventDispatcher eventDispatcher;
	    ControllerStore controllers;
	    ViewStore views;
	    StateMachine states;
		ec::EntityComponentStore entities;

    public:
	    Game(const Game &) = delete;
	    Game &operator=(const Game &) = delete;
		
	    /*! \brief Set whether or not the rendering frame rate should be logically locked at 60fps */
	    EMNA_EXTERN void setFrameRateLocked(bool isFrameRateLocked);

	    /*! \brief Get whether or not the rendering frame rate is being logically locked at 60fps */
	    EMNA_EXTERN bool isFrameRateLocked() const;

	    EMNA_EXTERN void beginDraw();

	    /*! \brief If override, call this from an inheriting class will draw the global views */
	    EMNA_EXTERN virtual void draw();

	    EMNA_EXTERN void endDraw();

	    EMNA_EXTERN void beginUpdate(std::chrono::microseconds deltaTime);

	    /*! \brief If override, call this from an inheriting class will update the global controllers and state */
	    EMNA_EXTERN virtual void update(std::chrono::microseconds deltaTime);

	    EMNA_EXTERN void endUpdate(std::chrono::microseconds deltaTime);
	    inline bool isAlive() const { return m_isAlive; }

	    /*! \brief Call from the Game to close the game */
	    inline void kill() { m_isAlive = false; }

	    EMNA_EXTERN Game(std::uint_least16_t viewX = 640, std::uint_least16_t viewY = 480);
		EMNA_EXTERN Game(std::uint_least16_t viewX, std::uint_least16_t viewY, 
						 std::function<drivers::System (maths::point2i)> systemFactory);
		
	    EMNA_EXTERN virtual ~Game();

	    EMNA_EXTERN void run();
    };
}

#endif// EMNA_GAME_H_INCLUDED
