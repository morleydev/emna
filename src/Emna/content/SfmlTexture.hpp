#ifndef EMNA_CONTENT_SFML_SFMLTEXTURE_H_INCLUDED
#define EMNA_CONTENT_SFML_SFMLTEXTURE_H_INCLUDED

#include "Texture.h"
#include <SFML/Graphics.hpp>

namespace emna
{
    namespace content
    {
        class SfmlTexture : public Texture
        {
        public:
	        SfmlTexture(sf::Texture texture);

	        virtual ~SfmlTexture();

	        inline sf::Texture &getTexture()
	        {
		        return m_texture;
	        }

	        inline const sf::Texture &getTexture() const
	        {
		        return m_texture;
	        }

        private:
	        sf::Texture m_texture;
        };
    }
}

#endif // EMNA_CONTENT_SFML_SFMLTEXTURE_H_INCLUDED
