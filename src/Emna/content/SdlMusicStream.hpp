#ifndef EMNA_CONTENT_SDLMUSICSTREAM_H_INCLUDED
#define EMNA_CONTENT_SDLMUSICSTREAM_H_INCLUDED

#include "MusicStream.h"

namespace emna
{
	namespace content
	{
		class SdlMusicStream : public MusicStream
		{
		public:
			virtual ~SdlMusicStream();
		};
	}
}

#endif // EMNA_CONTENT_SDLMUSICSTREAM_H_INCLUDED
