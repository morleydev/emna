#ifndef EMNA_CONTENT_SFMLMUSICSTREAM_H_INCLUDED
#define EMNA_CONTENT_SFMLMUSICSTREAM_H_INCLUDED

#include "MusicStream.h"

#include <SFML/Audio.hpp>
#include <memory>

namespace emna
{
    namespace content
    {
        class SfmlMusicStream : public MusicStream
        {
        private:
	        std::unique_ptr<sf::Music> m_music;

        public:
	        SfmlMusicStream(std::unique_ptr<sf::Music> music);

	        virtual ~SfmlMusicStream();

	        inline sf::Music &getData()
	        {
		        return *m_music;
	        }

	        inline const sf::Music &getData() const
	        {
		        return *m_music;
	        }
        };
    }
}

#endif // EMNA_CONTENT_SFMLMUSICSTREAM_H_INCLUDED
