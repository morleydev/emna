#ifndef EMNA_CONTENT_NULLSOUNDEFFECT_H_INCLUDED
#define EMNA_CONTENT_NULLSOUNDEFFECT_H_INCLUDED

#include "SoundEffect.h"
#include <string>

namespace emna
{
	namespace content
	{
		class NullSoundEffect : public SoundEffect
		{
		private:
			std::string m_filename;

		public:
			NullSoundEffect(std::string filename) : m_filename(filename) { }
			virtual ~NullSoundEffect();

			const std::string& getFilename() const { return m_filename; }
		};
	}
}

#endif // EMNA_CONTENT_NULLSOUNDEFFECT_H_INCLUDED
