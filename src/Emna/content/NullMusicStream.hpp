#ifndef EMNA_CONTENT_NULLMUSICSTREAM_H_INCLUDED
#define EMNA_CONTENT_NULLMUSICSTREAM_H_INCLUDED

#include "MusicStream.h"
#include <string>

namespace emna
{
    namespace content
    {
        class NullMusicStream : public MusicStream
        {
        private:
	        std::string m_filename;

        public:
	        NullMusicStream(std::string streamName);

	        virtual ~NullMusicStream();

	        std::string getFilename() const
	        {
		        return m_filename;
	        }
        };
    }
}

#endif // EMNA_CONTENT_NULLMUSICSTREAM_H_INCLUDED
