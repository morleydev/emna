#ifndef EMNA_CONTENT_SOUNDEFFECT_H_INCLUDED
#define EMNA_CONTENT_SOUNDEFFECT_H_INCLUDED

#include "../configure.h"

namespace emna
{
    namespace content
    {
        class SoundEffect
        {
        public:
	        EMNA_EXTERN virtual ~SoundEffect();
        };
    }
}

#endif // EMNA_CONTENT_SOUNDEFFECT_H_INCLUDED
