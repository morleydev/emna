#ifndef EMNA_CONTENT_NULLTEXTURE_H_INCLUDED
#define EMNA_CONTENT_NULLTEXTURE_H_INCLUDED

#include "Texture.h"
#include <string>

namespace emna
{
    namespace content
    {
        class NullTexture : public Texture
        {
        private:
	        std::string m_textureName;

        public:
	        NullTexture(std::string textureName);

	        virtual ~NullTexture();

	        inline std::string getName() const
	        {
		        return m_textureName;
	        }
        };
    }
}

#endif // EMNA_CONTENT_NULLTEXTURE_H_INCLUDED
