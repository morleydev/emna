#include "SfmlMusicStream.hpp"

emna::content::SfmlMusicStream::SfmlMusicStream(std::unique_ptr<sf::Music> music)
		: m_music(std::move(music))
{
}

emna::content::SfmlMusicStream::~SfmlMusicStream()
{
}
