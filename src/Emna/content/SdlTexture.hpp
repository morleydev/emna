#ifndef EMNA_CONTENT_SDL_SDLTEXTURE_H_INCLUDED
#define EMNA_CONTENT_SDL_SDLTEXTURE_H_INCLUDED

#include "Texture.h"
#include <SDL/SDL.h>
#include <memory>
#include <functional>

namespace emna
{
    namespace content
    {
        class SdlTexture : public Texture
        {
        private:
	        std::unique_ptr<SDL_Surface, std::function<void(SDL_Surface *)>> m_texture;
	        std::size_t m_realWidth;
	        std::size_t m_realHeight;

        public:
	        SdlTexture(std::unique_ptr<SDL_Surface, std::function<void(SDL_Surface *)>>);

	        virtual ~SdlTexture();

	        inline SDL_Surface *getTexture()
	        {
		        return m_texture.get();
	        }

	        inline SDL_Surface *getTexture() const
	        {
		        return m_texture.get();
	        }

	        inline std::size_t realWidth() const
	        {
		        return m_realWidth;
	        }

	        inline std::size_t realHeight() const
	        {
		        return m_realHeight;
	        }
        };
    }
}

#endif // EMNA_CONTENT_SDL_SDLTEXTURE_H_INCLUDED
