#include "SdlTexture.hpp"

emna::content::SdlTexture::SdlTexture(std::unique_ptr<SDL_Surface, std::function<void(SDL_Surface *)>> texture)
		: m_texture(std::move(texture))
{
	setWidth(m_texture->w);
	setHeight(m_texture->h);
}

emna::content::SdlTexture::~SdlTexture()
{
}
