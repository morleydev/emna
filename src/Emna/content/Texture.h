#ifndef EMNA_CONTENT_TEXTURE_H_INCLUDED
#define EMNA_CONTENT_TEXTURE_H_INCLUDED

#include "../configure.h"
#include <cstddef>

namespace emna
{
    namespace content
    {
        class Texture
        {
        public:
	        EMNA_EXTERN virtual ~Texture();

	        inline std::size_t getWidth() const
	        {
		        return m_width;
	        }

	        inline std::size_t getHeight() const
	        {
		        return m_height;
	        }

        protected:
	        inline void setWidth(std::size_t w)
	        {
		        m_width = w;
	        }

	        inline void setHeight(std::size_t h)
	        {
		        m_height = h;
	        }

        private:
	        std::size_t m_width, m_height;
        };
    }
}

#endif // EMNA_CONTENT_TEXTURE_H_INCLUDED
