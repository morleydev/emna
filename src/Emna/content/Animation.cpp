#include "Animation.h"
#include <iostream>

emna::content::Animation::Animation(std::unique_ptr<Texture> animationTexture, 
                                    std::chrono::microseconds animationFrameLength, 
									std::vector<Frame> frames)
: m_animationTexture(std::move(animationTexture)),
  m_animationFrameLength(animationFrameLength),
  m_animationFrames(frames)
{
}

emna::content::Animation::Frame emna::content::Animation::getTextureFrameAt(std::chrono::microseconds time) const
{
	return m_animationFrames.at((time.count() / m_animationFrameLength.count()) % m_animationFrames.size());
}

const emna::content::Texture& emna::content::Animation::getTexture() const
{
	return *m_animationTexture;
}

std::chrono::microseconds emna::content::Animation::getLength() const
{
	return std::chrono::microseconds(m_animationFrameLength.count() * m_animationFrames.size());
}

std::size_t emna::content::Animation::getFrameCount() const
{
	return m_animationFrames.size();
}
