#include "NullMusicStream.hpp"


emna::content::NullMusicStream::NullMusicStream(std::string streamName)
		: m_filename(streamName)
{
}

emna::content::NullMusicStream::~NullMusicStream()
{
}
