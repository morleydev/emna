#ifndef EMNA_CONTENT_SFMLSOUNDEFFECT_H_INCLUDED
#define EMNA_CONTENT_SFMLSOUNDEFFECT_H_INCLUDED

#include "SoundEffect.h"
#include <SFML/Audio.hpp>
#include <memory>

namespace emna
{
	namespace content
	{
		class SfmlSoundEffect : public SoundEffect
		{
		private:
			std::shared_ptr<sf::SoundBuffer> m_soundBuffer;

		public:
			SfmlSoundEffect(std::shared_ptr<sf::SoundBuffer> buffer) : m_soundBuffer(buffer) { }
			virtual ~SfmlSoundEffect();

			std::shared_ptr<sf::SoundBuffer> getBuffer() const { return m_soundBuffer; }
		};
	}
}

#endif// EMNA_CONTENT_SFMLSOUNDEFFECT_H_INCLUDED
