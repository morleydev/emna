#ifndef EMNA_CONTENT_MUSICSTREAM_H_INCLUDED
#define EMNA_CONTENT_MUSICSTREAM_H_INCLUDED

#include "../configure.h"

namespace emna
{
    namespace content
    {
        class MusicStream
        {
        private:
	        bool m_loops;

        public:
	        EMNA_EXTERN MusicStream();
	        EMNA_EXTERN virtual ~MusicStream();

	        bool getLooping() const
	        {
		        return m_loops;
	        }

	        void setLooping(bool loops)
	        {
		        m_loops = loops;
	        }
        };
    }
}

#endif // EMNA_CONTENT_MUSICSTREAM_H_INCLUDED
