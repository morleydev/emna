#ifndef EMNA_CONTENT_SDLSOUNDEFFECT_H_INCLUDED
#define EMNA_CONTENT_SDLSOUNDEFFECT_H_INCLUDED

#include "SoundEffect.h"

namespace emna
{
	namespace content
	{
		class SdlSoundEffect : public SoundEffect
		{
		public:
			virtual ~SdlSoundEffect();
		};
	}
}

#endif//EMNA_CONTENT_SDLSOUNDEFFECT_H_INCLUDED
