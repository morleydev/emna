#ifndef EMNA_CONTENT_ANIMATION_H_INCLUDED
#define EMNA_CONTENT_ANIMATION_H_INCLUDED

#include "../configure.h"

#include "Texture.h"
#include "../maths/point2.h"
#include <chrono>
#include <vector>
#include <memory>

namespace emna
{
	namespace content
	{
		class Animation
		{
		public:
			struct Frame
			{
				maths::point2i position;
				maths::point2i size;
			};
			
		private:
			std::unique_ptr<Texture> m_animationTexture;
			std::chrono::microseconds m_animationFrameLength;
			std::vector<Frame> m_animationFrames;
			
		public:
			EMNA_EXTERN Animation(std::unique_ptr<Texture> animationTexture,
								  std::chrono::microseconds animationFrameLength, 
								  std::vector<Frame> frames);
		
			EMNA_EXTERN Frame getTextureFrameAt(std::chrono::microseconds) const;
			EMNA_EXTERN const Texture& getTexture() const;
			
			EMNA_EXTERN std::chrono::microseconds getLength() const;
			EMNA_EXTERN std::size_t getFrameCount() const;
		};
	}
}

#endif//EMNA_CONTENT_ANIMATION_H_INCLUDED
