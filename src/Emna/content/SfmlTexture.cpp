#include "SfmlTexture.hpp"

emna::content::SfmlTexture::~SfmlTexture()
{
}

emna::content::SfmlTexture::SfmlTexture(sf::Texture texture)
		: m_texture(texture)
{
	auto size = m_texture.getSize();
	setWidth(size.x);
	setHeight(size.y);
}
