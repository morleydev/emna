#ifndef EMNA_CONTROLLERSTORE_H_INCLUDED
#define EMNA_CONTROLLERSTORE_H_INCLUDED

#include "configure.h"
#include "Controller.h"
#include "util/make_unique.h"

#include <memory>
#include <typeinfo>
#include <typeindex>
#include <map>

namespace emna
{
    class ControllerStore
    {
    private:
	    std::map<std::type_index, std::unique_ptr<Controller>> m_controllerSet;

    public:
	    template<typename T, typename... Args> void add(Args... args)
	    {
		    add( typeid(T), std::unique_ptr<Controller>(util::make_unique<T>(args...)) );
	    }

	    template<typename T> T& get() const { return static_cast<T&>( get(typeid(T)) ); }
	    template<typename T> void remove() { return remove(typeid(T)); }

	    EMNA_EXTERN void tick(std::chrono::microseconds deltaTime);

    private:
	    EMNA_EXTERN void add(const std::type_info &type, std::unique_ptr<Controller> controller);
	    EMNA_EXTERN void remove(const std::type_info &type);
	    EMNA_EXTERN Controller& get(const std::type_info &type) const;
    };
}

#endif//EMNA_CONTROLLERSTORE_H_INCLUDED
