#ifndef EMNA_UTIL_EXCHANGE_H_INCLUDED
#define EMNA_UTIL_EXCHANGE_H_INCLUDED

#include <utility>

namespace emna
{
	namespace util
	{
		template<typename T, typename U>
		T exchange(T& orig, U newT)
		{
			T copy = std::move(orig);
			orig = std::move(newT);
			return copy;
		}
	}
}

#endif//