#include "to_global_string.h"
#include <set>

const char *emna::util::to_global_string(std::string str)
{
	static std::set<std::string> s_stringStore;

	return s_stringStore.insert(str).first->c_str();
}
