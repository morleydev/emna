#ifndef EMNA_UTIL_LOG_H_INCLUDED
#define EMNA_UTIL_LOG_H_INCLUDED

#include "../configure.h"
#include <ostream>

namespace emna
{
	namespace util
	{
		namespace detail
		{
			EMNA_EXTERN std::ostream& getLogStream();
		    EMNA_EXTERN std::ostream& getErrStream();

		    inline void stream_variadic(std::ostream& stream) { stream << std::endl; }
		    template<typename... ARGS, typename T>
		    void stream_variadic(std::ostream& stream, const T& next, const ARGS& ... tail)
		    {
			    stream_variadic(stream << next, tail...);
		    }
		}

	    EMNA_EXTERN void setLogStream(std::ostream& stream);
	    EMNA_EXTERN void setErrStream(std::ostream& stream);

		enum class log_level
		{
			debug,
			message,
			warning,
			error
		};

		template<typename... ARGS>
		void log(log_level level, const ARGS& ... message)
		{
			switch(level)
			{
			case log_level::debug:
#if defined(EMNA_DEBUG) || defined(DEBUG) || defined(_DEBUG)
				detail::stream_variadic(detail::getLogStream(), "[DEBUG] ", message...);
#endif
				break;

			case log_level::message:
				detail::stream_variadic(detail::getLogStream(), "[MSG] ", message...);
				break;

			case log_level::warning:
				detail::stream_variadic(detail::getLogStream(), "[WARN] ", message...);
				break;

			case log_level::error:
				detail::stream_variadic(detail::getErrStream(), "[ERR] ", message...);
				break;
			}
		}
	}

}

#endif // EMNA_UTIL_LOG_H_INCLUDED
