#include "log.h"
#include <iostream>

namespace
{
	static std::ostream* get_cout() { return &std::cout; }
    static std::ostream* get_cerr() { return &std::cerr; }

	std::ostream* g_logStream = &std::cout;
    std::ostream* g_errStream = &std::cerr;
}

void emna::util::setLogStream(std::ostream& stream)
{
	g_logStream = &stream;
}

void emna::util::setErrStream(std::ostream& stream)
{
	g_errStream = &stream;
}


std::ostream& emna::util::detail::getLogStream()
{
	return *g_logStream;
}

std::ostream& emna::util::detail::getErrStream()
{
	return *g_errStream;
}
