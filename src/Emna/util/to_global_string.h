#ifndef EMNA_UTIL_GLOBALSTRINGSTORE_H_INCLUDED
#define EMNA_UTIL_GLOBALSTRINGSTORE_H_INCLUDED

#include "../configure.h"
#include <string>

namespace emna
{
    namespace util
    {
        extern EMNA_EXTERN const char *to_global_string(std::string);
    }
}

#endif // EMNA_UTIL_GLOBALSTRINGSTORE_H_INCLUDED
