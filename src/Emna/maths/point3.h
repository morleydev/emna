#ifndef EMNA_MATHS_POINT3_H_INCLUDED
#define EMNA_MATHS_POINT3_H_INCLUDED

#include <cstdint>
#include <ostream>

namespace emna
{
    namespace maths
    {
        template<typename T>
        struct point3
        {
	        point3(): x(0), y(0), z(0)
	        {
	        }

	        point3(T x, T y, T z): x(x), y(y), z(z)
	        {
	        }

	        T x;
	        T y;
	        T z;
        };

        typedef point3<float> point3f;
        typedef point3<std::size_t> point3u;
        typedef point3<std::intptr_t> point3i;

        template<typename T>
        point3<T> operator+(point3<T> a, point3<T> b)
        {
	        return point3<T>(a.x + b.x, a.y + b.y, a.z + b.z);
        }

        template<typename T>
        point3<T> operator-(point3<T> a, point3<T> b)
        {
	        return point3<T>(a.x - b.x, a.y - b.y, a.z - b.z);
        }

        template<typename T>
        point3<T> operator*(point3<T> a, T b)
        {
	        return point3<T>(a.x * b, a.y * b, a.z * b);
        }

        template<typename T>
        point3<T> operator/(point3<T> a, T b)
        {
	        return point3<T>(a.x / b, a.y / b, a.z / b);
        }

        template<typename T>
        bool operator==(point3<T> a, point3<T> b)
        {
	        return a.x == b.x && a.y == b.y && a.z == b.z;
        }

        template<typename T>
        bool operator!=(point3<T> a, point3<T> b)
        {
	        return a.x != b.x && a.y != b.y && a.z != b.z;
        }

        template<typename T>
        bool operator>=(point3<T> a, point3<T> b)
        {
	        return a.x >= b.x && a.y >= b.y && a.z >= b.z;
        }

        template<typename T>
        bool operator<=(point3<T> a, point3<T> b)
        {
	        return a.x <= b.x && a.y <= b.y && a.z <= b.z;
        }

        template<typename T>
        bool operator>(point3<T> a, point3<T> b)
        {
	        return a.x > b.x && a.y > b.y && a.z > b.z;
        }

        template<typename T>
        bool operator<(point3<T> a, point3<T> b)
        {
	        return a.x < b.x && a.y < b.y && a.z < b.z;
        }

        template<typename T>
        std::ostream &operator<<(std::ostream &stream, const point3<T> &point)
        {
	        return stream << '{' << point.x << ',' << point.y << ',' << point.z << '}';
        }

    }
}

#endif // EMNA_MATHS_POINT3_H_INCLUDED
