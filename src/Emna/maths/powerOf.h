#ifndef EMNA_MATHS_POWEROF_H_INCLUDED
#define EMNA_MATHS_POWEROF_H_INCLUDED

namespace emna
{
    namespace maths
    {
        template<typename T>
        bool isPowerOf2(T value)
        {
	        if (value < T(1))
		        return false;

	        auto x = static_cast<unsigned>(value);
	        return (x & (x - 1)) == 0;
        }

        template<typename T>
        T nextPowerOf2(T value)
        {
	        if (value < T(1))
		        return T(1);

	        T x(2);
	        while (x <= value) x *= x;
	        return x;
        }
    }
}

#endif // EMNA_MATHS_POWEROF_H_INCLUDED
