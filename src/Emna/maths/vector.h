#ifndef EMNA_MATHS_VECTOR_H_INCLUDED
#define EMNA_MATHS_VECTOR_H_INCLUDED

#include "point2.h"
#include "point3.h"
#include <cmath>

namespace emna
{
    namespace maths
    {
        template<typename T>
        T dot_product(point2<T> a, point2<T> b)
        {
	        return a.x * b.x + a.y * b.y;
        }

        template<typename T>
        T dot_product(point3<T> a, point3<T> b)
        {
	        return a.x * b.x + a.y * b.y + a.z * b.z;
        }

        template<typename T>
        auto magnitude_squared(T a) -> decltype(dot_product(a, a))
        {
	        return dot_product(a, a);
        }

        template<typename T>
        auto magnitude(T a) -> decltype(std::sqrt(magnitude_squared(a)))
        {
	        return std::sqrt(magnitude_squared(a));
        }

        template<typename T>
        auto normalise(T a) -> decltype(a / magnitude(a))
        {
	        return a / magnitude(a);
        }
    }
}

#endif // EMNA_MATHS_VECTOR_H_INCLUDED
