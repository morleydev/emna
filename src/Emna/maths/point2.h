#ifndef EMNA_MATHS_POINT2_H_INCLUDED
#define EMNA_MATHS_POINT2_H_INCLUDED

#include <cstdint>
#include <ostream>

namespace emna
{
    namespace maths
    {
        template<typename T>
        struct point2
        {
	        point2(): x(0), y(0)
	        {
	        }

	        point2(T x, T y): x(x), y(y)
	        {
	        }

	        T x;
	        T y;
        };

        typedef point2<float> point2f;
        typedef point2<std::size_t> point2u;
        typedef point2<std::intptr_t> point2i;

        template<typename T>
        point2<T> operator+(point2<T> a, point2<T> b)
        {
	        return point2<T>(a.x + b.x, a.y + b.y);
        }

        template<typename T>
        point2<T> operator-(point2<T> a, point2<T> b)
        {
	        return point2<T>(a.x - b.x, a.y - b.y);
        }

        template<typename T>
        point2<T> operator*(point2<T> a, T b)
        {
	        return point2<T>(a.x * b, a.y * b);
        }

        template<typename T>
        point2<T> operator/(point2<T> a, T b)
        {
	        return point2<T>(a.x / b, a.y / b);
        }

        template<typename T>
        bool operator==(point2<T> a, point2<T> b)
        {
	        return a.x == b.x && a.y == b.y;
        }

        template<typename T>
        bool operator!=(point2<T> a, point2<T> b)
        {
	        return a.x != b.x && a.y != b.y;
        }

        template<typename T>
        bool operator>=(point2<T> a, point2<T> b)
        {
	        return a.x >= b.x && a.y >= b.y;
        }

        template<typename T>
        bool operator<=(point2<T> a, point2<T> b)
        {
	        return a.x <= b.x && a.y <= b.y;
        }

        template<typename T>
        bool operator>(point2<T> a, point2<T> b)
        {
	        return a.x > b.x && a.y > b.y;
        }

        template<typename T>
        bool operator<(point2<T> a, point2<T> b)
        {
	        return a.x < b.x && a.y < b.y;
        }

        template<typename T>
        std::ostream &operator<<(std::ostream &stream, const point2<T> &point)
        {
	        return stream << '{' << point.x << ',' << point.y << '}';
        }
    }
}

#endif // EMNA_MATHS_POINT2_H_INCLUDED
