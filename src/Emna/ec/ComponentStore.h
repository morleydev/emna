#ifndef EMNA_EC_COMPONENTSTORE_H_INCLUDED
#define EMNA_EC_COMPONENTSTORE_H_INCLUDED

#include "Component.h"
#include <map>
#include <typeindex>
#include <algorithm>

namespace emna
{
    namespace ec
    {
        class ComponentStore
        {
        private:
	        std::map<std::type_index, Component<void>> m_componentStore;

        public:
	        ComponentStore(): m_componentStore()
	        {
	        }

	        ComponentStore(const ComponentStore &orig): m_componentStore(orig.m_componentStore)
	        {
	        }

	        ComponentStore(ComponentStore &&orig): m_componentStore(std::move(orig.m_componentStore))
	        {
	        }

	        ComponentStore &operator=(const ComponentStore &orig)
	        {
		        m_componentStore = orig.m_componentStore;
		        return *this;
	        }

	        ComponentStore &operator=(ComponentStore &&orig)
	        {
		        m_componentStore = std::move(orig.m_componentStore);
		        return *this;
	        }

	        inline void add(Component<void> c)
	        {
		        m_componentStore[std::type_index(c.getType())] = std::move(c);
	        }

	        inline void remove(const std::type_info &type)
	        {
		        m_componentStore.erase(type);
	        }

	        inline bool empty() const
	        {
		        return m_componentStore.empty();
	        }

	        inline std::size_t size() const
	        {
		        return m_componentStore.size();
	        }

	        inline bool has(const std::type_info &type) const
	        {
		        return m_componentStore.find(std::type_index(type)) != m_componentStore.end();
	        }

	        inline Component<void> &get(const std::type_info &type)
	        {
		        return m_componentStore.find(std::type_index(type))->second;
	        }

	        inline bool tryGet(const std::type_info &type, Component<void> &out) const
	        {
		        const auto it = m_componentStore.find(std::type_index(type));
		        if (it == m_componentStore.end())
			        return false;

		        out = it->second;
		        return true;
	        }

	        inline const Component<void> &get(const std::type_info &type) const
	        {
		        return m_componentStore.find(std::type_index(type))->second;
	        }

	        template<typename U>
	        struct _iterator
	        {
	        private:
		        U m_underlying;

	        public:
		        _iterator(U underlying): m_underlying(underlying)
		        {
		        }

		        _iterator(const _iterator &i): m_underlying(i.m_underlying)
		        {
		        }

		        _iterator(_iterator &&i): m_underlying(std::move(i.m_underlying))
		        {
		        }

		        _iterator &operator=(const _iterator &i)
		        {
			        m_underlying = i.m_underlying;
			        return *this;
		        }

		        _iterator &operator=(_iterator &&i)
		        {
			        m_underlying = std::move(i.m_underlying);
			        return *this;
		        }

		        Component<void> &operator*()
		        {
			        return m_underlying->second;
		        }

		        const Component<void> &operator*() const
		        {
			        return m_underlying->second;
		        }

		        Component<void> *operator->()
		        {
			        return &(m_underlying->second);
		        }

		        const Component<void> *operator->() const
		        {
			        return &(m_underlying->second);
		        }

		        _iterator &operator++()
		        {
			        ++m_underlying;
			        return *this;
		        }

		        _iterator operator++(int)
		        {
			        iterator prev(m_underlying++);
			        return prev;
		        }

		        bool operator==(const _iterator &i) const
		        {
			        return m_underlying == i.m_underlying;
		        }

		        bool operator!=(const _iterator &i) const
		        {
			        return m_underlying != i.m_underlying;
		        }
	        };

	        typedef _iterator<std::map<std::type_index, Component<void>>::iterator> iterator;
	        typedef const _iterator<std::map<std::type_index, Component<void>>::const_iterator> const_iterator;

	        inline iterator begin()
	        {
		        return (m_componentStore.begin());
	        }

	        inline const_iterator begin() const
	        {
		        return (m_componentStore.begin());
	        }

	        inline iterator end()
	        {
		        return (m_componentStore.end());
	        }

	        inline const_iterator end() const
	        {
		        return (m_componentStore.end());
	        }
        };
    }
}


#endif // EMNA_EC_COMPONENTSTORE_H_INCLUDED
