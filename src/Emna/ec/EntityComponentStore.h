#ifndef EMNA_EC_ENTITYCOMPONENTSTORE_H_INCLUDED
#define EMNA_EC_ENTITYCOMPONENTSTORE_H_INCLUDED

#include "../configure.h"
#include "EntityId.h"
#include "ComponentStore.h"

#include <vector>
#include <set>
#include <map>
#include <stdexcept>
#include <sstream>

namespace emna
{
    namespace ec
    {
		namespace detail
		{
			template<typename... ARGS> struct TypeSetFactory { };
			template<typename T, typename... ARGS> struct TypeSetFactory<T, ARGS...>
			{ 
				void operator()(std::set<const std::type_info *> &set)
				{
					set.insert(&typeid(T));
					TypeSetFactory<ARGS...>()(set);
				}
			};
			template<typename T> struct TypeSetFactory<T>
			{ 
				void operator()(std::set<const std::type_info *> &set)
				{
					set.insert(&typeid(T));
				}
			};
			
		}
		
        class EntityComponentStore
        {
        private:
	        std::map<EntityId, ComponentStore> m_entityStore;

	        template<typename... ARGS>
	        static std::set<const std::type_info *> createTypeSet()
	        {
		        std::set<const std::type_info*> set;
		        detail::TypeSetFactory<ARGS...>()(set);
				return set;
	        }

	        EMNA_EXTERN void _link(EntityId entityId, Component<void> component);
	        EMNA_EXTERN void _unlink(EntityId entityId, const std::type_info &componentType);

	        EMNA_EXTERN std::vector<EntityId> _getAllEntitiesWith(const std::type_info &componentType) const;
	        EMNA_EXTERN std::vector<EntityId> _getAllEntitiesWith(const std::set<const std::type_info*> &componentType) const;

	        EMNA_EXTERN Component<void> _getComponentFor(const std::type_info &componentType, EntityId entity) const;
			
        public:
	        EMNA_EXTERN void add(EntityId id);
	        EMNA_EXTERN void remove(EntityId id);
	        EMNA_EXTERN std::vector<EntityId> getAllEntities() const;

	        template<typename T>
	        void link(EntityId entityId, T component)
	        {
		        _link(entityId, Component<void>(Component<T>(component)));
	        }

	        template<typename T>
	        void unlink(EntityId entityId)
	        {
		        _unlink(entityId, typeid(T));
	        }

			template<typename... ARGS>
	        std::vector<EntityId> getAllEntitiesWith() const
	        {
		        return _getAllEntitiesWith(createTypeSet<ARGS...>());
	        }

	        template<typename T>
	        T getComponentFor(EntityId entity) const
	        {
				auto component = _getComponentFor(typeid(T), entity);
				if ( !component ) 
				{
					std::stringstream str; 
					str << "Component " << typeid(T).name() << " not connected to entity " << entity;
					throw std::runtime_error(str.str());
				}
				return component.getData<T>();
	        }
        };
    }
}

#endif // EMNA_EC_ENTITYCOMPONENTSTORE_H_INCLUDED
