#ifndef EMNA_EC_COMPONENT_H_INCLUDED
#define EMNA_EC_COMPONENT_H_INCLUDED

#include <memory>
#include <functional>

namespace emna
{
    namespace ec
    {
        template<typename T>
        class Component
        {
        private:
	        T m_data;

        public:
	        Component(): m_data()
	        {
	        }

	        Component(T c): m_data(c)
	        {
	        }

	        template<typename Y>
	        Component(const Component<Y> &c)
			        : m_data(c.getData())
	        {
	        }

	        template<typename Y>
	        Component(Component<Y> &&c)
			        : m_data(std::move(c.getDataRef()))
	        {
	        }

	        template<typename Y>
	        Component &operator=(const Component<Y> &c)
	        {
		        m_data = c.getData();
		        return *this;
	        }

	        template<typename Y>
	        Component &operator=(Component<Y> &&c)
	        {
				m_data = std::move(c.getDataRef());
		        return *this;
	        }

	        inline const T& getData() const
	        {
		        return m_data;
	        }
			
			inline T& getDataRef() 
			{
				return m_data;
			}

	        inline void setData(T newData)
	        {
		        m_data = std::move(newData);
	        }
        };

        template<>
        class Component<void>
        {
        private:
			template<typename T> 
			static std::shared_ptr<T> DefaultCopyOperation(std::shared_ptr<void> orig) 
			{ 
					auto origPtr = std::static_pointer_cast<T>(orig);
					return std::make_shared<T>(*origPtr);
			}

	        std::shared_ptr<void> m_data;
	        std::function<std::shared_ptr<void>(std::shared_ptr<void>)> m_copy;
	        const std::type_info *m_type;

        public:
	        Component(): m_data(), m_copy([](std::shared_ptr<void>)
			                                      {
				                                      return nullptr;
			                                      }), m_type(nullptr)
	        {
	        }

	        Component(const Component<void> &c)
			        : m_data(c.m_copy(c.m_data)),
			          m_copy(c.m_copy),
			          m_type(c.m_type)
	        {
	        }

	        Component(Component<void> &&c)
			        : m_data(std::move(c.m_data)),
			          m_copy(std::move(c.m_copy)),
			          m_type(std::move(c.m_type))
	        {
	        }

	        template<typename T>
	        Component(const Component<T> &c)
			        : m_data(std::make_shared<T>(c.getData())),
					  m_copy(&DefaultCopyOperation<T>),
			          m_type(&typeid(T))
	        {
	        }

	        template<typename T>
	        Component<void> &operator=(const Component<T> &c)
	        {
		        m_data = std::make_shared<T>(c.getData());
				m_copy = &DefaultCopyOperation<T>;

		        m_type = &typeid(T);
		        return *this;
	        }

	        Component<void> &operator=(const Component<void> &c)
	        {
		        m_data = c.m_copy(c.m_data);
		        m_copy = c.m_copy;
		        m_type = c.m_type;
		        return *this;
	        }

	        Component<void> &operator=(Component<void> &&c)
	        {
		        m_data = std::move(c.m_data);
		        m_copy = std::move(c.m_copy);
		        m_type = std::move(c.m_type);
		        return *this;
	        }

	        template<typename T>
	        T getData() const
	        {
		        return *std::static_pointer_cast<T>(m_data);
	        }

	        const std::type_info& getType() const
	        {
		        return *m_type;
	        }

	        explicit operator bool() const
	        {
		        return static_cast<bool>(m_data);
	        }
        };
    }
}

#endif // EMNA_EC_COMPONENT_H_INCLUDED
