#include "EntityComponentStore.h"
#include <cpplinq.hpp>

void emna::ec::EntityComponentStore::add(EntityId id)
{
	m_entityStore.insert(std::make_pair(id, ComponentStore()));
}

void emna::ec::EntityComponentStore::remove(EntityId id)
{
	m_entityStore.erase(id);
}

void emna::ec::EntityComponentStore::_link(EntityId entityId, Component<void> component)
{
	auto it = m_entityStore.find(entityId);
	if (it == m_entityStore.end())
		return;

	it->second.add(component);
}

void emna::ec::EntityComponentStore::_unlink(EntityId entityId, const std::type_info &componentType)
{
	auto it = m_entityStore.find(entityId);
	if (it == m_entityStore.end())
		return;

	it->second.remove(componentType);
}

std::vector<emna::ec::EntityId> emna::ec::EntityComponentStore::getAllEntities() const
{
	return cpplinq::from(m_entityStore)
	       >> cpplinq::select([](const std::pair<EntityId, ComponentStore> &pair) { return pair.first; })
	       >> cpplinq::to_vector();
}

std::vector<emna::ec::EntityId> emna::ec::EntityComponentStore::_getAllEntitiesWith(const std::set<const std::type_info*> &componentTypes) const
{
	return cpplinq::from(m_entityStore)
	       >> cpplinq::where([&](const std::pair<EntityId, ComponentStore> &pair) { return cpplinq::from(componentTypes) >> cpplinq::all([&](const std::type_info* ti) { return pair.second.has(*ti); }); })
	       >> cpplinq::select([](const std::pair<EntityId, ComponentStore> &pair) { return pair.first; })
	       >> cpplinq::to_vector();
}

std::vector<emna::ec::EntityId> emna::ec::EntityComponentStore::_getAllEntitiesWith(const std::type_info &componentType) const
{
	return cpplinq::from(m_entityStore)
	       >> cpplinq::where([&](const std::pair<EntityId, ComponentStore> &pair) { return pair.second.has(componentType); })
	       >> cpplinq::select([](const std::pair<EntityId, ComponentStore> &pair) { return pair.first; })
	       >> cpplinq::to_vector();
}

emna::ec::Component<void> emna::ec::EntityComponentStore::_getComponentFor(const std::type_info &componentType, EntityId entity) const
{
	const auto it = m_entityStore.find(entity);
	if (it == m_entityStore.end())
		return Component<void>();

	Component<void> component;
	it->second.tryGet(componentType, component);
	return component;
}
