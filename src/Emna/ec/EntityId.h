#ifndef EMNA_EC_ENTITYID_H_INCLUDED
#define EMNA_EC_ENTITYID_H_INCLUDED

#include "../configure.h"
#include <cstdint>
#include <ostream>

namespace emna
{
    namespace ec
    {
        class EntityId
        {
        private:
	        unsigned int m_entityId;

	        EMNA_EXTERN EntityId(unsigned int id);

        public:
	        EMNA_EXTERN EntityId();

	        EMNA_EXTERN static EntityId generate();

	        EMNA_EXTERN bool operator==(EntityId o) const;
	        EMNA_EXTERN bool operator!=(EntityId o) const;
	        EMNA_EXTERN bool operator>(EntityId o) const;
	        EMNA_EXTERN bool operator>=(EntityId o) const;
	        EMNA_EXTERN bool operator<(EntityId o) const;
	        EMNA_EXTERN bool operator<=(EntityId o) const;

	        friend std::ostream &operator<<(std::ostream &out, EntityId id);
        };
        inline std::ostream &operator<<(std::ostream &out, EntityId id)
		{
			return (out << "[Entity: " << id.m_entityId << "]");
		}
    }
}

#endif // EMNA_EC_ENTITYID_H_INCLUDED
