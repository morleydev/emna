#include "EntityId.h"

namespace
{
	unsigned int g_prevEntityId;
}

emna::ec::EntityId::EntityId(unsigned int id)
		: m_entityId(id)
{
}

emna::ec::EntityId::EntityId()
		: m_entityId(0)
{
}

emna::ec::EntityId emna::ec::EntityId::generate()
{
	return emna::ec::EntityId(++g_prevEntityId);
}

bool emna::ec::EntityId::operator==(EntityId o) const
{
    return m_entityId == o.m_entityId;
}

bool emna::ec::EntityId::operator!=(EntityId o) const
{
    return m_entityId != o.m_entityId;
}

bool emna::ec::EntityId::operator>(EntityId o) const
{
    return m_entityId > o.m_entityId;
}

bool emna::ec::EntityId::operator>=(EntityId o) const
{
    return m_entityId >= o.m_entityId;
}

bool emna::ec::EntityId::operator<(EntityId o) const
{
    return m_entityId < o.m_entityId;
}

bool emna::ec::EntityId::operator<=(EntityId o) const
{
    return m_entityId <= o.m_entityId;
}
