#ifndef EMNA_CONTENTFACTORY_H_INCLUDED
#define EMNA_CONTENTFACTORY_H_INCLUDED

#include "configure.h"
#include "content/Texture.h"
#include "content/SoundEffect.h"
#include "content/MusicStream.h"

#include <memory>
#include <string>
#include <istream>

namespace emna
{
    class ContentFactory
    {
    public:
	    EMNA_EXTERN virtual ~ContentFactory();

	    virtual std::unique_ptr<content::Texture> loadTexture(std::string) = 0;
	    virtual std::unique_ptr<content::SoundEffect> loadSoundEffect(std::string) = 0;
	    virtual std::unique_ptr<content::MusicStream> openMusicStream(std::string) = 0;
	    virtual std::unique_ptr<std::istream> openFileStream(std::string) = 0;
    };
}

#endif // EMNA_CONTENTFACTORY_H_INCLUDED
