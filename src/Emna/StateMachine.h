#ifndef EMNA_STATEMACHINE_H_INCLUDED
#define EMNA_STATEMACHINE_H_INCLUDED

#include "configure.h"
#include "State.h"
#include <map>
#include <chrono>
#include <functional>

namespace emna
{
    class StateMachine
    {
    private:
	    std::map< std::size_t, std::function<std::unique_ptr<State> ()> > m_stateFactoryMap;
	    std::unique_ptr<State> m_currentState;
	    std::size_t m_currentStateId;

    public:
	    EMNA_EXTERN void add(std::size_t id, std::function<std::unique_ptr<State> ()> stateFactory);
	    EMNA_EXTERN void move(std::size_t id);
	    EMNA_EXTERN void update(std::chrono::microseconds deltatime);
	    EMNA_EXTERN void draw(GraphicsDriver& graphics);
    };
}

#endif // EMNA_STATEMACHINE_H_INCLUDED
