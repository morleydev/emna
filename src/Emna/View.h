#ifndef EMNA_VIEW_H_INCLUDED
#define EMNA_VIEW_H_INCLUDED

#include "configure.h"
#include "GraphicsDriver.h"
#include <cstdint>

namespace emna
{
	class View
	{
	private:
		std::int_least32_t m_depth;

	public:
		EMNA_EXTERN View();
		EMNA_EXTERN virtual ~View();

		inline std::int_least32_t getDepth() const { return m_depth; }
		inline void setDepth(std::int_least32_t depth) { m_depth = depth; }

		virtual void draw(GraphicsDriver& graphics) = 0;
	};
}

#endif//EMNA_VIEW_H_INCLUDED
