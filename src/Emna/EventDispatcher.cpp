#include "EventDispatcher.h"

void emna::EventDispatcherHandler::unbind()
{
	if ( dispatcher != nullptr )
	{
		dispatcher->disconnect(*this);
		dispatcher = nullptr;
	}
}


void emna::EventDispatcher::disconnect(const EventDispatcherHandler& handler)
{
	auto mapIt = m_eventMap.find(handler.typeIndex);
	if (mapIt == m_eventMap.end())
		return;

	mapIt->second.erase(handler.it);
}	

emna::EventDispatcherHandler emna::EventDispatcher::bind(const std::type_info& type, std::function<void (std::shared_ptr<void>)> handler)
{
	std::type_index typeIndex(type);
	auto& list = m_eventMap[typeIndex];
	list.push_back(handler);
	auto it = --list.end();
	return EventDispatcherHandler(this, it, typeIndex);
}

void emna::EventDispatcher::trigger(const std::type_info& type, std::shared_ptr<void> data)
{
	for(auto& handler : m_eventMap[std::type_index(type)])
		handler(data);
}

emna::EventDispatcherHandler::EventDispatcherHandler()
		: dispatcher(nullptr),
		  it(),
		  typeIndex(typeid(nullptr))
{
}

emna::EventDispatcherHandler::EventDispatcherHandler(EventDispatcher* d,
                                                     std::list< std::function<void (std::shared_ptr<void>)> >::iterator i,
		                                             std::type_index t)
: dispatcher(d),
it(i),
typeIndex(t)
{
}

emna::EventDispatcherHandler::EventDispatcherHandler(EventDispatcherHandler&& orig)
: dispatcher(util::exchange(orig.dispatcher, nullptr)),
it(std::move(orig.it)),
typeIndex(std::move(orig.typeIndex))
{
}

emna::EventDispatcherHandler& emna::EventDispatcherHandler::operator=(EventDispatcherHandler&& orig)
{
	unbind();

	dispatcher = util::exchange(orig.dispatcher, nullptr);
	it = std::move(orig.it);
	typeIndex = std::move(orig.typeIndex);
	return *this;
}

emna::EventDispatcherHandler::~EventDispatcherHandler()
{
	unbind();
}
