#include "StateMachine.h"

void emna::StateMachine::draw(GraphicsDriver& graphics)
{
	if (m_currentState)
		m_currentState->draw(graphics);
}

void emna::StateMachine::update(std::chrono::microseconds deltatime)
{
	if (!m_currentState)
		return;

	auto nextState = m_currentState->update(deltatime);
	if (m_currentStateId == nextState)
		return;

	move(nextState);
}

void emna::StateMachine::move(std::size_t id)
{
	auto stateFactoryIt = m_stateFactoryMap.find(id);
	if (stateFactoryIt == m_stateFactoryMap.end())
		return;

	m_currentState = stateFactoryIt->second();
	m_currentStateId = id;
}

void emna::StateMachine::add(std::size_t id, std::function<std::unique_ptr<State> ()> stateFactory)
{
	m_stateFactoryMap[id] = stateFactory;
}