#include "ControllerStore.h"
#include <stdexcept>
#include <string>

void emna::ControllerStore::add(const std::type_info &type, std::unique_ptr<Controller> controller)
{
	m_controllerSet.insert(std::make_pair(std::type_index(type), std::move(controller)));
}

void emna::ControllerStore::remove(const std::type_info &type)
{
	m_controllerSet.erase(std::type_index(type));
}

emna::Controller& emna::ControllerStore::get(const std::type_info &type) const
{
	auto i = m_controllerSet.find(std::type_index(type));
	if ( i == m_controllerSet.end() )
		throw std::out_of_range(std::string("Type ") + type.name() + " not registered controller");

	return *i->second;
}

void emna::ControllerStore::tick(std::chrono::microseconds deltaTime)
{
	for(auto& c : m_controllerSet)
		c.second->update(deltaTime);
}
