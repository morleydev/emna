#ifndef EMNA_INPUTDEVICE_H_INCLUDED
#define EMNA_INPUTDEVICE_H_INCLUDED

#include "configure.h"
#include "input/Key.h"
#include "input/ButtonState.h"
#include "input/MouseButton.h"
#include "maths/point2.h"

#include <functional>

namespace emna
{
    class InputDriver
    {
    private:
	    bool m_hasKeyboard;
	    bool m_hasMouse;

    protected:
	    inline void setHasMouseAccess(bool hasMouse)
	    {
		    m_hasMouse = hasMouse;
	    }

	    inline void setHasKeyboardAccess(bool hasKeyboard)
	    {
		    m_hasKeyboard = hasKeyboard;
	    }

    public:
	    inline bool hasMouseAccess() const
	    {
		    return m_hasMouse;
	    }

	    inline bool hasKeyboardAccess() const
	    {
		    return m_hasKeyboard;
	    }

	    EMNA_EXTERN InputDriver();

	    EMNA_EXTERN virtual ~InputDriver();

	    virtual void poll() = 0;

	    virtual void onKey(std::function<void(input::Key, input::ButtonState)>) = 0;

	    virtual void onMouseMove(std::function<void(maths::point2u)>) = 0;

	    virtual void onMouseClick(std::function<void(input::MouseButton, maths::point2u, input::ButtonState)>) = 0;

	    virtual void onClose(std::function<void(void)>) = 0;
    };
}

#endif //EMNA_INPUTDEVICE_H_INCLUDED
