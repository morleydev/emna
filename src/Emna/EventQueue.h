#ifndef EMNA_EVENTQUEUE_H_INCLUDED
#define EMNA_EVENTQUEUE_H_INCLUDED

#include "configure.h"
#include <deque>
#include <memory>
#include <typeinfo>

namespace emna
{
	class EventQueue
	{
	private:
		std::deque< std::pair<const std::type_info*, std::shared_ptr<void>> > m_queue;

		EMNA_EXTERN void _push(const std::type_info& type, std::shared_ptr<void> data);
		
	public:
		template<typename T, typename... ARGS> void push(ARGS&&... data) 
		{
			_push(typeid(T), std::make_shared<T>(std::forward<ARGS>(data)...));
		}

		EMNA_EXTERN bool pop(std::pair<const std::type_info*, std::shared_ptr<void>>& out);
	};
}

#endif // EMNA_EVENTQUEUE_H_INCLUDED
