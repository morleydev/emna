#ifndef EMNA_EVENTDISPATCHER_H_INCLUDED
#define EMNA_EVENTDISPATCHER_H_INCLUDED

#include "util/exchange.h"
#include "configure.h"
#include <map>
#include <list>
#include <memory>
#include <typeindex>
#include <typeinfo>
#include <functional>

namespace emna
{
    class EventDispatcher;
	class EventDispatcherHandler
	{
	public:
		EventDispatcherHandler(const EventDispatcherHandler&) = delete;
		EventDispatcherHandler& operator=(const EventDispatcherHandler&) = delete;

		EMNA_EXTERN void unbind();

		EMNA_EXTERN EventDispatcherHandler();
		EMNA_EXTERN EventDispatcherHandler(EventDispatcher* d,
		                       std::list< std::function<void (std::shared_ptr<void>)> >::iterator i,
		                       std::type_index t);

		EMNA_EXTERN EventDispatcherHandler(EventDispatcherHandler&& orig);
		EMNA_EXTERN EventDispatcherHandler& operator=(EventDispatcherHandler&& orig);
		EMNA_EXTERN ~EventDispatcherHandler();

		EventDispatcher* dispatcher;
		std::list< std::function<void (std::shared_ptr<void>)> >::iterator it;
		std::type_index typeIndex;
	};

	class EventDispatcher
	{
	private:
		friend class EventDispatcherHandler;
		
		typedef std::list< std::function<void (std::shared_ptr<void>)> > EventHandlerList;
		std::map<std::type_index, EventHandlerList> m_eventMap;

		EMNA_EXTERN void disconnect(const EventDispatcherHandler& handler);
		EMNA_EXTERN EventDispatcherHandler bind(const std::type_info& type, std::function<void (std::shared_ptr<void>)> handler);

	public:
		template<typename T> EventDispatcherHandler bind(std::function<void (const T&)> f)
		{
			return bind(typeid(T), [f](std::shared_ptr<void> ptr) { f(*std::static_pointer_cast<T>(ptr)); });
		}

		EMNA_EXTERN void trigger(const std::type_info& type, std::shared_ptr<void> data);
	};
}

#endif // EMNA_EVENTDISPATCHER_H_INCLUDED
