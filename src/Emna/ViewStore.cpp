#include "ViewStore.h"
#include <algorithm>
#include <stdexcept>

void emna::ViewStore::draw(GraphicsDriver &graphics)
{
	for(auto& c : m_views)
		c->draw(graphics);
}

void emna::ViewStore::add(const std::type_info &type, std::unique_ptr<View> view)
{
	if ( m_viewSet.find(type) != m_viewSet.end() )
		return;

	m_views.push_front(std::move(view));
	m_viewSet.insert(std::make_pair(std::type_index(type), m_views.begin()));
	m_views.sort([](std::unique_ptr<View>& v1, std::unique_ptr<View>& v2) { return v1->getDepth() < v2->getDepth(); });
}

void emna::ViewStore::remove(const std::type_info &type)
{
	auto i = m_viewSet.find(std::type_index(type));
	if ( i == m_viewSet.end() )
		return;

	m_views.erase(i->second);
	m_viewSet.erase(i);
}

emna::View & emna::ViewStore::get(const std::type_info &type) const
{
	auto i = m_viewSet.find(std::type_index(type));
	if ( i == m_viewSet.end() )
		throw std::out_of_range(std::string("Type ") + type.name() + " not registered view");

	return **(i->second);
}
