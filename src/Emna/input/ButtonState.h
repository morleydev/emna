#ifndef EMNA_INPUT_KEYSTATE_H_INCLUDED
#define EMNA_INPUT_KEYSTATE_H_INCLUDED

namespace emna
{
    namespace input
    {
        enum class ButtonState
        {
	        Down,
	        Up
        };
    }
}

#endif // EMNA_INPUT_KEYSTATE_H_INCLUDED
