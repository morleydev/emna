#ifndef EMNA_INPUT_KEY_H_INCLUDED
#define EMNA_INPUT_KEY_H_INCLUDED

#include <map>
#include <cstdint>
#include <algorithm>
#include <string>
#include "../util/to_global_string.h"

namespace emna
{
    namespace input
    {
        struct Key
        {
        private:
	        const char *m_name;
	        std::uint32_t m_code;

        private:
	        Key(const char *name, std::uint32_t code)
			        : m_name(name),
			          m_code(code)
	        {
	        }

        public:
	        inline bool operator==(const Key &b) const
	        {
		        return m_code == b.m_code;
	        }

	        inline bool operator!=(const Key &b) const
	        {
		        return m_code != b.m_code;
	        }

	        inline bool operator>(const Key &b) const
	        {
		        return m_code > b.m_code;
	        }

	        inline bool operator<(const Key &b) const
	        {
		        return m_code < b.m_code;
	        }

	        inline bool operator>=(const Key &b) const
	        {
		        return m_code >= b.m_code;
	        }

	        inline bool operator<=(const Key &b) const
	        {
		        return m_code <= b.m_code;
	        }

	        inline std::uint32_t getCode() const
	        {
		        return m_code;
	        }

	        inline std::string getName() const
	        {
		        return std::string(m_name);
	        }

	        inline static Key instance(std::string name, std::uint32_t code)
	        {
		        return Key(util::to_global_string(name), code);
	        }
        };

        namespace KeyCode
        {
            namespace
            {
#define _EMNA_DECLARE_KEY(K) Key K = Key::instance(#K, std::hash<std::string>()(#K))
#define _EMNA_DECLARE_KEY_NAMED(K, N) Key K = Key::instance(#N, std::hash<std::string>()(#K))

                _EMNA_DECLARE_KEY(A);
                _EMNA_DECLARE_KEY(B);
                _EMNA_DECLARE_KEY(C);
                _EMNA_DECLARE_KEY(D);
                _EMNA_DECLARE_KEY(E);
                _EMNA_DECLARE_KEY(F);
                _EMNA_DECLARE_KEY(G);
                _EMNA_DECLARE_KEY(H);
                _EMNA_DECLARE_KEY(I);
                _EMNA_DECLARE_KEY(J);
                _EMNA_DECLARE_KEY(K);
                _EMNA_DECLARE_KEY(L);
                _EMNA_DECLARE_KEY(M);
                _EMNA_DECLARE_KEY(N);
                _EMNA_DECLARE_KEY(O);
                _EMNA_DECLARE_KEY(P);
                _EMNA_DECLARE_KEY(Q);
                _EMNA_DECLARE_KEY(R);
                _EMNA_DECLARE_KEY(S);
                _EMNA_DECLARE_KEY(T);
                _EMNA_DECLARE_KEY(U);
                _EMNA_DECLARE_KEY(V);
                _EMNA_DECLARE_KEY(W);
                _EMNA_DECLARE_KEY(X);
                _EMNA_DECLARE_KEY(Y);
                _EMNA_DECLARE_KEY(Z);

                _EMNA_DECLARE_KEY(Escape);
                _EMNA_DECLARE_KEY(Space);

                _EMNA_DECLARE_KEY(Up);
                _EMNA_DECLARE_KEY(Down);
                _EMNA_DECLARE_KEY(Left);
                _EMNA_DECLARE_KEY(Right);

                _EMNA_DECLARE_KEY(F1);
                _EMNA_DECLARE_KEY(F2);
                _EMNA_DECLARE_KEY(F3);
                _EMNA_DECLARE_KEY(F4);
                _EMNA_DECLARE_KEY(F5);
                _EMNA_DECLARE_KEY(F6);
                _EMNA_DECLARE_KEY(F7);
                _EMNA_DECLARE_KEY(F8);
                _EMNA_DECLARE_KEY(F9);
                _EMNA_DECLARE_KEY(F10);
                _EMNA_DECLARE_KEY(F11);
                _EMNA_DECLARE_KEY(F12);

                _EMNA_DECLARE_KEY_NAMED(Num0, 0);
                _EMNA_DECLARE_KEY_NAMED(Num1, 1);
                _EMNA_DECLARE_KEY_NAMED(Num2, 2);
                _EMNA_DECLARE_KEY_NAMED(Num3, 3);
                _EMNA_DECLARE_KEY_NAMED(Num4, 4);
                _EMNA_DECLARE_KEY_NAMED(Num5, 5);
                _EMNA_DECLARE_KEY_NAMED(Num6, 6);
                _EMNA_DECLARE_KEY_NAMED(Num7, 7);
                _EMNA_DECLARE_KEY_NAMED(Num8, 8);
                _EMNA_DECLARE_KEY_NAMED(Num9, 9);

#undef _EMNA_DECLARE_KEY_NAMED
#undef _EMNA_DECLARE_KEY
            }
        }
    }
}

#endif // EMNA_INPUT_KEY_H_INCLUDED
