#ifndef EMNA_INPUT_MOUSEBUTTON_H_INCLUDED
#define EMNA_INPUT_MOUSEBUTTON_H_INCLUDED

namespace emna
{
    namespace input
    {
        enum class MouseButton
        {
	        Left,
	        Middle,
	        Right
        };
    }
}

#endif // EMNA_INPUT_MOUSEBUTTON_H_INCLUDED
