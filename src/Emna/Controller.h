#ifndef EMNA_CONTROLLER_H_INCLUDED
#define EMNA_CONTROLLER_H_INCLUDED

#include "configure.h"
#include <chrono>

namespace emna
{
    class Controller
    {
    public:
	    EMNA_EXTERN virtual ~Controller();
	    virtual void update(std::chrono::microseconds deltaTime) = 0;
    };
}

#endif // EMNA_CONTROLLER_H_INCLUDED
