file(GLOB_RECURSE EMNA_INCLUDE_FILES ${EMNA_SOURCE_DIR}/src/*.h)

SET(EMNA_SRC_FILES 	${EMNA_SOURCE_DIR}/src/Emna/Game.cpp
					${EMNA_SOURCE_DIR}/src/Emna/Controller.cpp
					${EMNA_SOURCE_DIR}/src/Emna/ControllerStore.cpp
					${EMNA_SOURCE_DIR}/src/Emna/View.cpp
					${EMNA_SOURCE_DIR}/src/Emna/ViewStore.cpp
					${EMNA_SOURCE_DIR}/src/Emna/EventDispatcher.cpp
					${EMNA_SOURCE_DIR}/src/Emna/EventQueue.cpp
					${EMNA_SOURCE_DIR}/src/Emna/State.cpp
					${EMNA_SOURCE_DIR}/src/Emna/StateMachine.cpp
                    ${EMNA_SOURCE_DIR}/src/Emna/ContentFactory.cpp
					${EMNA_SOURCE_DIR}/src/Emna/AudioDriver.cpp
                    ${EMNA_SOURCE_DIR}/src/Emna/GraphicsDriver.cpp
					${EMNA_SOURCE_DIR}/src/Emna/InputDriver.cpp
					${EMNA_SOURCE_DIR}/src/Emna/NetworkDriver.cpp
					${EMNA_SOURCE_DIR}/src/Emna/content/Texture.cpp
					${EMNA_SOURCE_DIR}/src/Emna/content/MusicStream.cpp
					${EMNA_SOURCE_DIR}/src/Emna/content/SoundEffect.cpp
					${EMNA_SOURCE_DIR}/src/Emna/content/Animation.cpp
					${EMNA_SOURCE_DIR}/src/Emna/content/NullTexture.cpp
					${EMNA_SOURCE_DIR}/src/Emna/content/NullMusicStream.cpp
					${EMNA_SOURCE_DIR}/src/Emna/content/NullSoundEffect.cpp
					${EMNA_SOURCE_DIR}/src/Emna/drivers/NullDrivers.cpp
					${EMNA_SOURCE_DIR}/src/Emna/drivers/NullAudioDevice.cpp
					${EMNA_SOURCE_DIR}/src/Emna/drivers/NullGraphicsDevice.cpp
					${EMNA_SOURCE_DIR}/src/Emna/drivers/NullInputDevice.cpp
					${EMNA_SOURCE_DIR}/src/Emna/drivers/NullNetworkDevice.cpp
					${EMNA_SOURCE_DIR}/src/Emna/drivers/NullContentFactory.cpp
					${EMNA_SOURCE_DIR}/src/Emna/drivers/SystemFactory.cpp
					${EMNA_SOURCE_DIR}/src/Emna/input/Key.cpp
					${EMNA_SOURCE_DIR}/src/Emna/util/to_global_string.cpp
					${EMNA_SOURCE_DIR}/src/Emna/util/log.cpp
					${EMNA_SOURCE_DIR}/src/Emna/ec/EntityId.cpp
					${EMNA_SOURCE_DIR}/src/Emna/ec/EntityComponentStore.cpp
					${EMNA_SOURCE_DIR}/src/Emna/network/Client.cpp
					${EMNA_SOURCE_DIR}/src/Emna/network/NullClient.cpp
					${EMNA_SOURCE_DIR}/src/Emna/network/Server.cpp
					${EMNA_SOURCE_DIR}/src/Emna/network/NullServer.cpp)

if (NOT DISABLE_SDL)
	SET(EMNA_SRC_FILES ${EMNA_SRC_FILES} 
					   ${EMNA_SOURCE_DIR}/src/Emna/content/SdlTexture.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/content/SdlMusicStream.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/content/SdlSoundEffect.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SdlDrivers.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SdlContentFactory.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SdlAudioDevice.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SdlGraphicsDevice.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SdlInputDevice.cpp)
endif()

if (NOT DISABLE_SFML)
	SET(EMNA_SRC_FILES ${EMNA_SRC_FILES}
					   ${EMNA_SOURCE_DIR}/src/Emna/content/SfmlTexture.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/content/SfmlMusicStream.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/content/SfmlSoundEffect.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SfmlDrivers.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SfmlContentFactory.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SfmlAudioDevice.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SfmlGraphicsDevice.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SfmlInputDevice.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/drivers/SfmlNetworkDevice.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/network/SfmlClient.cpp
					   ${EMNA_SOURCE_DIR}/src/Emna/network/SfmlServer.cpp)
endif()

if (EMSCRIPTEN) # Required for cereal to compile with Emscripten
    SET(EMNA_SRC_FILES ${EMNA_SRC_FILES} ${EMSCRIPTEN_ROOT_PATH}/system/lib/libcxxabi/src/cxa_demangle.cpp)
endif()